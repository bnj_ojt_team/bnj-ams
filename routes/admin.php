<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin;

// Admin Custom Routes

Route::get('/', function () {
    return redirect()->route('admin.dashboard.index');
});

// Dashboard Controller
Route::namespace(Admin::class)
    ->controller(DashboardController::class)
    ->prefix('dashboard')->name('dashboard.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index', 'index')->name('index');
    });

// Database Backup Controller
Route::namespace(Admin::class)
    ->controller(DatabaseBackupController::class)
    ->prefix('database')->name('database.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('backup', 'backup')->name('backup');
        Route::get('download-file/{filename}', 'downloadFile')->name('download-file');
        Route::get('create-new-backup', 'createNewBackup')->name('create-new-backup');
        Route::get('create-new-main-backup', 'createNewMainBackup')->name('create-new-main-backup');
    });

// Quotes Controller
Route::namespace(Admin::class)
    ->controller(QuotesController::class)
    ->prefix('quotes')->name('quotes.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::post('add', 'add')->name('add');
        Route::match(['post', 'get'],'edit/{id}', 'edit')->name('edit');
        Route::delete('delete/{id}', 'delete')->name('delete');
        Route::get('get-all-quotes', 'getAllQuotes')->name('get-all-quotes');
        Route::get('is-active/{id}/{process}', 'isActive')->name('is-active');
    });

// Holidays Controller
Route::namespace(Admin::class)
    ->controller(HolidaysController::class)
    ->prefix('holidays')->name('holidays.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::get('get-all-holidays', 'getAllHolidays')->name('get-all-holidays');
        Route::post('add', 'add')->name('add');
        Route::match(['post', 'get'], 'edit/{id}', 'edit')->name('edit');
        Route::delete('delete/{id}', 'delete')->name('delete');
    });

// Users Controller
Route::namespace(Admin::class)
    ->controller(UsersController::class)
    ->prefix('users')->name('users.')
    ->middleware(['auth', 'role:admin'])->group(function(){
        Route::match(['post', 'get'],'add', 'add')->name('add');
        Route::match(['post', 'get'],'edit', 'edit')->name('edit');
        Route::get('admin', 'admin')->name('admin');
        Route::get('manager', 'manager')->name('manager');
        Route::get('index', 'index')->name('index');
        Route::get('account-setting', 'accountSetting')->name('account-setting');
        Route::get('is-active/{id}/{process}', 'isActive')->name('is-active');
    });

// EmployeeRoles Controller
Route::namespace(Admin::class)
    ->controller(EmployeeRolesController::class)
    ->prefix('employee-roles')->name('employee-roles.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::post('add', 'add')->name('add');
        Route::match(['post', 'get'], 'edit/{id}', 'edit')->name('edit');
        Route::delete('delete/{id}/{password}', 'delete')->name('delete');
    });

// Employees Controller
Route::namespace(Admin::class)
    ->controller(EmployeesController::class)
    ->prefix('employees')->name('employees.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::post('add', 'add')->name('add');
        Route::match(['post', 'get'], 'edit/{id}', 'edit')->name('edit');
        Route::get('get-birthdays','getBirthdays')->name('get-birthdays');
        Route::get('update-contributions/{userId}','updateContributions')->name('update-contributions');
    });

// Attendances Controller
Route::namespace(Admin::class)
    ->controller(AttendancesController::class)
    ->prefix('attendances')->name('attendances.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::get('today-overtime-attendances', 'todayOvertimeAttendances')->name('today-overtime-attendances');
        Route::get('get-today-overtime-attendances','getTodayOvertimeAttendances')->name('get-today-overtime-attendances');
        Route::get('get-attendances','getAttendances')->name('get-attendances');
        Route::get('accomplishment/{id}','accomplishment')->name('accomplishment');
        Route::get('reason/{id}','reason')->name('reason');
        Route::get('update-overtime/{id}','updateOvertime')->name('update-overtime');
        Route::get('home-attendances','homeAttendances')->name('home-attendances');
        Route::get('update-attendance/{id}','updateAttendance')->name('update-attendance');
    });

// Company Settings Controller Controller
Route::namespace(Admin::class)
    ->controller(CompanySettings::class)
    ->prefix('company-settings')->name('company-settings.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::get('get-company-settings','getCompanySettings')->name('get-company-settings');
        Route::post('add','add')->name('add');
        Route::match(['post','get'],'edit/{id}','edit')->name('edit');
        Route::delete('delete/{id}','delete')->name('delete');
        Route::post('active/{id}','active')->name('active');
    });
    
// Contributions Controller Controller
Route::namespace(Admin::class)
    ->controller(ContributionsController::class)
    ->prefix('contributions')->name('contributions.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::match(['post','get'], 'add', 'add')->name('add');
        Route::match(['post','get'], 'edit/{id}', 'edit')->name('edit');
        Route::delete('delete/{id}', 'delete')->name('delete');
    });

// Salary Loans Controller Controller
Route::namespace(Admin::class)
    ->controller(SalaryLoansController::class)
    ->prefix('salary-loans')->name('salary-loans.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index','index')->name('index');
        Route::get('get-salary-loans','getSalaryLoans')->name('get-salary-loans');
        Route::match(['post','get'],'edit/{id}','edit')->name('edit');
        Route::post('status/{id}/{index}','status')->name('status');
    });

// Payroll Controller Controller
Route::namespace(Admin::class)
    ->controller(PayrollsController::class)
    ->prefix('payrolls')->name('payrolls.')
    ->middleware(['auth', 'role:admin'])->group(function () {
        Route::get('index','index')->name('index');
        Route::get('get-attendances/{month?}/{year?}','getAttendances')->name('get-attendances');
        Route::get('get-attendance-lists/{userId}/{month?}/{year?}','getAttendanceLists')->name('getAttendanceLists');
        Route::get('get-estimated-salary/{userId}/{month?}/{year?}', 'getEstimatedSalary')->name('get-estimated-salary');
        Route::get('get-contribution-rate/{userId}/{month?}/{year?}', 'getContributionRate')->name('get-contribution-rate');
    });
