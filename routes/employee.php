<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Employee;

// Employee Custom Routes

Route::get('/', function () {
    return redirect()->route('employee.dashboard.index');
});

// Dashboard Controller
Route::namespace(Employee::class)
    ->controller(DashboardController::class)
    ->prefix('dashboard')->name('dashboard.')
    ->middleware(['auth', 'role:employee'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::get('get-all-holidays', 'getAllHolidays')->name('get-all-holidays');
        Route::get('get-birthdays','getBirthdays')->name('get-birthdays');

});

// Users Controller
Route::namespace(Employee::class)
    ->controller(UsersController::class)
    ->prefix('users')->name('users.')
    ->middleware(['auth', 'role:employee'])->group(function(){
        Route::match(['post', 'get'],'edit', 'edit')->name('edit');
        Route::get('account-setting', 'accountSetting')->name('account-setting');
    });

//Attendances Controller
Route::namespace(Employee::class)
    ->controller(AttendancesController::class)
    ->prefix('attendances')->name('attendances.')
    ->middleware(['auth', 'role:employee'])->group(function(){
        Route::get('index/{year?}/{month?}', 'index')->name('index');
        Route::post('set-attendance', 'setAttendance')->name('set-attendance');
        Route::get('get-reason-data/{id?}/{reportType}', 'getReasonData')->name('get-reason-data');
        Route::get('get-accomplishment-data/{id}', 'getAccomplishmentData')->name('get-accomplishment-data')->whereNumber('id');
    });

// Salary Loans Controller
Route::namespace(Employee::class)
    ->controller(SalaryLoansController::class)
    ->prefix('salary-loans')->name('salary-loans.')
    ->middleware(['auth', 'role:employee'])->group(function(){
        Route::get('index', 'index')->name('index');
        Route::post('add', 'add')->name('add');
        Route::match(['post', 'get'], 'edit/{id}', 'edit')->name('edit')->whereNumber('id');
        Route::delete('delete/{id}', 'delete')->name('delete')->whereNumber('id');
    });

// Salary Deductions Controller
Route::namespace(Employee::class)
    ->controller(SalaryDeductionsController::class)
    ->prefix('salary-deductions')->name('salary-deductions.')
    ->middleware(['auth', 'role:employee'])->group(function(){
        Route::get('index', 'index')->name('index');
        Route::post('add', 'add')->name('add');
        Route::match(['post', 'get'], 'edit/{id}', 'edit')->name('edit')->whereNumber('id');
        Route::delete('delete/{id}', 'delete')->name('delete')->whereNumber('id');
    });

// Payroll Controller
Route::namespace(Employee::class)
    ->controller(PayrollController::class)
    ->prefix('payroll')->name('payroll.')
    ->middleware(['auth', 'role:employee'])->group(function(){
        Route::get('index/{month?}/{year?}', 'index')->name('index');
        Route::get('get-estimated-salary/{month?}/{year?}', 'getEstimatedSalary')->name('get-estimated-salary');
        Route::get('get-contribution-rate/{month?}/{year?}', 'getContributionRate')->name('get-contribution-rate');
        // Route::get('salaryDeductions/{userId}','salaryDeductions')->name('salaryDeductions');
    });