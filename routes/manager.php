<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Manager;

// Manager Custom Routes

Route::get('/', function () {
    return redirect()->route('manager.dashboard.index');
});

// Dashboard Controller
Route::namespace(Manager::class)
    ->controller(DashboardController::class)
    ->prefix('dashboard')->name('dashboard.')
    ->middleware(['auth', 'role:manager'])->group(function () {
        Route::get('index', 'index')->name('index');
});

// Users Controller
Route::namespace(Manager::class)
    ->controller(UsersController::class)
    ->prefix('users')->name('users.')
    ->middleware(['auth', 'role:manager'])->group(function(){
        Route::match(['post', 'get'],'edit', 'edit')->name('edit');
        Route::get('account-setting', 'accountSetting')->name('account-setting');
    });

// Holidays Controller
Route::namespace(Manager::class)
->controller(DashboardController::class)
->prefix('dashboard')->name('dashboard.')
->middleware(['auth', 'role:manager'])->group(function () {
    Route::get('index', 'index')->name('index');
    Route::get('get-all-holidays', 'getAllHolidays')->name('get-all-holidays');

});

// Employees Controller
Route::namespace(Manager::class)
    ->controller(EmployeesController::class)
    ->prefix('employees')->name('employees.')
    ->middleware(['auth', 'role:manager'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::get('get-birthdays','getBirthdays')->name('get-birthdays');
        Route::post('add', 'add')->name('add');
    });

// Employees Attendances Controller
Route::namespace(Manager::class)
    ->controller(EmployeesAttendancesController::class)
    ->prefix('employees-attendances')->name('employees-attendances.')
    ->middleware(['auth', 'role:manager'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::get('get-attendances','getAttendances')->name('get-attendances');
        Route::get('accomplishment/{id}','accomplishment')->name('accomplishment');
        Route::get('reason/{id}','reason')->name('reason');
    });

//Attendances Controller
Route::namespace(Manager::class)
    ->controller(AttendancesController::class)
    ->prefix('attendances')->name('attendances.')
    ->middleware(['auth', 'role:manager'])->group(function(){
        Route::get('index/{year?}/{month?}', 'index')->name('index');
        Route::post('set-attendance', 'setAttendance')->name('set-attendance');
        Route::get('get-reason-data/{id?}/{reportType}', 'getReasonData')->name('get-reason-data');
        Route::get('get-accomplishment-data/{id}', 'getAccomplishmentData')->name('get-accomplishment-data')->whereNumber('id');
    });

    // Salary Loans Controller
Route::namespace(Manager::class)
    ->controller(SalaryLoansController::class)
    ->prefix('salary-loans')->name('salary-loans.')
    ->middleware(['auth', 'role:manager'])->group(function(){
        Route::get('index', 'index')->name('index');
        Route::post('add', 'add')->name('add');
        Route::match(['post', 'get'], 'edit/{id}', 'edit')->name('edit')->whereNumber('id');
        Route::delete('delete/{id}', 'delete')->name('delete')->whereNumber('id');
    });

// Salary Deductions Controller
Route::namespace(Manager::class)
    ->controller(SalaryDeductionsController::class)
    ->prefix('salary-deductions')->name('salary-deductions.')
    ->middleware(['auth', 'role:manager'])->group(function(){
        Route::get('index', 'index')->name('index');
        Route::post('add', 'add')->name('add');
        Route::match(['post', 'get'], 'edit/{id}', 'edit')->name('edit')->whereNumber('id');
        Route::delete('delete/{id}', 'delete')->name('delete')->whereNumber('id');
    });

// Payroll Controller
Route::namespace(Manager::class)
    ->controller(PayrollController::class)
    ->prefix('payroll')->name('payroll.')
    ->middleware(['auth', 'role:manager'])->group(function(){
        Route::get('index/{month?}/{year?}', 'index')->name('index');
        Route::get('get-estimated-salary/{month?}/{year?}', 'getEstimatedSalary')->name('get-estimated-salary');
        Route::get('get-contribution-rate/{month?}/{year?}', 'getContributionRate')->name('get-contribution-rate');
    });

// Employees Payroll Controller Controller
Route::namespace(Manager::class)
    ->controller(EmployeesPayrollController::class)
    ->prefix('employees-payroll')->name('employees-payroll.')
    ->middleware(['auth', 'role:manager'])->group(function () {
        Route::get('index','index')->name('index');
        Route::get('get-attendances/{month?}/{year?}','getAttendances')->name('get-attendances');
        Route::get('get-attendance-lists/{userId}/{month?}/{year?}','getAttendanceLists')->name('getAttendanceLists');
        Route::get('get-estimated-salary/{userId}/{month?}/{year?}', 'getEstimatedSalary')->name('get-estimated-salary');
        Route::get('get-contribution-rate/{userId}/{month?}/{year?}', 'getContributionRate')->name('get-contribution-rate');
    });

// Contributions Controller Controller
Route::namespace(Manager::class)
    ->controller(ContributionsController::class)
    ->prefix('contributions')->name('contributions.')
    ->middleware(['auth', 'role:manager'])->group(function () {
        Route::get('index', 'index')->name('index');
        Route::match(['post','get'], 'add', 'add')->name('add');
        Route::match(['post','get'], 'edit/{id}', 'edit')->name('edit');
        Route::delete('delete/{id}', 'delete')->name('delete');
    });