<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('auth.user.index');
});

// Route::group(['namespace' => 'Auth', 'controller' => 'AuthController', 'prefix' => 'auth', 'as' => 'auth.'], function () {
//     Route::get('index', 'index')->name('index');
// });

// Route::controller(AuthController::class)->prefix('auth')->name('auth.')->group(function () {
//     Route::get('index', 'index')->name('index');
//     Route::post('authenticate', 'authenticate')->name('authenticate');
//     Route::get('logout', 'logout')->name('logout');
// });

// AuthController
Route::prefix('auth')->name('auth.')->group(function () {
    Route::namespace(Auth::class)->controller(AuthController::class)->prefix('user')->name('user.')->group(function () {
        Route::get('/', 'index')->name('index')->middleware(['guest']);
        Route::get('index', 'index')->name('index')->middleware(['guest']);
        Route::post('authenticate', 'authenticate')->name('authenticate')->middleware(['guest']);
        Route::match(['post', 'get'], 'forgot-password', 'forgotPassword')->name('forgot-password')->middleware(['guest']);
        Route::match(['post', 'get'],'password-reset/{token}', 'passwordReset')->name('password-reset')->middleware(['guest']);
        Route::get('logout', 'logout')->name('logout');
    });
});

