-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2023 at 10:39 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bnj_ams_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomplishments`
--

CREATE TABLE `accomplishments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attendance_id` bigint(20) UNSIGNED NOT NULL,
  `content` mediumtext NOT NULL,
  `status` varchar(255) NOT NULL COMMENT 'Submitted is the default value if not work from home, Pending and Approved if work from home',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accomplishments`
--

INSERT INTO `accomplishments` (`id`, `attendance_id`, `content`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 6, '<p>Some text.</p>', 'Approved', '2023-07-18 05:32:03', '2023-07-18 05:32:03', NULL),
(3, 9, '<p>Some text.</p>', 'Approved', '2023-07-18 06:38:55', '2023-07-18 06:38:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `attendance_date` date NOT NULL,
  `timein_am` time DEFAULT NULL,
  `timeout_am` time DEFAULT NULL,
  `timein_pm` time DEFAULT NULL,
  `timeout_pm` time DEFAULT NULL,
  `attendance_total_hours` double DEFAULT NULL,
  `is_holiday` tinyint(1) DEFAULT NULL,
  `is_present` tinyint(1) DEFAULT NULL,
  `is_late` tinyint(1) DEFAULT NULL,
  `is_work_from_home` tinyint(1) NOT NULL DEFAULT 0,
  `is_overtime` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Check if attendance is approved as overtime',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `user_id`, `attendance_date`, `timein_am`, `timeout_am`, `timein_pm`, `timeout_pm`, `attendance_total_hours`, `is_holiday`, `is_present`, `is_late`, `is_work_from_home`, `is_overtime`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 12, '2023-07-03', '09:00:00', '12:00:00', '13:00:00', '17:30:00', 7.5, NULL, NULL, NULL, 0, 0, '0000-00-00 00:00:00', '2023-07-03 01:58:01', '2023-07-17 02:33:04'),
(3, 14, '2023-07-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '0000-00-00 00:00:00', '2023-07-18 05:28:24', '2023-07-17 03:08:09'),
(6, 14, '2023-07-18', '09:00:00', '12:00:00', '13:00:00', '18:00:00', 7.5, NULL, NULL, NULL, 1, 1, '0000-00-00 00:00:00', '2023-07-18 08:09:54', '2023-07-17 03:05:36'),
(7, 14, '2023-07-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2023-07-17 02:01:29', '2023-07-17 03:04:33', NULL),
(8, 14, '2023-06-01', '09:00:00', '12:00:00', '13:00:00', '17:30:00', 7.5, NULL, NULL, NULL, 0, 0, '0000-00-00 00:00:00', '2023-07-17 02:04:24', '2023-07-17 02:04:39'),
(9, 14, '2023-07-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2023-07-18 06:38:55', '2023-07-18 06:38:55', NULL),
(10, 14, '2023-07-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2023-07-18 07:23:44', '2023-07-18 08:09:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_settings`
--

CREATE TABLE `company_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `morning_time_in` time NOT NULL,
  `morning_time_out` time NOT NULL,
  `afternoon_time_in` time NOT NULL,
  `afternoon_time_out` time NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_settings`
--

INSERT INTO `company_settings` (`id`, `morning_time_in`, `morning_time_out`, `afternoon_time_in`, `afternoon_time_out`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '09:00:00', '12:00:00', '13:00:00', '17:30:00', 1, '2023-03-14 03:12:44', '2023-03-20 01:30:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `user_id`, `fullname`, `relation`, `phone_number`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 12, 'Aries Tomas', 'Father', '0935-274-5815', 'Sagana', '2023-03-06 01:00:29', '2023-03-09 05:49:50', NULL),
(4, 14, '0codingislife0', 'Father', '0990-978-9678', '0codingislife0', '2023-03-23 07:37:55', '2023-03-23 07:37:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contributions`
--

CREATE TABLE `contributions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `employer_rate` double(8,5) NOT NULL,
  `employee_rate` double(8,5) NOT NULL,
  `total_percentage` double(8,5) NOT NULL,
  `employee_role_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contributions`
--

INSERT INTO `contributions` (`id`, `name`, `employer_rate`, `employee_rate`, `total_percentage`, `employee_role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'philhealth', 0.02000, 0.02000, 0.04000, 3, '2023-07-03 09:12:33', '2023-07-14 07:45:28', NULL),
(2, 'sss', 0.09500, 0.04500, 0.14000, 3, '2023-07-03 09:13:04', '2023-07-03 09:13:04', NULL),
(3, 'philhealth', 0.02000, 0.02000, 0.04000, 4, '2023-07-03 09:12:33', '2023-07-14 07:45:21', NULL),
(4, 'sss', 0.09500, 0.04500, 0.14000, 4, '2023-07-03 09:13:04', '2023-07-03 09:13:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_roles`
--

CREATE TABLE `employee_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `position` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `basic_salary` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_roles`
--

INSERT INTO `employee_roles` (`id`, `position`, `level`, `basic_salary`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Programmer', 1, 11000, '2023-03-02 02:52:55', '2023-03-02 03:03:06', NULL),
(4, 'Programmer', 2, 16000, '2023-03-03 07:59:27', '2023-03-03 07:59:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `excuses`
--

CREATE TABLE `excuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attendance_id` bigint(20) UNSIGNED NOT NULL,
  `timein_am` time DEFAULT NULL,
  `timeout_am` time DEFAULT NULL,
  `timein_pm` time DEFAULT NULL,
  `timeout_pm` time DEFAULT NULL,
  `excuse_total_hours` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excuses`
--

INSERT INTO `excuses` (`id`, `attendance_id`, `timein_am`, `timeout_am`, `timein_pm`, `timeout_pm`, `excuse_total_hours`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, '2023-07-03 01:58:01', '2023-07-17 02:33:04', NULL),
(2, 2, NULL, NULL, NULL, NULL, NULL, '2023-07-03 02:02:26', '2023-07-03 02:02:26', NULL),
(3, 3, NULL, NULL, NULL, NULL, NULL, '2023-07-14 06:31:37', '2023-07-14 06:31:37', NULL),
(4, 4, NULL, NULL, NULL, NULL, NULL, '2023-07-17 01:25:26', '2023-07-17 01:25:26', NULL),
(5, 5, NULL, NULL, NULL, NULL, NULL, '2023-07-17 01:45:47', '2023-07-17 01:45:47', NULL),
(6, 6, NULL, NULL, NULL, NULL, NULL, '2023-07-17 01:51:04', '2023-07-17 01:51:04', NULL),
(7, 7, NULL, NULL, NULL, NULL, NULL, '2023-07-17 02:01:30', '2023-07-17 02:01:30', NULL),
(8, 8, NULL, NULL, NULL, NULL, NULL, '2023-07-17 02:04:24', '2023-07-17 02:04:24', NULL),
(9, 9, NULL, NULL, NULL, NULL, NULL, '2023-07-18 06:38:55', '2023-07-18 06:38:55', NULL),
(10, 10, NULL, NULL, NULL, NULL, NULL, '2023-07-18 07:23:44', '2023-07-18 07:23:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `holiday_name` varchar(255) NOT NULL,
  `holiday_date` date NOT NULL,
  `holiday_type` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  `rate` double(8,2) NOT NULL,
  `is_per_year` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`id`, `holiday_name`, `holiday_date`, `holiday_type`, `color`, `rate`, `is_per_year`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'New Year', '2020-01-01', 'Special Non Working Holiday', '#35DDEF', 200.00, 1, '2023-02-28 02:55:45', '2023-03-15 02:44:39', NULL),
(9, 'Christmas', '2022-12-25', 'Special Non Working Holiday', '#E72856', 200.00, 1, '2023-03-01 06:48:38', '2023-03-06 05:43:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_attendances`
--

CREATE TABLE `home_attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `accomplishment_id` bigint(20) UNSIGNED NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time NOT NULL,
  `total_work_hours` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_attendances`
--

INSERT INTO `home_attendances` (`id`, `accomplishment_id`, `time_in`, `time_out`, `total_work_hours`, `created_at`, `updated_at`) VALUES
(4, 2, '09:00:00', '12:00:00', 3, '2023-07-18 05:33:27', '2023-07-18 05:33:27'),
(5, 2, '13:00:00', '17:30:00', 4.5, '2023-07-18 05:33:27', '2023-07-18 05:33:27'),
(6, 3, '09:00:00', '12:00:00', 3, '2023-07-18 06:38:55', '2023-07-18 06:38:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_02_22_012145_create_profiles_table', 1),
(6, '2023_02_22_012418_create_attendances_table', 1),
(7, '2023_02_22_012501_create_contacts_table', 1),
(8, '2023_02_22_012601_create_holidays_table', 1),
(9, '2023_02_22_012706_create_excuses_table', 1),
(10, '2023_02_22_012730_create_quotes_table', 1),
(11, '2023_02_22_012809_create_reasons_table', 1),
(12, '2023_02_22_013341_create_company_settings_table', 1),
(13, '2023_02_22_014304_create_employee_roles_table', 1),
(14, '2023_02_22_014649_create_accomplishments_table', 1),
(15, '2023_03_20_094119_create_home_attendances_table', 1),
(16, '2023_03_29_102600_create_contributions_table', 1),
(17, '2023_03_29_102711_create_salary_loans_table', 1),
(18, '2023_04_04_090212_create_salary_deductions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `employee_role_id` bigint(20) UNSIGNED NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `employee_id_number` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `date_employed` date NOT NULL,
  `employment_status` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `has_contributions` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `employee_role_id`, `firstname`, `middlename`, `lastname`, `employee_id_number`, `birthdate`, `date_employed`, `employment_status`, `contact_number`, `address`, `has_contributions`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 12, 3, 'retxej112600', 'retxej112600', 'retxej112600', '19100301', '2023-03-06', '2023-03-06', 'Regular', '0921-835-6618', 'Sagana', 1, '2023-03-06 01:00:29', '2023-07-17 05:08:52', NULL),
(5, 14, 4, '0codingislife0', '0codingislife0', '0codingislife0', '123454654647', '2023-03-23', '2023-03-23', 'Regular', '0990-978-6785', '0codingislife0', 1, '2023-03-23 07:37:55', '2023-07-17 05:20:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quotes` text NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `quotes`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(27, 'Time is Gold', 1, '2023-03-02 02:06:22', '2023-03-13 02:47:56', NULL),
(29, 'Think All Possibilities', 0, '2023-03-06 01:17:17', '2023-03-06 03:59:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reasons`
--

CREATE TABLE `reasons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attendance_id` bigint(20) UNSIGNED NOT NULL,
  `content` mediumtext NOT NULL,
  `report_type` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_deductions`
--

CREATE TABLE `salary_deductions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `salary_loan_id` bigint(20) UNSIGNED NOT NULL,
  `deduction` double NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salary_deductions`
--

INSERT INTO `salary_deductions` (`id`, `salary_loan_id`, `deduction`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 4, 1000, '2023-07-17', '2023-07-17 06:36:02', '2023-07-17 06:36:02', NULL),
(3, 6, 1200, '2023-07-17', '2023-07-17 06:40:02', '2023-07-17 06:40:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `salary_loans`
--

CREATE TABLE `salary_loans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `remainder` double NOT NULL,
  `balance` double NOT NULL,
  `reason` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salary_loans`
--

INSERT INTO `salary_loans` (`id`, `user_id`, `amount`, `remainder`, `balance`, `reason`, `status`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 14, 1000, 0, 1000, 'Pang bili ng sapatos', 'Completed', '2023-07-17', '2023-07-17 06:35:13', '2023-07-17 06:35:36', NULL),
(4, 14, 0, 0, 0, 'N/A', 'Deduction', '2023-07-17', '2023-07-17 06:36:02', '2023-07-17 06:36:02', NULL),
(5, 12, 1200, 0, 1200, 'Pang bili ng mouse', 'Completed', '2023-07-17', '2023-07-17 06:37:39', '2023-07-17 06:39:54', NULL),
(6, 12, 0, 0, 0, 'N/A', 'Deduction', '2023-07-17', '2023-07-17 06:40:02', '2023-07-17 06:40:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `password`, `token`, `role`, `image`, `active`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'jextertomas', 'jextertomas@gmail.com', NULL, '$2y$10$5QN3RkH3nYCnK7LDaQ8gxejbpPOo5kCxbvY4Z/CPFu/4JAbaMoYT2', '63fffac12388b63fffac12389263fffac12389663fffac12389863fffac12389b', 'admin', NULL, 1, NULL, '2023-02-27 01:27:56', '2023-07-17 05:14:30', NULL),
(12, 'retxej112600', 'retxej112600@gmail.com', NULL, '$2y$10$H4aDLLYO./BZBwWaCr/6C.Lvw6b3Dnq33FMZ2FUX9GwO2e0UROROC', NULL, 'employee', NULL, 1, NULL, '2023-03-06 01:00:29', '2023-07-17 05:15:13', NULL),
(14, '0codingislife0', '0.coding.is.life.0@gmail.com', NULL, '$2y$10$HpFwB/WgimrX.kaH8ktAvualQuYcHWHRIk9PyJ6MyyDvMEHDPA6N6', NULL, 'manager', NULL, 1, NULL, '2023-03-23 07:37:55', '2023-07-17 05:20:39', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accomplishments`
--
ALTER TABLE `accomplishments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_settings`
--
ALTER TABLE `company_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contributions`
--
ALTER TABLE `contributions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_roles`
--
ALTER TABLE `employee_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excuses`
--
ALTER TABLE `excuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_attendances`
--
ALTER TABLE `home_attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reasons`
--
ALTER TABLE `reasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_deductions`
--
ALTER TABLE `salary_deductions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_loans`
--
ALTER TABLE `salary_loans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accomplishments`
--
ALTER TABLE `accomplishments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `company_settings`
--
ALTER TABLE `company_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contributions`
--
ALTER TABLE `contributions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employee_roles`
--
ALTER TABLE `employee_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `excuses`
--
ALTER TABLE `excuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `home_attendances`
--
ALTER TABLE `home_attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `reasons`
--
ALTER TABLE `reasons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_deductions`
--
ALTER TABLE `salary_deductions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `salary_loans`
--
ALTER TABLE `salary_loans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
