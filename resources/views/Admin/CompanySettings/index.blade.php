@extends('Admin.layout.default')
@section('title', 'Admin.Attendance-Settings')
@section('content')
    {{-- Modal --}}
    <link rel="stylesheet" href="{{url('public/timepicker/css/timepicker.css')}}">
    <link rel="stylesheet" href="{{url('public/timepicker/css/timepicker.min.css')}}">
    <script src="{{url('public/timepicker/js/timepicker.js')}}"></script>
    <script src="{{url('public/timepicker/js/timepicker.min.js')}}"></script>

    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-md">
            <form action="" method="" id="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                        <div class="modal-header">
                        <h4 class="modal-title" id="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <label for="morning-time-in">Morning Time In</label>
                                <div class="form-group">
                                    <div class="input-group date" id="date-morning-time-in" data-target-input="nearest">
                                        <input type="text" name="morning_time_in" class="form-control datetimepicker-input rounded-0"
                                               id="morning-time-in" data-target="#date-morning-time-in" required>
                                        <div class="input-group-append" data-target="#date-morning-time-in"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <label for="morning-time-out">Morning Time out</label>
                                <div class="form-group">
                                    <div class="input-group date" id="date-morning-time-out" data-target-input="nearest">
                                        <input type="text" name="morning_time_out" class="form-control datetimepicker-input rounded-0"
                                               data-target="#date-morning-time-out"  id="morning-time-out" required>
                                        <div class="input-group-append" data-target="#date-morning-time-out"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <label for="afternoon-time-in">Afternoon Time in</label>
                                <div class="form-group">
                                    <div class="input-group date" id="date-afternoon-time-in" data-target-input="nearest">
                                        <input type="text" name="afternoon_time_in" class="form-control datetimepicker-input rounded-0"
                                               id="afternoon-time-in"  data-target="#date-afternoon-time-in" required>
                                        <div class="input-group-append" data-target="#date-afternoon-time-in"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <label for="afternoon-time-out">Afternoon Time out</label>
                                <div class="form-group">
                                    <div class="input-group date" id="date-afternoon-time-out" data-target-input="nearest">
                                        <input type="text" name="afternoon_time_out" data-target="#date-afternoon-time-out"
                                               class="form-control datetimepicker-input rounded-0" id="afternoon-time-out" required>
                                        <div class="input-group-append" data-target="#date-afternoon-time-out"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text">
                                                <i class="fa fa-clock"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <!-- checkbox -->
                                <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                        <input type="checkbox" id="active" name="active" value="1">
                                        <label for="active">
                                            Set as Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0" id="save-button"></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Datatable --}}
    <div class="card card-primary card-outline">
        <div class="card-header">
            <button class="btn btn-primary col-2 float-right rounded-0" id="add"> New Attendance Setting </button>
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th colspan="2">Morning</th>
                        <th colspan="2">Afternoon</th>
                        <th rowspan="2" colspan="1">Active</th>
                        <th rowspan="2" colspan="1">Action</th>
                    </tr>
                    <tr>
                        <th>Time In</th>
                        <th>Time Out</th>
                        <th>Time In</th>
                        <th>Time Out</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- Script --}}
    <script>
        $(function() {
            var url = '';

            $('#date-morning-time-in, #date-morning-time-out, #date-afternoon-time-in, #date-afternoon-time-out')
                .datetimepicker({
                    use24hours: true,
                    format: 'HH:mm',
                    pickDate: false,
                });

           $('input[type="text"]').on('keypress keyup keydown',function (e) {
               if(!e.key.match(/^([0-9\:]{1,})$/) || this.value.length > 4){
                    e.preventDefault();
               }
           });

            // Click Add
            $(document).on('click', '#add', function() {
                url = base_url + 'admin/company-settings/add';
                $('#save-button').html('Submit');
                $('#modal-title').html('Add Company Setting');
                $('#modal').modal('show');
            });

            // Datatable
            var table = $("#datatable").DataTable({
                order: [ [0, 'Asc'] ],
                responsive: true,
                language: {
                    emptyTable: 'No Company Settings data found'
                },
                ajax: {
                    url: base_url + 'admin/company-settings/get-company-settings',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        $('#datatable > tbody').html(
                            '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                    }
                },
                createdRow: function( row, data, dataIndex ) {

                },
                columns: [
                    { data: 'morning_time_in'},
                    { data: 'morning_time_out'},
                    { data: 'afternoon_time_in'},
                    { data: 'afternoon_time_out'},
                    { data: 'active'},
                    { data: 'id'}
                ],
                columnDefs: [
                    {
                        targets: 5,
                        data: null,
                        orderable:false,
                        render: function(data, type, row) {
                            var active = row.active ? `<a data-id="${row.id}" title="Active" style="cursor: pointer;" class="dropdown-item set-as-active" status="Inactive"><i class="fas fa-toggle-on" aria-hidden="true"></i> Disable</a>` :
                               `<a data-id="${row.id}" title="Inactive" style="cursor: pointer;" class="dropdown-item set-as-active" status="Active"><i class="fas fa-toggle-off" aria-hidden="true"></i> Enable</a>`;


                            return `<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-flat">Action</button>
                                <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">

                                    <a data-id="${row.id}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                                    <a data-id="${row.id}" title="Delete" style="cursor: pointer;" class="dropdown-item delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>

                                    ${active}
                                </div>
                            </div>`;
                        }
                    },
                    {
                        targets: 4,
                        data: null,
                        orderable:false,
                        render: function(data, type, row) {
                            return row.active ? 'Active': 'Not Active';
                        }
                    }
                ],
            });

            // Form submit
            $(document).on('submit', '#form', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                if(url === base_url + 'admin/company-settings/add'){
                    $('#save-button').html('Saving <i class="fas fa-spinner"></i>');
                }else{
                    $('#save-button').html('Updating <i class="fas fa-spinner"></i>');
                }
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    type: 'json',
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $('#modal').modal('hide');
                        response(data.message, data.result);
                        table.ajax.reload(null, false);
                    },
                    error: function(xhr, status, error) {
                        if(url === base_url + 'admin/company-settings/add'){
                    $('#save-button').html('Save');
                }else{
                    $('#save-button').html('Update Changes');
                }
                        var title = '';
                        var validation = JSON.parse(xhr.responseText);
                        title = validation.errors ? validation.errors[Object.keys(validation
                            .errors)[0]] : validation.title;
                        response(title, 'error');
                        table.ajax.reload();
                    }
                });
            });

            // Click Edit
            table.on('click', '.edit', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                url = base_url + 'admin/company-settings/edit/' + dataId;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        $('#morning-time-in').val((data.morning_time_in).substring(0,5));
                        $('#morning-time-out').val((data.morning_time_out).substring(0,5));
                        $('#afternoon-time-in').val((data.afternoon_time_in).substring(0,5));
                        $('#afternoon-time-out').val((data.afternoon_time_out).substring(0,5));
                        $('#level').val(data.level);
                        $('#basic_salary').val(data.basic_salary);
                        $('#save-button').html('Update Changes');
                        $('#modal-title').html('Edit Company Settings');
                        $('#modal').modal('show');
                    },
                    error: function(xhr, status, error) {
                        table.ajax.reload();
                        response(error, 'error');
                    }
                });
            });

            // Delete Function
            table.on('click', '.delete', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = base_url + 'admin/company-settings/delete/' + dataId;
                Swal.fire({
                    title: 'Delete Company Setting?',
                    text: 'Are You Sure you want to delete this Company Setting?',
                    icon: 'warning',
                    animation: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                }).then(function(result) {
                    if (result.isConfirmed) {

                        $.ajax({
                            url: href,
                            type: 'DELETE',
                            method: 'DELETE',
                            dataType: 'JSON',
                            beforeSend: function () {
                                Swal.fire({
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                    }
                                });
                            },
                            success: function(data, textStatus, jqXHR) {
                                Swal.close();
                                table.ajax.reload();
                                response(data.message, data.result);
                            },
                            error: function(xhr, status, error) {
                                table.ajax.reload();
                                response(error, 'error');
                            }
                        });

                    }
                });
            });

            // Click Active
            table.on('click', '.set-as-active', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = base_url + 'admin/company-settings/active/' + dataId;
                var status = $(this).attr('status');
                Swal.fire({
                    title: 'Set as '+status+'?',
                    text: null,
                    icon: 'info',
                    animation: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                }).then(function(result) {
                    if (result.isConfirmed) {

                        $.ajax({
                            url: href,
                            type: 'POST',
                            method: 'POST',
                            dataType: 'JSON',
                            beforeSend: function () {
                                Swal.fire({
                                    title: 'Updating...',
                                    timer: 10000,
                                    animate:false,
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                        const b = Swal.getHtmlContainer().querySelector('b');
                                        timerInterval = setInterval(() => {
                                            b.textContent = Swal.getTimerLeft()
                                        }, 100);
                                    },
                                    willClose: () => {
                                        clearInterval(timerInterval);
                                    }
                                });
                            },
                            success: function(data, textStatus, jqXHR) {
                                table.ajax.reload();
                                response(data.message, data.result);
                            },
                            error: function(xhr, status, error) {
                                table.ajax.reload();
                                response(error, 'error');
                            }
                        });

                    }
                });
            });

            // Hidden modal
            $('#modal').on('hidden.bs.modal', function() {
                url = '';
                $('#save-button').html('');
                $('#modal-title').html('');
                $('#form')[0].reset();
            });

            // Sweetalert
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            function response(title, icon) {
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }

        });
    </script>
@endsection
