@extends('Admin.layout.default')
@section('title', 'Admin.Quotes')
@section('content')

    {{-- Modal --}}
    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <form action="" method="" id="form">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- Content Here --}}
                        <label for="">Quote Name</label>
                        <div class="col-lg-12">
                            <div class="input-group">
                                <input type="text" name="quotes" class="form-control rounded-0" id="quotes" placeholder="Quote">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="is_active" value="1"
                                    id="is_active">
                                <label for="is_active" class="custom-control-label">Set as active</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0" id="save-button"></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Datatable --}}
    <div class="card card-primary card-outline">
        <div class="card-header">
            <button class="btn btn-primary col-2 float-right rounded-0" id="add"> New Quote </button>
        </div>
        <div class="card-body">

            <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Quote</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- Script --}}
    <script>
        $(function() {
            var url = '';

            // Datatable
            var datatable_instance = $("#datatable").DataTable({
                "order": [
                    [0, 'Asc']
                ],
                "responsive": true,
                "language": {
                    "emptyTable": "No Quotes Data found"
                },
                "ajax": {
                    url: base_url + 'admin/quotes/get-all-quotes',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        $('#datatable > tbody').html(
                            '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                    },
                },
                initComplete: function() {
                    var api = this.api();
                    var data = api.rows().data();

                    // Filter out hidden rows from the DataTable's dataset
                    var visibleData = data.filter(function(row) {
                        return row.id !== 27;
                    });

                    // Clear the existing data and redraw the DataTable with the visible data
                    api.clear().rows.add(visibleData).draw();
                },
                "columns": [{
                    data: 'quotes'
                }, {
                    data: 'is_active'
                }, {
                    data: 'id'
                }],
                "columnDefs": [{
                    targets: 1,
                    data: null,
                    render: function(data, type, row) {
                        if (data == 0) {
                            return 'Inactive';
                        } else {
                            return 'Active';
                        }
                    }
                }, {
                    targets: 2,
                    data: null,
                    render: function(data, type, row) {
                        if (row.is_active == true) {
                            return `<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-flat">Action</button>
                                <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a data-id="${row.id}" title="Active" style="cursor: pointer;" class="dropdown-item set-as-active" data-process="off"><i class="fas fa-toggle-on" aria-hidden="true"></i> Disable</a>

                                    <a data-id="${row.id}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                                    <a data-id="${row.id}" title="Delete" style="cursor: pointer;" class="dropdown-item delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                </div>
                            </div>`;
                        } else {
                            return `<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-flat">Action</button>
                                <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a data-id="${row.id}" title="Active" style="cursor: pointer;" class="dropdown-item set-as-active" data-process="on"><i class="fas fa-toggle-off" aria-hidden="true"></i> Enable</a>

                                    <a data-id="${row.id}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                                    <a data-id="${row.id}" title="Delete" style="cursor: pointer;" class="dropdown-item delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                </div>
                            </div>`;
                        }
                    }
                }],
                "rowCallback": function(row, data, index) {
                    if(data.id == 27){
                        $(row).hide();
                    }
                }
            });

            // Click Add
            $(document).on('click', '#add', function() {
                url = base_url + 'admin/quotes/add';
                $('#save-button').html('Save');
                $('#modal-title').html('Add New Quote');
                $('#modal').modal('show');
            });

            // Click Edit
            datatable_instance.on('click', '.edit', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                url = base_url + 'admin/quotes/edit/' + dataId;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        $('#quotes').val(data.quotes);
                        $('#is_active').prop('checked', data.is_active);
                        $('#save-button').html('Update Changes');
                        $('#modal-title').html('Edit Quote');
                        $('#modal').modal('show');
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.url(base_url + 'admin/quotes/get-all-quotes')
                            .load();
                        response(error, 'error');
                    }
                });
            });

            // Form submit
            $(document).on('submit', '#form', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                if(url === base_url + 'admin/quotes/add'){
                    $('#save-button').html('Saving <i class="fas fa-spinner"></i>');
                }else{
                    $('#save-button').html('Updating <i class="fas fa-spinner"></i>');
                }
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    type: 'json',
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $('#modal').modal('hide');
                        response(data.title, data.icon);
                        datatable_instance.ajax.url(base_url + 'admin/quotes/get-all-quotes')
                            .load();
                    },
                    error: function(xhr, status, error) {
                        if(url === base_url + 'admin/quotes/add'){
                            $('#save-button').html('Save');
                        }else{
                            $('#save-button').html('Update Changes');
                        }
                        var title = '';
                        var validation = JSON.parse(xhr.responseText);
                        title = validation.errors ? validation.errors[Object.keys(validation
                            .errors)[0]] : validation.title;
                        response(title, 'error');
                        datatable_instance.ajax.url(base_url + 'admin/quotes/get-all-quotes')
                            .load();
                    }
                });
            });

            // Hidden modal
            $('#modal').on('hidden.bs.modal', function() {
                url = '';
                $('#save-button').html('');
                $('#modal-title').html('');
                $('#form')[0].reset();
            });

            // Onclick active
            datatable_instance.on('click', '.set-as-active', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var dataProcess = $(this).attr('data-process');
                // var href = 'isActive/'+dataId;
                var href = base_url + 'admin/quotes/is-active/' + dataId + '/' + dataProcess;
                $.ajax({
                    url: href,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        datatable_instance.ajax.url(base_url + 'admin/quotes/get-all-quotes')
                            .load();
                        response(data.title, data.icon);
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.url(base_url + 'admin/quotes/get-all-quotes')
                            .load();
                        response(error, 'error');
                    }
                })
            });

            // Delete Function
            datatable_instance.on('click', '.delete', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = base_url + 'admin/quotes/delete/' + dataId;
                Swal.fire({
                    title: 'Delete Quote?',
                    text: 'Are You Sure you want to delete this Quote?',
                    icon: 'warning',
                    animation: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                }).then(function(result) {
                    if (result.isConfirmed) {
                        let timerInterval;
                        Swal.fire({
                            title: 'Deleting!',
                            timer: 10000,
                            animation: false,
                            timerProgressBar: false,
                            didOpen: () => {
                                Swal.showLoading();
                                const b = Swal.getHtmlContainer().querySelector('b');
                                timerInterval = setInterval(() => {
                                    b.textContent = Swal.getTimerLeft()
                                }, 100);

                                $.ajax({
                                    url: href,
                                    type: 'DELETE',
                                    method: 'DELETE',
                                    dataType: 'JSON',
                                    success: function(data, textStatus, jqXHR) {
                                        Swal.close();
                                        datatable_instance.ajax.url(
                                            base_url +
                                            'admin/quotes/get-all-quotes'
                                            ).load();
                                        response(data.title, data.icon);
                                    },
                                    error: function(xhr, status, error) {
                                        datatable_instance.ajax.url(
                                            base_url +
                                            'admin/quotes/get-all-quotes'
                                            ).load();
                                        response(error, 'error');
                                    }
                                });
                            },
                            willClose: () => {
                                clearInterval(timerInterval);
                            }
                        });
                    }
                });
            });

            // Sweetalert
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            function response(title, icon) {
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }
        });
    </script>
@endsection
