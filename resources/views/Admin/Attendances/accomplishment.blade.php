<?php

use Dompdf\Dompdf;

$attendance_date = $attendance->attendance_date;
$profile_name = $attendance->user->profile->firstname.' '.$attendance->user->profile->middlename.' '.$attendance->user->profile->lastname;

class XPDF extends Dompdf{

}

$html = '';
// instantiate and use the dompdf class
$dompdf = new XPDF();

$html .= '<h1>Accomplishments</h1>';
$html .= '<small style="position: absolute; float: right; top: 2.0em;">'.$profile_name.'</small>';
$html .= '<small style="position: relative; float: right; bottom: 1.8em;">'.$attendance_date.'</small>';

$html .='<div style="width::100%; height: auto; padding: 1em;">';
$html .='<ul style="width: 50%; float: left;">';
$html .='<li>Regular Schedule</li>';
$html .='<li>'.($attendance->timein_am).' - '.($attendance->timeout_am).'</li>';
$html .='<li>'.($attendance->timein_pm).' - '.($attendance->timeout_pm).'</li>';
$html .='</ul>';
$html .='<ul style="width: 50%; float: right;">';
$html .='<li>Work Form Home</li>';
foreach ($attendance->accomplishment->homeAttendances as $homeAttendance){
    $html .='<li>'.($homeAttendance->time_in).' - '.($homeAttendance->time_out).'</li>';
}

$html .='</ul>';
$html .='</div>';

$html .= '<div style="width: 100%; height: auto; padding:1em;">';
$html .= $attendance->accomplishment->content;
$html .= '</div>';

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('letter', 'portrait');

$dompdf->addInfo('Title', $attendance->user->profile->firstname);
// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream(uniqid().'.pdf',['Attachment' => false]);

exit;
