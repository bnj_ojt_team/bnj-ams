@extends('Admin.layout.default')
@section('title', 'Admin.Work From Home Attendances')
@section('content')

	<div class="card card-primary card-outline">
		<!-- /.card-header -->
		<div class="card-body">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-sm-12">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<div class="table-responsive">
						<table id="datatable" class="table table-bordered table-striped">
							<thead class="text-center">
								<tr>
									<th>Date</th>
									<th>Name</th>
									<th>Reports</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.card-body -->
	</div>

	<script type="text/javascript">

		$(function(){
			// Datatable
			var datatable_instance = $("#datatable").DataTable({
				"order": [
					[0, 'Asc']
				],
				"search": {
					"regex": true
				},
				"dom": "lBrtip",
				"responsive": true,
				"language": {
					"emptyTable": "No Attendances Data found!"
				},
				"ajax": {
					url: base_url + 'admin/attendances/home-attendances',
					method: 'GET',
					dataType: 'JSON',
					beforeSend: function() {
						$('#datatable > tbody').html(
							'<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
						);
					},
	                complete: function() {
	                    // Enable export buttons after the AJAX request is complete
	                    datatable_instance.buttons().enable();
	                }
				},
				initComplete: function() {
					// Apply the search
					var api = this.api();

					const year = $('#year');
					const month = $('#month');
					var val = '';

					year.yearpicker({
						onShow: null,
						onChange: function(value) {
							if (value != null) {
								if (month.val()) {
									val = value + '/' + month.val();
								} else {
									val = value + '/';
								}
								return api.columns([1]).search(val, true, true).draw();
							}
						},
						markAsBold: true,
						allowCustomValue: true,
					});

					month.on('change', function() {
						val = '/' + this.value + '/';

						if (year.val() && this.value.length == 0) {
							val = year.val() + '/';
							return api.columns([1]).search(val, true, true).draw();
						}

						if (year.val()) {
							val = year.val() + '/' + this.value;
							return api.columns([1]).search(val, true, true).draw();
						}
						return api.columns([1]).search(val, true, true).draw();

						//return api.columns([2]).search(this.value, true, false).draw();
					});

					year.on('keyup keydown', function(e) {

						if (e.key.match(/^([0-9]{1,})$/g) || e.keyCode == 8) {
							if (month.val()) {
								val = this.value + '/' + month.val();
							} else {
								val = this.value + '/';
							}
							return api.columns([1]).search(val, true, true).draw();
						} else {
							return e.preventDefault();
						}

					});

					$('#profiles').on('change', function() {
						return api.columns([0]).search((this.value)? '^' + (this.value) + '$': '' , true, false).draw();
					});

				},
				buttons: [
				    {
						extend: 'excelHtml5',
						attr: {
							id: 'excel',
						},
						title: function filename() {
	                        var profiles = ($('#profiles').val())? $('#profiles').val(): 'Attendances';
	                        var year = ($('#year').val())? $('#year').val(): moment().format('Y');
	                        var month = ($('#month').val())? $('#month option:selected').text(): moment().format('MMMM');
	                        return profiles+' '+month+' '+year;
	                    },
						text: '<i class="far fa-file-excel"></i> .xlsx',
						tag: 'button',
						className: 'btn btn-flat btn-success rounded-bottom border-top-0 border-default px-5',
						exportOptions: {
							columns: [0, 1]
						},
						action: function(e, dt, node, config) {
							Swal.fire({
								title: 'Export To Excel',
								text: 'Are You Sure?',
								icon: 'info',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Yes'
							}).then(function(result) {
								if (result.isConfirmed) {
									setTimeout(function() {
										$.fn.dataTable.ext.buttons.excelHtml5.action
											.call(dt.button(this), e, dt, node,
											config);
									}, 1000);
								}
							});
						},
						footer: true
					},
				],
				"columns": [
					{
						data: 'id',
						render: function(data, type, row) {
							return row.attendance.attendance_date;
						}
					},
					{
						data: 'id',
						render: function(data, type, row) {
							return `
									${row.attendance.user.profile.lastname}, 
									${row.attendance.user.profile.firstname}, 
									${row.attendance.user.profile.middlename}
							`;
						}
					},
					{
						data: 'id',
						render: function(data, type, row) {

							var accomplishment = '';
							if (data) {
								accomplishment += '<a data-id="' + row.attendance.id +
									'" title="View Accomplishment" class="btn btn-danger accomplishments">' +
									'<i class="fa fa-file-pdf"></i></a>';
							}

							var reason = '';
							$.map(row.attendance.reason, function(data) {
								reason += ' | <a data-id="' + data.id + '" title="' + (data
										.report_type) + '" class="btn btn-info reason">' +
									'<i class="fa fa-file-word"></i></a>';
							});

							return accomplishment + reason;
						}
					},
					{
						data: 'id',
						render: function(data, type, row){
							return row.status;
						}
					},
					{
						data: 'id',
						render: function(data, type, row) {
	                    	return `<div class="btn-group">
	                                <button type="button" class="btn btn-primary btn-flat">Action</button>
	                                <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
	                                <span class="sr-only">Toggle Dropdown</span>
	                                </button>
	                                <div class="dropdown-menu" role="menu">
	                                    <a data-id="${row.id}" title="Work From Home" style="cursor: pointer;" class="dropdown-item update-attendance"> ${(row.status != 'Approved') ? '<i class="fas fa-thumbs-up"></i> Approve':'<i class="fas fa-thumbs-down"></i> Disapprove'} </a>
	                                </div>
	                            </div>`;
	                    }
					},
				],
			});

			// Click Update Overtime
            datatable_instance.on('click', '.update-attendance', function(e){
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                // var href = 'isActive/'+dataId;
                var href = `${base_url}admin/attendances/update-attendance/${dataId}`;
                $.ajax({
                    url: href,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        datatable_instance.ajax.reload();
                        response(data.title, data.icon);
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.reload();
                        response(error, 'error');
                    }
                })
            });

			// Accomplishment Function
			datatable_instance.on('click', '.accomplishments', function(e) {
				e.preventDefault();
				var dataId = $(this).attr('data-id');
				var href = base_url + 'admin/attendances/accomplishment/' + dataId;
				Swal.fire({
					title: 'View Accomplishment?',
					text: 'Are You Sure?',
					icon: 'info',
					animation: true,
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes',
					closeOnConfirm: false,
					closeOnCancel: false,
				}).then(function(result) {
					if (result.isConfirmed) {
						window.open(href);
					}
				});
			});

			// Reason Function
			datatable_instance.on('click', '.reason', function(e) {
				e.preventDefault();
				var dataId = $(this).attr('data-id');
				var href = base_url + 'admin/attendances/reason/' + dataId;
				Swal.fire({
					title: 'View Reason?',
					text: 'Are You Sure?',
					icon: 'info',
					animation: true,
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes',
					closeOnConfirm: false,
					closeOnCancel: false,
				}).then(function(result) {
					if (result.isConfirmed) {
						window.open(href);
					}
				});
			});

			// Sweetalert
			var Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 4000
			});

			function response(title, icon) {
				Toast.fire({
					icon: icon,
					title: title
				})
			}
		});
			
	</script>

@endsection