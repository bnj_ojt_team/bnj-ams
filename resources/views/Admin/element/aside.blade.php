<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light">{{ ucwords(__('BNJ-AMS')) }}</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ (auth()->user()->image == null) ? asset('preview.png'):asset('storage/avatars/'.auth()->user()->image) }}" class="img-circle elevation-2" alt="User Image" style="height: 2.1rem; width: 2.1rem;">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ ucfirst(__('Admin')) }}</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
            data-accordion="false">
                <li class="nav-header">{{ strtoupper(__('generals')) }}</li>
                <li class="nav-item">
                    <a href="{{ route('admin.dashboard.index') }}" class="nav-link {{ (url()->current() == route('admin.dashboard.index')) ? "active":"" }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{ ucfirst(__('Dashboard')) }}
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.employees.index') }}" class="nav-link {{url()->current() == route('admin.employees.index') ? 'active': ''}}">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            {{ ucfirst(__('Employees')) }}
                        </p>
                    </a>
                </li>

                <li class="nav-item {{ (
                        in_array(url()->current(),[
                           route('admin.attendances.index'),
                           route('admin.attendances.today-overtime-attendances'),
                           route('admin.attendances.home-attendances'),
                       ])
                       ) ? 'menu-is-opening menu-open':'' }}">
                    <a href="#" class="nav-link {{ (
                        in_array(url()->current(),[
                           route('admin.attendances.index'),
                           route('admin.attendances.today-overtime-attendances'),
                           route('admin.attendances.home-attendances'),
                       ])
                       ) ? 'active':'' }}">
                        <i class="nav-icon fas fa-clock"></i>
                        <p>
                            {{ ucfirst(__('Attendance')) }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.attendances.index') }}" class="nav-link {{ (url()->current() == route('admin.attendances.index')) ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('Attendances')) }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.attendances.today-overtime-attendances') }}" class="nav-link {{ (url()->current() == route('admin.attendances.today-overtime-attendances')) ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('OT Attendances')) }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.attendances.home-attendances') }}" class="nav-link {{ (url()->current() == route('admin.attendances.home-attendances')) ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('Work From Home')) }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.salary-loans.index') }}" class="nav-link {{route('admin.salary-loans.index') == url()->current()? 'active': ''}}">
                        <i class="nav-icon fas fa-hand-holding-usd"></i>
                        <p>
                            {{ ucfirst(__('Loan')) }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.payrolls.index') }}" class="nav-link {{route('admin.payrolls.index') == url()->current()? 'active': ''}}">
                        <i class="nav-icon fas fa-money-check-alt"></i>
                        <p>
                            {{ ucfirst(__('Payroll')) }}
                        </p>
                    </a>
                </li>
                <li class="nav-header">{{ strtoupper(__('Settings')) }}</li>
                <li class="nav-item">
                    <a href="{{ route('admin.users.index') }}" class="nav-link {{url()->current() == route('admin.users.index') ? 'active': ''}}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            {{ ucfirst(__('Users')) }}
                        </p>
                    </a>
                </li>

                <li class="nav-item {{ (
                        in_array(url()->current(),[
                           route('admin.employee-roles.index'),
                           route('admin.holidays.index'),
                           route('admin.quotes.index'),
                           route('admin.company-settings.index'),
                           route('admin.contributions.index'),
                       ])
                       ) ? 'menu-is-opening menu-open':'' }}">
                    <a href="#" class="nav-link {{ (
                        in_array(url()->current(),[
                           route('admin.employee-roles.index'),
                           route('admin.holidays.index'),
                           route('admin.quotes.index'),
                           route('admin.company-settings.index'),
                           route('admin.contributions.index'),
                       ])
                       ) ? 'active':'' }}">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            {{ ucfirst(__('System Settings')) }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.contributions.index') }}" class="nav-link {{ (url()->current() == route('admin.contributions.index')) ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('Contributions')) }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.holidays.index') }}" class="nav-link {{ (url()->current() == route('admin.holidays.index')) ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('Holidays')) }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.quotes.index') }}" class="nav-link {{ (url()->current() == route('admin.quotes.index')) ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('Quotes')) }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.company-settings.index')}}" class="nav-link {{url()->current() == route('admin.company-settings.index')? 'active': ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('Attendance Settings')) }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.employee-roles.index') }}" class="nav-link {{ (url()->current() == route('admin.employee-roles.index')) ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('Employee Roles')) }}</p>
                            </a>
                        </li>                
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.database.backup') }}" class="nav-link {{ (url()->current() == route('admin.database.backup')) ? "active":"" }}">
                        <i class="nav-icon fas fa-database"></i>
                        <p>
                            {{ ucfirst(__('Database Backups')) }}
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
