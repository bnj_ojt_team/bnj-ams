<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="javascript:void(0)" id="clock" style="font-weight: bolder;" class="nav-link active">
                <script>
                    var date = moment().format('H:mm:ss A');
                    document.write(date);

                    $(function () {
                        'use strict';
                        setInterval(function () {
                            $('#clock').text(moment().format('H:mm:ss A'));
                        },1000);
                    });

                </script>
            </a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-user"></i>
                {{ auth()->user()->username }}
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header">Account Settings</span>
                <a href="{{ route('admin.users.account-setting') }}" class="dropdown-item {{ (url()->current() == route('admin.users.account-setting')) ? 'active':'' }}">
                    <i class="fas fa-user mr-2"></i> {{ ucfirst(__('My Account')) }}
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('auth.user.logout') }}" class="dropdown-item">
                    <i class="fas fa-sign-out-alt mr-2"></i> {{ ucfirst(__('Logout')) }}
                </a>
                <div class="dropdown-divider"></div>
            </div>
        </li>
    </ul>
</nav>
