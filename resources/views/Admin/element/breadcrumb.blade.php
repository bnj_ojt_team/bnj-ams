<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ $title_array[1] }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    @foreach ($title_array as $item)
                        <li class="breadcrumb-item"><a href="#">{{ $item }}</a></li>
                    @endforeach
                </ol>
            </div>
        </div>
    </div>
</div>
