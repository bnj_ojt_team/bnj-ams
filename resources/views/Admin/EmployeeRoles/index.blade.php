@extends('Admin.layout.default')
@section('title', 'Admin.Employee Roles')
@section('content')
    {{-- Modal --}}
    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-md">
            <form action="" method="" id="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="">Position</label>
                                    <input type="text" name="position" class="form-control rounded-0" id="position"
                                           placeholder="Enter Position">
                                </div>
                                <div class="form-group">
                                    <label for="">Level</label>
                                    <input type="number" name="level" class="form-control rounded-0" id="level"
                                           placeholder="Enter Level">
                                </div>
                                <div class="form-group">
                                    <label for="">Basic Salary</label>
                                    <input type="number" name="basic_salary" class="form-control rounded-0" id="basic_salary"
                                           placeholder="Enter Basic Salary">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0" id="save-button"></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Datatable --}}
    <div class="card card-primary card-outline">
        <div class="card-header">
            {{--<h3 class="card-title col-6">{{ __('Employee Roles List') }}</h3>--}}
            <button class="btn btn-primary col-2 float-right rounded-0" id="add"> New Employee Role</button>
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Position</th>
                        <th>Level</th>
                        <th>Basic Salary</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- Script --}}
    <script>
        $(function() {
            var url = '';

            // Click Add
            $(document).on('click', '#add', function() {
                url = base_url + 'admin/employee-roles/add';
                $('#save-button').html('Save Changes');
                $('#modal-title').html('Add New Employee Role');
                $('#modal').modal('show');
            });

            // Datatable
            var datatable_instance = $("#datatable").DataTable({
                "order": [
                    [0, 'Asc']
                ],
                "responsive": true,
                "language": {
                    "emptyTable": "No Employee Role data found"
                },
                "ajax": {
                    url: base_url + 'admin/employee-roles/index',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        $('#datatable > tbody').html(
                            '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                    },
                },
                "columns": [{
                    data: 'position'
                }, {
                    data: 'level'
                }, {
                    data: 'basic_salary'
                }, {
                    data: 'id'
                }],
                "columnDefs": [{
                    targets: 0,
                    data: null,
                    render: function(data, type, row) {
                        return data.charAt(0).toUpperCase() + data.slice(1);
                    }
                }, {
                    targets: 3,
                    data: null,
                    render: function(data, type, row) {
                        return `<div class="btn-group">
                            <button type="button" class="btn btn-primary btn-flat">Action</button>
                            <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                                <a data-id="${row.id}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                                <a data-id="${row.id}" title="Delete" style="cursor: pointer;" class="dropdown-item delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                        </div>`;
                    }
                }],
            });

            $('input[type="number"]').on('keypress',function (e) {
                if(!e.key.match(/^([0-9])$/g)){
                    e.preventDefault();
                }
            });

            // Form submit
            $(document).on('submit', '#form', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                if(url === base_url + 'admin/employee-roles/add'){
                    $('#save-button').html('Saving <i class="fas fa-spinner"></i>');
                }else{
                    $('#save-button').html('Updating <i class="fas fa-spinner"></i>');
                }
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    type: 'json',
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $('#modal').modal('hide');
                        response(data.title, data.icon);
                        datatable_instance.ajax.reload();
                    },
                    error: function(xhr, status, error) {
                        if(url === base_url + 'admin/employee-roles/add'){
                    $('#save-button').html('Save');
                }else{
                    $('#save-button').html('Update Changes');
                }
                        var title = '';
                        var validation = JSON.parse(xhr.responseText);
                        title = validation.errors ? validation.errors[Object.keys(validation
                            .errors)[0]] : validation.title;
                        response(title, 'error');
                        datatable_instance.ajax.reload();
                    }
                });
            });

            // Click Edit
            datatable_instance.on('click', '.edit', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                url = base_url + 'admin/employee-roles/edit/' + dataId;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        $('#position').val(data.position);
                        $('#level').val(data.level);
                        $('#basic_salary').val(data.basic_salary);
                        $('#save-button').html('Update Changes');
                        $('#modal-title').html('Edit Employee Role');
                        $('#modal').modal('show');
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.reload();
                        response(error, 'error');
                    }
                });
            });

            // Delete Function
            datatable_instance.on('click', '.delete', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = base_url + 'admin/employee-roles/delete/' + dataId;
                Swal.fire({
                    title: 'Delete Employee Role?',
                    text: 'Are You Sure you want to delete this Employee Role?',
                    icon: 'warning',
                    animation: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                }).then(function(result) {
                    if (result.isConfirmed) {
                        let timerInterval;

                        (async () => {

                            const { value: password } = await Swal.fire({
                            title: 'Confirm Password',
                            input: 'password',
                            inputLabel: 'Your Password',
                            inputPlaceholder: 'Enter Your Password',
                            showCancelButton: true,
                            inputValidator: (value) => {
                                if (!value) {
                                    return 'You need to enter your password!'
                                }
                            }
                        })

                        if (password) {

                            Swal.fire({
                                    title: 'Deleting...',
                                    timer: 10000,
                                    animation: false,
                                    timerProgressBar: false,
                                    didOpen: () => {
                                    Swal.showLoading();
                            const b = Swal.getHtmlContainer().querySelector('b');
                            timerInterval = setInterval(() => {
                                b.textContent = Swal.getTimerLeft()
                        }, 100);

                            $.ajax({
                                url: href+'/'+`${password}`,
                                type: 'DELETE',
                                method: 'DELETE',
                                dataType: 'JSON',
                                success: function(data, textStatus, jqXHR) {
                                    Swal.close();
                                    datatable_instance.ajax.reload();
                                    response(data.title, data.icon);
                                },
                                error: function(xhr, status, error) {
                                    datatable_instance.ajax.reload();
                                    var title = '';
                                    var validation = JSON.parse(xhr.responseText);
                                    title = validation.errors ? validation.errors[Object.keys(validation
                                        .errors)[0]] : validation.title;
                                    response(title, 'error');
                                }
                            });
                        },
                            willClose: () => {
                                clearInterval(timerInterval);
                            }
                        });

                        }

                    })()

                    }
                });
            });

            // Hidden modal
            $('#modal').on('hidden.bs.modal', function() {
                url = '';
                $('#save-button').html('');
                $('#modal-title').html('');
                $('#form')[0].reset();
            });

            // Sweetalert
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            function response(title, icon) {
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }

        });
    </script>
@endsection
