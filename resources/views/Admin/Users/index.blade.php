@extends('Admin.layout.default')
@section('title', 'Admin.Users')
@section('content')

    <?php
    $roles = [
        'admin'
    ];
    ?>

    {{-- Modal --}}
    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-lg">
            <form action="" method="post" id="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="input-group mb-3 d-flex justify-content-center">
                                <center>
                                    <img src="{{ asset('preview.png') }}" alt="" id="file-preview"
                                         @error('username') style="height: 4rem; width: 4rem; border-radius: 50%; border: 2px solid rgb(255, 0, 0) !important;" @enderror
                                    style="height: 4rem; width: 4rem; border-radius: 50%; border: 1px solid rgb(184, 184, 184);"><br>
                                    <label for="">Avatar</label>
                                    @error('file')
                                    <div class="container text-danger">{{ '*' . $message }}</div>
                                    @enderror
                                </center>
                                <input name="file" type="file" class="form-control rounded-0" id="file"
                                       placeholder="Full name" accept="image/*" style="display: none;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Username</label>
                                    <input type="text" name="username" class="form-control rounded-0"
                                           id="username" placeholder="Enter Username">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" name="email" class="form-control rounded-0"
                                           id="email" placeholder="Enter Email">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Role</label>
                                    <select name="role" id="role" class="form-control rounded-0">
                                        <option value="">Select Role</option>
                                        @foreach($roles as $role)
                                            <option value="{{$role}}">{{ucwords($role)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="pass-confirm-pass-row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">New Password</label>
                                    <input type="password" name="password" class="form-control rounded-0"
                                           id="password" placeholder="Enter Password">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Confirm Password</label>
                                    <input type="password" name="confirm-password"
                                           class="form-control rounded-0" id="confirm-password"
                                           placeholder="Enter Confirm Password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0" id="save-button"></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Datatable --}}
    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title col-6">{{ __('Employee Users List') }}</h3>
            <button class="btn btn-primary float-right rounded-0 col-2" id="add">New User </button>
        </div>
        <div class="card-body">


            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3" id="employment-status-select">

                </div>
            </div>

            <hr class="mt-3 mb-3">

            <div class="row">
                <div class="col-md-12">
                    <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Account Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Script --}}
    <script>
        $(function() {

            var url = '';
            var src = $('#file-preview').attr('src');
            // Datatable
            var datatable_instance = $("#datatable").DataTable({
                "order": [
                    [0, 'Asc']
                ],
                "responsive": true,
                "language": {
                    "emptyTable": "No Employee Data found"
                },
                "ajax": {
                    url: base_url + 'admin/users/index',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        $('#datatable > tbody').html(
                            '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                    },
                },
                initComplete: function () {
                    var api = this.api();
                    api.columns([2])
                        .every(function () {
                            var column = this;
                            var filter = '<select name="dt_filter" class="form-control rounded-0" id="dt-filter">' +
                                '<option value="">All</option>' +
                                '</select>';
                            var select = $(filter)
                                .appendTo($('#employment-status-select'))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                    column.search(val ? '^' + val + '$' : '', true, false).draw();
                                });

                            column
                                .data()
                                .unique()
                                .sort()
                                .each(function (d, j) {
                                    select.append('<option value="' + d + '">' + (d.toUpperCase().charAt(0) + d.substr(1) ) + '</option>');
                                });
                        });

                    $('#employment-status-select').prepend('<label for="dt-filter">Roles</label>');

                },
                "columns": [{
                    data: 'username'
                }, {
                    data: 'email'
                }, {
                    data: 'role'
                }, {
                    data: 'active'
                }, {
                    data: 'id'
                }],
                "columnDefs": [{
                    targets: 2,
                    data: null,
                    render: function(data, type, row) {
                        return data.charAt(0).toUpperCase() + data.slice(1);
                    }
                }, {
                    targets: 3,
                    data: null,
                    render: function(data, type, row) {
                        return (data == 1) ? 'Active' : 'Disabled';

                    }
                }, {
                    targets: 4,
                    data: null,
                    render: function(data, type, row) {
                        if (row.active == true) {
                            return `<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-flat">Action</button>
                                <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a data-id="${row.id}" title="Disable" style="cursor: pointer;" class="dropdown-item set-as-active" data-process="off"><i class="fas fa-user-lock" aria-hidden="true"></i> Disable</a>
                                </div>
                            </div>`;
                        }else{
                            return `<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-flat">Action</button>
                                <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a data-id="${row.id}" title="Activate" style="cursor: pointer;" class="dropdown-item set-as-active" data-process="on"><i class="fas fa-user-check" aria-hidden="true"></i> Activate</a>
                                </div>
                            </div>`;
                        }
                    }
                }],
            });

            // Onclick active
            datatable_instance.on('click', '.set-as-active', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var dataProcess = $(this).attr('data-process');
                var href = base_url + 'admin/users/is-active/' + dataId + '/' + dataProcess;
                Swal.fire({
                    title: (dataProcess == 'on') ? 'Activate Account?' : 'Disable Account?',
                    text: (dataProcess == 'on') ?
                        'Are you sure you want to activate this account? ' :
                        'Are you sure you want to disable this account? ',
                    icon: 'warning',
                    animation: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                }).then(function(result) {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: href,
                            method: 'GET',
                            dataType: 'JSON',
                            success: function(data, textStatus, jqXHR) {
                                datatable_instance.ajax.reload();
                                response(data.title, data.icon);
                            },
                            error: function(xhr, status, error) {
                                datatable_instance.ajax.reload();
                                response(error, 'error');
                            }
                        })
                    }
                });
            });

            // Click Add
            $(document).on('click', '#add', function() {
                url = base_url + 'admin/users/add';
                $('#save-button').html('Save');
                $('#modal-title').html('Add New Users');
                $('#modal').modal('show');
            });

            // Hidden modal
            $('#modal').on('hidden.bs.modal', function() {
                url = '';
                $('#save-button').html('');
                $('#modal-title').html('');
                $('#form')[0].reset();
                $('#file-preview').attr('src',src);
            });

            $('#file').on('change', function(event) {
                var image = URL.createObjectURL(event.target.files[0]);
                $('#file-preview').attr('src', image);
            });
            $('#file-preview').on('click', function() {
                $('#file').click();
            });

            $(document).on('submit', '#form', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                $('#save-button').html('Saving <i class="fas fa-spinner"></i>');
                $.ajax({
                    url:url,
                    method: 'POST',
                    data: data,
                    type: 'json',
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        response(data.title, data.icon);
                        datatable_instance.ajax.reload(null, false);
                        $('#modal').modal('hide');
                    },
                    error: function(xhr, status, error) {
                        var title = '';
                        var validation = JSON.parse(xhr.responseText);
                        title = validation.errors ? validation.errors[Object.keys(validation
                            .errors)[0]] : validation.title;
                        response(title, 'error');
                        datatable_instance.ajax.reload();
                    }
                });
            });

            // Sweetalert
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            function response(title, icon) {
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }

        });
    </script>
@endsection
