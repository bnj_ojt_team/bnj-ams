@extends('Admin.layout.default')
@section('title', 'Admin.Employees')
@section('content')
    @php
        $employment_status = [
            'Trainee' => 'Trainee',
            'Retired' => 'Retired',
            'Regular' => 'Regular',
        ];

        $roles = [
            'employee',
            'manager'
        ];
    @endphp
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>

    {{-- Modal --}}
    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-xl">
            <form action="" method="" id="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        {{-- Employees Account --}}
                        <div class="row">
                            <div class="card w-100">
                                <div class="card-header bg-secondary">
                                    <h3 class="card-title">Employee's Account</h3>
                                </div>
                                <div class="card-body">
                                    {{-- User Model Forms --}}
                                    {{-- Content Here --}}
                                    <div class="row">
                                        <div class="input-group mb-3 d-flex justify-content-center">
                                            <center>
                                                <img src="{{ asset('preview.png') }}" alt="" id="file-preview"
                                                    @error('username') style="height: 4rem; width: 4rem; border-radius: 50%; border: 2px solid rgb(255, 0, 0) !important;" @enderror
                                                    style="height: 4rem; width: 4rem; border-radius: 50%; border: 1px solid rgb(184, 184, 184);"><br>
                                                <label for="">Avatar</label>
                                                @error('file')
                                                    <div class="container text-danger">{{ '*' . $message }}</div>
                                                @enderror
                                            </center>
                                            <input name="file" type="file" class="form-control rounded-0" id="file"
                                                placeholder="Full name" accept="image/*" style="display: none;">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="">Username</label>
                                                <input type="text" name="username" class="form-control rounded-0"
                                                    id="username" placeholder="Enter Username">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="">Email</label>
                                                <input type="text" name="email" class="form-control rounded-0"
                                                    id="email" placeholder="Enter Email">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="">Role</label>
                                                <select name="role" id="role" class="form-control rounded-0">
                                                    <option value="">Select Role</option>
                                                    @foreach($roles as $role)
                                                        <option value="{{$role}}">{{ucwords($role)}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="pass-confirm-pass-row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">New Password</label>
                                                <input type="password" name="password" class="form-control rounded-0"
                                                    id="password" placeholder="Enter Password">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">Confirm Password</label>
                                                <input type="password" name="confirm-password"
                                                    class="form-control rounded-0" id="confirm-password"
                                                    placeholder="Enter Confirm Password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Employees Profile --}}
                        <div class="row">
                            <div class="card w-100">
                                <div class="card-header bg-secondary">
                                    <h3 class="card-title">Employee's Profile</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="">Firstname</label>
                                                <input type="text" name="profile[firstname]"
                                                    class="form-control rounded-0" id="firstname"
                                                    placeholder="Enter Firstname">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="">Middlename</label>
                                                <input type="text" name="profile[middlename]"
                                                    class="form-control rounded-0" id="middlename"
                                                    placeholder="Enter Middlename">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="">Lastname</label>
                                                <input type="text" name="profile[lastname]"
                                                    class="form-control rounded-0" id="lastname"
                                                    placeholder="Enter Lastname">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">ID Number</label>
                                                <input type="number" name="profile[employee_id_number]"
                                                    class="form-control rounded-0" id="employee_id_number"
                                                    placeholder="Enter ID Number">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">Birthdate</label>
                                                <div class="input-group date" id="datepickerbirthdate"
                                                    data-target-input="nearest">
                                                    <input type="text"
                                                        class="form-control datetimepicker-input rounded-0"
                                                        data-target="#datepickerbirthdate" name="profile[birthdate]"
                                                        id="birthdate" placeholder="Date Of Birth">
                                                    <div class="input-group-append" data-target="#datepickerbirthdate"
                                                        data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">Date Employed</label>
                                                <div class="input-group date" id="datepickerdateemployed"
                                                    data-target-input="nearest">
                                                    <input type="text"
                                                        class="form-control datetimepicker-input rounded-0"
                                                        data-target="#datepickerdateemployed"
                                                        name="profile[date_employed]" id="date_employed"
                                                        placeholder="Date Employed">
                                                    <div class="input-group-append" data-target="#datepickerdateemployed"
                                                        data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">Employment Status</label>
                                                <select name="profile[employment_status]" id="employment_status"
                                                    class="form-control rounded-0" disabled="">
                                                    <option value="" class="d-none">-- Select Employment Status --
                                                    </option>
                                                    @foreach ($employment_status as $item)
                                                        <option value="{{ $item }}">{{ $item }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">Contact Number</label>
                                                <input type="text" class="form-control rounded-0"
                                                    name="profile[contact_number]" id="contact_number"
                                                    placeholder="Contact Number"
                                                    data-inputmask="'mask': ['9999-999-9999', '+63999-999-9999']"
                                                    data-mask>
                                                <label for="">Position</label>
                                                <select name="profile[employee_role_id]" id="employee_role_id"
                                                    class="form-control rounded-0">
                                                    <option value="" class="d-none">-- Select Position --</option>
                                                    @foreach ($employeeRoles as $key => $value)
                                                        <option value="{{ $key }}">{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">Address</label>
                                                <textarea name="profile[address]" class="form-control rounded-0" id="address" placeholder="Enter Address"
                                                    row="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Employees Contact --}}
                        <div class="row">
                            <div class="card w-100">
                                <div class="card-header bg-secondary">
                                    <h3 class="card-title">Employee's Contact</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">Fullname</label>
                                                <input type="text" name="contact[fullname]"
                                                    class="form-control rounded-0" id="fullname"
                                                    placeholder="Enter Fullname">
                                                <label for="">Relation</label>
                                                <input type="text" name="contact[relation]"
                                                    class="form-control rounded-0" id="relation"
                                                    placeholder="Enter Relation">
                                                <label for="">Phone Number</label>
                                                <input type="text" class="form-control rounded-0"
                                                    name="contact[phone_number]" id="phone_number"
                                                    placeholder="Phone Number"
                                                    data-inputmask="'mask': ['9999-999-9999', '+63999-999-9999']"
                                                    data-mask>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="">Address</label>
                                                <textarea name="contact[address]" class="form-control rounded-0" id="contact-address" placeholder="Enter Address"
                                                    row="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0" id="save-button"></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Datatable --}}
    <div class="card card-primary card-outline">
        <div class="card-header">
            <button class="btn btn-primary float-right rounded-0 col-2" id="add"> New Employee </button>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3" id="employment-status-select">

                </div>
            </div>

            <hr class="mt-3 mb-3">

            <div class="row">
              <div class="col-md-12">
                  <div class="table-responsive">
                      <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Name</th>
                                  <th>Position</th>
                                  <th>ID Number</th>
                                  <th>Birthdate</th>
                                  <th>Date Employed</th>
                                  <th>Employment Status</th>
                                  <th>Contact Number</th>
                                  <th>Address</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                      </table>
                  </div>
              </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('plugins/inputmask/jquery.inputmask.min.js') }}"></script>
    {{-- Script --}}
    <script>
        $(function() {
            var url = '';
            $('[data-mask]').inputmask();

            //Date pickers
            $('#datepickerdateemployed').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#datepickerbirthdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });

            // Image upload on click and preview
            $('#file').on('change', function(event) {
                var image = URL.createObjectURL(event.target.files[0]);
                $('#file-preview').attr('src', image);
            });
            $('#file-preview').on('click', function() {
                $('#file').click();
            });

            // Click Add
            $(document).on('click', '#add', function() {
                url = base_url + 'admin/employees/add';
                $('#pass-confirm-pass-row').removeAttr('style');
                $('#employment_status > option[value="Trainee"]').prop('selected', true);
                $('#employment_status').attr('disabled', 'disabled');
                $('#save-button').html('Save Changes');
                $('#modal-title').html('Add New Employee Profile');
                $('#modal').modal('show');
            });

            // Datatable
            var datatable_instance = $("#datatable").DataTable({
                "order": [
                    [0, 'Asc']
                ],
                "responsive": true,
                "language": {
                    "emptyTable": "No Employees data found"
                },
                "ajax": {
                    url: base_url + 'admin/employees/index',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        $('#datatable > tbody').html(
                            '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                    },
                },
                initComplete: function () {
                    var api = this.api();
                    api.columns([5])
                        .every(function () {
                            var column = this;
                            var filter = '<select name="dt_filter" class="form-control rounded-0" id="dt-filter">' +
                                '<option value="">All</option>' +
                                '</select>';
                            var select = $(filter)
                                .appendTo($('#employment-status-select'))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                    column.search(val ? '^' + val + '$' : '', true, false).draw();
                                });

                            column
                                .data()
                                .unique()
                                .sort()
                                .each(function (d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                });
                        });

                    $('#employment-status-select').prepend('<label for="dt-filter">Employment Status</label>');

                },
                "columns": [{
                    data: 'profile.firstname'
                }, {
                    data: 'profile.employee_role.position'
                }, {
                    data: 'profile.employee_id_number'
                }, {
                    data: 'profile.birthdate'
                }, {
                    data: 'profile.date_employed'
                }, {
                    data: 'profile.employment_status'
                }, {
                    data: 'profile.contact_number'
                }, {
                    data: 'profile.address'
                }, {
                    data: 'id'
                }],
                "columnDefs": [{
                    targets: 0,
                    data: null,
                    render: function(data, type, row) {
                        return row.profile.lastname + ', ' + row.profile.firstname + ' ' + row
                            .profile.middlename;
                    }
                }, {
                    targets: 1,
                    data: null,
                    render: function(data, type, row) {
                        return row.profile.employee_role.position + ' ' + row.profile
                            .employee_role.level;
                    }
                }, {
                    targets: 3,
                    data: null,
                    render: function(data, type, row) {
                        return moment(data).format('MMMM DD, YYYY');
                    }
                }, {
                    targets: 4,
                    data: null,
                    render: function(data, type, row) {
                        return moment(data).format('MMMM DD, YYYY');
                    }
                }, {
                    targets: 8,
                    data: null,
                    render: function(data, type, row) {
                        return `<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-flat">Action</button>
                                <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a data-id="${row.id}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                                    <a data-id="${row.id}" title="Update Contributions" style="cursor: pointer;" class="dropdown-item update-contributions"> ${(row.profile.has_contributions) ? '<i class="fas fa-toggle-on"></i> Disable Contributions':'<i class="fas fa-toggle-off"></i> Enable Contributions'} </a>
                                </div>
                            </div>`;
                    }
                }],
            });

            // Form submit
            $(document).on('submit', '#form', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                if(url === base_url + 'admin/employees/add'){
                    $('#save-button').html('Saving <i class="fas fa-spinner"></i>');
                }else{
                    $('#save-button').html('Updating <i class="fas fa-spinner"></i>');
                }
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    type: 'json',
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $('#modal').modal('hide');
                        response(data.title, data.icon);
                        datatable_instance.ajax.reload();
                    },
                    error: function(xhr, status, error) {
                        if(url === base_url + 'admin/employees/add'){
                        $('#save-button').html('Save');
                    }else{
                        $('#save-button').html('Update Changes');
                    }
                        var title = '';
                        var validation = JSON.parse(xhr.responseText);
                        title = validation.errors ? validation.errors[Object.keys(validation
                            .errors)[0]] : validation.title;
                        response(title, 'error');
                        datatable_instance.ajax.reload();
                    }
                });
            });

            // Click Edit
            datatable_instance.on('click', '.edit', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                url = base_url + 'admin/employees/edit/' + dataId;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        // Profile Inputs
                        $('#pass-confirm-pass-row').attr('style', 'display: none;');
                        $('#password').removeAttr('name');
                        $('#firstname').val(data.profile.firstname);
                        $('#middlename').val(data.profile.middlename);
                        $('#lastname').val(data.profile.lastname);
                        $('#employee_id_number').val(data.profile.employee_id_number);
                        $('#birthdate').val(data.profile.birthdate);
                        $('#date_employed').val(data.profile.date_employed);
                        $('#contact_number').val(data.profile.contact_number);
                        $('#address').val(data.profile.address);
                        $('#role').val(data.role);
                        $('#employment_status').removeAttr('disabled');
                        $('#employment_status > option[value="' + data.profile.employment_status + '"]').prop('selected', true);
                        $('#employee_role_id > option[value="' + data.profile.employee_role_id + '"]').prop('selected', true);
                        // Account Inputs
                        $('#username').val(data.username);
                        $('#email').val(data.email);
                        // Contact Inputs
                        $('#fullname').val(data.contact.fullname);
                        $('#relation').val(data.contact.relation);
                        $('#contact-address').val(data.contact.address);
                        $('#phone_number').val(data.contact.phone_number);
                        $('#save-button').html('Update Changes');
                        $('#modal-title').html('Edit Employee Profile');
                        $('#modal').modal('show');
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.reload();
                        response(error, 'error');
                    }
                });
            });

            // Click Update Contributions
            datatable_instance.on('click', '.update-contributions', function(e){
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                // var href = 'isActive/'+dataId;
                var href = `${base_url}admin/employees/update-contributions/${dataId}`;
                $.ajax({
                    url: href,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        datatable_instance.ajax.reload();
                        response(data.title, data.icon);
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.reload();
                        response(error, 'error');
                    }
                })
            });

            // Hidden modal
            $('#modal').on('hidden.bs.modal', function() {
                url = '';
                $('#password').attr('name', 'password');
                $('#file-preview').attr('src', base_url + 'public/preview.png');
                $('#save-button').html('');
                $('#modal-title').html('');
                $('#form')[0].reset();
            });

            // Sweetalert
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            function response(title, icon) {
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }

        });
    </script>

@endsection
