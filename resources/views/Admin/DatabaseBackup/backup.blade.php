@extends('Admin.layout.default')
@section('title', 'Admin.Database Backup')
@section('content')
    <div class="container-fluid">
        <a href="{{ route('admin.database.create-new-main-backup') }}" type="button" class="btn btn-default btn-flat pull-right" id="new-main-backup">Create New Main Backup</a>
        <a href="{{ route('admin.database.create-new-backup') }}" type="button" class="btn btn-default btn-flat pull-right" id="new-backup">Create New Backup</a>
        <div class="card-header">
            <h3 class="card-title">Databases</h3>
        </div>
        <div class="card-body">
            @foreach ($files as $item)
                <a href="{{ route('admin.database.download-file', $item) }}" class="btn btn-default btn-block btn-flat">{{ __($item) }} <i class="fas fa-download"></i></a>
            @endforeach
        </div>
    </div>
    <script>
        $(function(){
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });
            var title = '{{ session('title') }}';
            var icon = '{{ session('icon') }}';
            if(icon != '' && title != ''){
                setTimeout(function(){
                    response(title,icon);
                }, 750);
            }

            function response(title, icon){
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }
        });
    </script>
@endsection
