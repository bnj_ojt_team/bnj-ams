@extends('Admin.layout.default')
@section('title', 'Admin.Contributions')
@section('content')

{{-- Modal --}}
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <form action="" method="" id="form">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- Content Here --}}
                    <label for="">Benefits</label>
                    <div class="col-lg-12">
                        <div class="input-group">
                            <select class="form-control rounded-0" name="name" id="name">
                                <option value="" selected>{{ ucwords('Select Benefits...') }}</option>
                                <option value="pagibig">{{ ucwords('pagibig') }}</option>
                                <option value="sss">{{ strtoupper('sss') }}</option>
                                <option value="philhealth">{{ ucwords('philhealth') }}</option>
                                <option value="other">{{ ucwords('other') }}</option>
                            </select>
                        </div>
                    </div>
                    <label id="other-label">{{ ucwords('other') }}</label>
                    <div class="col-lg-12">
                        <div class="input-group">
                            <input type="text" class="form-control rounded-0" name="name" id="other">
                        </div>
                    </div>
                    <label>{{ ucwords('employer rate') }}</label>
                    <div class="col-lg-12">
                        <div class="input-group">
                            <input type="number" step="any" class="form-control rounded-0" name="employer_rate" id="employer_rate">
                        </div>
                    </div>
                    <label>{{ ucwords('employee rate') }}</label>
                    <div class="col-lg-12">
                        <div class="input-group">
                            <input type="number" step="any" class="form-control rounded-0" name="employee_rate" id="employee_rate">
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <label for="employee-role-id">Position</label>
                        <select name="employee_role_id" id="employee-role-id" class="form-control rounded-0" required>
                            <option value="">Select Position</option>
                            @foreach($employeeRoles as $key => $value)
                                <option value="{{$key}}">{{ucwords($value)}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary rounded-0" id="save-button"></button>
                </div>
            </div>
        </form>
    </div>
</div>

{{-- Datatable --}}
<div class="card card-primary card-outline">
    <div class="card-header">
        <button class="btn btn-primary col-2 float-right rounded-0" id="add"> New Contribution </button>
    </div>
    <div class="card-body">

        <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Employer Rate</th>
                    <th>Employee Rate</th>
                    <th>Total Percentage</th>
                    <th>Rate For Position</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- Script --}}
<script>
    $(function() {
        var url = '';

            // Datatable
        var datatable_instance = $("#datatable").DataTable({
            "order": [
                [4]
                ],
            "responsive": true,
            "language": {
                "emptyTable": "No Contributions Found"
            },
            "ajax": {
                url: base_url + 'admin/contributions/index',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function() {
                    $('#datatable > tbody').html(
                        '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                },
            },
            "columns": [{
                data: 'name'
            }, {
                data: 'employer_rate'
            }, {
                data: 'employee_rate'
            },{
                data: 'total_percentage'
            },{
                data: 'employee_role.position'
            },{
                data: 'id'
            }],
            "columnDefs": [{
                targets: 5,
                data: null,
                render: function(data, type, row) {
                    return `<div class="btn-group">
                        <button type="button" class="btn btn-primary btn-flat">Action</button>
                        <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                        <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu" role="menu">
                            <a data-id="${row.id}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                            <a data-id="${row.id}" title="Delete" style="cursor: pointer;" class="dropdown-item delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                        </div>
                    </div>`;
                }
            },{
                targets: 4,
                data: null,
                render: function(data, type, row) {
                    return `${data} ${row.employee_role.level}`;
                }
            }],
        });

            // Click Add
        $(document).on('click', '#add', function() {
            url = base_url + 'admin/contributions/add';
            $('#save-button').html('Save');
            $('#modal-title').html('Add New Contribution');
            $('#modal').modal('show');
            $('#other-label').hide();
            $('#other').hide();
        });

        // Click Other
        $('#name').change(function() {
            if($(this).val() == 'other'){
            $('#name').removeAttr('name');     
            $('#other').attr('name', 'name');     
            $('#other-label').show();
            $('#other').show();

            }else{
            $('#name').attr('name', 'name');          
            $('#other').removeAttr('name');          
            $('#other-label').hide();
            $('#other').hide();
            }


        });

            // Click Edit
        datatable_instance.on('click', '.edit', function(e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            url = base_url + 'admin/contributions/edit/' + dataId;
            $.ajax({
                url: url,
                method: 'GET',
                dataType: 'JSON',
                success: function(data, textStatus, jqXHR) {
                    var benefits = ['pagibig','philhealth','sss'];

                    if(benefits.includes(data.name)){
                        $('#name').val(data.name);
                        $('#other-label').hide();
                        $('#other').hide();
                        $('#name').attr('name', 'name');
                        $('#other').removeAttr('name');
                    }else{
                        $('#name').val('other');
                        $('#other-label').show();
                        $('#other').show().val(data.name);
                        $('#name').removeAttr('name');
                        $('#other').attr('name', 'name');
                    }
                    $('#employee-role-id').val(data.employee_role_id);
                    $('#employer_rate').val(data.employer_rate * 100);
                    $('#employee_rate').val(data.employee_rate * 100);
                    $('#save-button').html('Update');
                    $('#modal-title').html('Edit Contribution');
                    $('#modal').modal('show');
                },
                error: function(xhr, status, error) {
                    response(error, 'error');
                }
            });
        });

            // Form submit
        $(document).on('submit', '#form', function(e) {
            e.preventDefault();
            var data = new FormData(this);
            if(url === base_url + 'admin/contributions/add'){
                $('#save-button').html('Saving <i class="fas fa-spinner"></i>');
            }else{
                $('#save-button').html('Updating <i class="fas fa-spinner"></i>');
            }
            // console.log(data);
            $.ajax({
                url: url,
                method: 'POST',
                data: data,
                type: 'json',
                contentType: false,
                processData: false,
                cache: false,
                success: function(data, textStatus, jqXHR) {
                    $('#modal').modal('hide');
                    // console.log(data);
                    response(data.title, data.icon);
                    datatable_instance.ajax.reload();
                    // datatable_instance.ajax.url(base_url + 'admin/contributions/index')
                    // .load();
                },
                error: function(xhr, status, error) {
                    if(url === base_url + 'admin/contributions/add'){
                        $('#save-button').html('Save');
                    }else{
                        $('#save-button').html('Update Changes');
                    }
                    var title = '';
                    var validation = JSON.parse(xhr.responseText);
                    title = validation.errors ? validation.errors[Object.keys(validation
                        .errors)[0]] : validation.title;
                    response(title, 'error');
                    datatable_instance.ajax.reload();
                    // datatable_instance.ajax.reload();
                    // datatable_instance.ajax.url(base_url + 'admin/contributions/index')
                    // .load();
                }
            });
        });

            // Hidden modal
        $('#modal').on('hidden.bs.modal', function() {
            url = '';
            $('#save-button').html('');
            $('#modal-title').html('');
            $('#form')[0].reset();
        });


            // Delete Function
        datatable_instance.on('click', '.delete', function(e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = base_url + 'admin/contributions/delete/' + dataId;
            Swal.fire({
                title: 'Delete Benefit?',
                text: '{{ ucfirst('are you sure you want to delete this benefit?') }}',
                icon: 'warning',
                animation: true,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                closeOnConfirm: false,
                closeOnCancel: false,
            }).then(function(result) {
                if (result.isConfirmed) {
                    let timerInterval;
                    Swal.fire({
                        title: 'Deleting...',
                        timer: 10000,
                        animation: false,
                        timerProgressBar: false,
                        didOpen: () => {
                            Swal.showLoading();
                            const b = Swal.getHtmlContainer().querySelector('b');
                            timerInterval = setInterval(() => {
                                b.textContent = Swal.getTimerLeft()
                            }, 100);

                            $.ajax({
                                url: href,
                                type: 'DELETE',
                                method: 'DELETE',
                                dataType: 'JSON',
                                success: function(data, textStatus, jqXHR) {
                                    Swal.close();
                                    datatable_instance.ajax.reload();
                                    response(data.title, data.icon);
                                },
                                error: function(xhr, status, error) {
                                    datatable_instance.ajax.reload();
                                    response(error, 'error');
                                }
                            });
                        },
                        willClose: () => {
                            clearInterval(timerInterval);
                        }
                    });
                }
            });
        });

            // Sweetalert
        var Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 4000
        });

        function response(title, icon) {
            Toast.fire({
                icon: icon,
                title: title
            })
        }
    });
</script>
@endsection
