@extends('Admin.layout.default')
@section('title', 'Admin.Salary-Loans')
@section('content')

    {{-- Modal --}}
    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <form action="#" method="post" id="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- Content Here --}}
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <label for="amount">Amount</label>
                                <input type="number" name="amount" id="amount" class="form-control rounded-0" placeholder="Amount" required>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <label for="balance">Balance</label>
                                <input type="number" name="balance" id="balance" class="form-control rounded-0" placeholder="Balance" required>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <label for="reason">Reason</label>
                                <textarea name="reason" id="reason" class="form-control rounded-0" required cols="30" rows="3" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0" id="save-button">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Datatable --}}
    <div class="card card-primary card-outline">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="profiles">Employees</label>
                        <select name="profiles" id="profiles" class="form-control rounded-0">
                            <option value="">Choose Employee</option>
                            @foreach ($profiles as $key => $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Amount</th>
                                <th>Remainder</th>
                                <th>Balance</th>
                                <th>Reason</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
                    
        </div>
    </div>

    {{-- Script --}}
    <script>
        $(function () {
            'use strict';

            const formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'PHP',
            });

            var url = '';

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy:true,
                processing:false,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                ordering: false,
                ajax:{
                    url: base_url + 'admin/salary-loans/get-salary-loans',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        $('#datatable > tbody').html(
                            '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                    },
                },
                initComplete: function() {
                    // Apply the search
                    var api = this.api();

                    $('#profiles').on('change', function() {
                        return api.columns([0]).search((this.value)? '^' + (this.value) + '$': '' , true, false).draw();
                    });

                },
                pagingType: 'full_numbers',
                columnDefs: [
                    {
                        targets: 0,
                        data: null,
                        render: function (data,type,row) {
                            return row.user.profile.firstname;
                        }
                    },
                    {
                        targets: 7,
                        data: null,
                        orderable:false,
                        searchable:false,
                        render: function(data,type,row){
                            var status = '';
                            if(row.status.toLowerCase() == 'pending'){
                                status += `<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-flat">Action</button>
                                    <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a data-id="${row.id}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                                        <a data-id="${row.id}/1" title="Complete" style="cursor: pointer;" class="dropdown-item status"><i class="fa fa-check" aria-hidden="true"></i> Complete</a>

                                        <a data-id="${row.id}/0" title="Decline" style="cursor: pointer;" class="dropdown-item status"><i class="fa fa-window-close" aria-hidden="true"></i> Decline</a>
                                    </div>
                                </div>`;
                            }else{
                                status += '--|--'
                            }
                            return status;
                        }
                    }
                ],
                columns: [
                    {data: 'user.profile.firstname'},
                    {
                        data: 'amount',
                        className: 'text-right',
                        render: function(data, meta, row) {
                            return formatter.format(data);
                        }
                    },
                    {
                        data: 'remainder',
                        className: 'text-right',
                        render: function(data, meta, row) {
                            return formatter.format(data);
                        }
                    },
                    {
                        data: 'balance',
                        className: 'text-right',
                        render: function(data, meta, row) {
                            return formatter.format(data);
                        }
                    },
                    {data: 'reason'},
                    {data: 'status'},
                    {data: 'date'},
                    {data: 'id'}
                ]
            });

            $('#modal').on('hidden.bs.modal', function (e) {
                url = '';
                $('#form')[0].reset();
            });

            $('#form').submit(function (e) {
                e.preventDefault();
                $('#save-button').html('Saving <i class="fas fa-spinner"></i>');
                const data = new FormData(this);
                $.ajax({
                    url: url,
                    method:'POST',
                    type:'POST',
                    data: data,
                    cache:false,
                    contentType: false,
                    processData: false
                }).done(function (data,response, status) {
                    $('#save-button').html('Save');
                    $('#modal').modal('toggle');
                    $('#form')[0].reset();
                    table.ajax.reload(null, false);
                    toast('success', data.result, data.message);
                }).fail(function (xhr, status, error) {
                    const response = JSON.parse(xhr.responseText);
                    toast('error', response.result, response.message);
                });
            });

            datatable.on('click','.edit',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                url = base_url + 'admin/salary-loans/edit/'+dataId;
                $.ajax({
                    url:url,
                    type: 'GET',
                    method: 'GET',
                    dataType:'JSON'
                }).done(function (data, status, xhr) {
                    $('#amount').val(data.amount);
                    $('#balance').val(data.balance);
                    $('#reason').val(data.reason);
                    $('#modal-title').html('Update Salary Loan');
                    $('#modal').modal('toggle');
                }).fail(function (xhr, status, error) {
                    const response = JSON.parse(xhr.responseText);
                    toast(response.icon, response.result, response.message);
                });
            });

            datatable.on('click','.delete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'delete/'+dataId;
                Swal.fire({
                    title: 'Delete File',
                    text: 'Are You Sure?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        $.ajax({
                            url:href,
                            type: 'DELETE',
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:'JSON',
                            beforeSend: function () {
                                Swal.fire({
                                    icon: null,
                                    title: null,
                                    text: null,
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                    }
                                });
                            }
                        }).done(function (data, status, xhr) {
                            toast('success', data.result, data.message);
                            table.ajax.reload(null, false);
                        }).fail(function (xhr, status, error) {
                            const response = JSON.parse(xhr.responseText);
                            toast('info', response.result, response.message);
                        });
                    }
                });
            });

            datatable.on('click','.status',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var status = $(this).attr('title');
                var href = base_url + 'admin/salary-loans/status/'+dataId;
                Swal.fire({
                    title: 'Set as '+status,
                    text: 'Are You Sure?',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        $.ajax({
                            url:href,
                            type: 'POST',
                            method: 'POST',
                            headers: {
                                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:'JSON',
                            beforeSend: function () {
                                Swal.fire({
                                    icon: null,
                                    title: 'Updating...',
                                    text: null,
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                    }
                                });
                            }
                        }).done(function (data, status, xhr) {
                            toast('success', data.result, data.message);
                            table.ajax.reload(null, false);
                        }).fail(function (xhr, status, error) {
                            const response = JSON.parse(xhr.responseText);
                            toast('info', response.result, response.message);
                        });
                    }
                });
            });

            $('input[type="number"]').on('keypress input', function (e) {
                if(!e.key.match(/^([0-9])$/g)){
                    e.preventDefault();
                }
            });

            function toast(icon, result, message) {
                Swal.fire({
                    icon:icon,
                    title:result,
                    text:message,
                    timer:3000,
                    toast: true,
                    position:'top-right',
                    showConfirmButton: false,
                });
            }

        });
    </script>
@endsection
