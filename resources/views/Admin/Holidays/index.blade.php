@extends('Admin.layout.default')
@section('title', 'Admin.Holidays')
@section('content')
    @php
        $holiday_types = [
            'Regular Holiday' => 'Regular Holiday',
            'Non Working Holiday' => 'Non Working Holiday',
            'Special Non Working Holiday' => 'Special Non Working Holiday',
        ];
    @endphp

    <style>
        .bootstrap-datetimepicker-widget{
            color: #000;
            background-color: #e0e0e0;
        }
    </style>

    {{-- Accordion --}}
    <div class="col-md-12 col-lg-12">
        <div class="card card-primary shadow-none collapsed-card">
            <div class="card-header">
                <h3 class="card-title">Calendar View</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-calendar-day"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                {{-- Evo Calendar --}}
                <div class="hero">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal --}}
    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <form action="" method="" id="form">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            {{-- Content Here --}}
                            <label for="holiday_name">Holiday</label>
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <input type="text" name="holiday_name" class="form-control rounded-0"
                                           id="holiday_name" placeholder="Holiday">
                                </div>
                            </div>
                            <label for="holiday_date">Holiday Date</label>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input rounded-0"
                                               data-target="#reservationdate" name="holiday_date" id="holiday_date" placeholder="Holiday Date">
                                        <div class="input-group-append" data-target="#reservationdate"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label for="holiday_type">Holiday Type</label>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <select class="custom-select form-control-border border-width-2 rounded-0"
                                            id="holiday_type" name="holiday_type">
                                        @foreach ($holiday_types as $item)
                                            <option value="{{ $item }}">{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <label for="color">Color</label>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="input-group my-colorpicker2 colorpicker-element" data-colorpicker-id="2">
                                        <input type="text" class="form-control" value="#8773c1" name="color" data-original-title="" title="" id="color">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="fas fa-square" style="color:#8773c1;"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>

                            <label for="rate">Rate</label>
                            <div class="col-lg-12">
                                <div class="input-group mb-3">
                                    <input type="text" name="rate" id="rate" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-percent"></i></span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input rounded-0" name="is_per_year"
                                           value="1" id="is_per_year">
                                    <label for="is_per_year" class="custom-control-label">Set as per year</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0" id="save-button"></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Datatable --}}
    <div class="card card-primary card-outline">
        <div class="card-header">
            <button class="btn btn-primary col-2 float-right rounded-0" id="add"> New Holiday </button>
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Holiday</th>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Occurence</th>
                        <th>Color</th>
                        <th>Rate</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    {{-- Script --}}
    <script>
        $(function() {
            var url = '';

            var color = $('span.input-group-text > i.fa-square').css('color');
            // Evo Calendar

            //Date picker
            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });

            //color picker with addon
            $('.my-colorpicker2').colorpicker();

            $('#color').on('change',function () {
                $('span.input-group-text > i.fa-square').css('color',this.value);
            });

            // Datatable
            var datatable_instance = $("#datatable").DataTable({
                "order": [
                    [0, 'Asc']
                ],
                "responsive": true,
                "language": {
                    "emptyTable": "No Holidays Data found"
                },
                "ajax": {
                    url: base_url + 'admin/holidays/get-all-holidays',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        $('#datatable > tbody').html(
                            '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                    },
                },
                "columns": [{
                    data: 'holiday_name'
                }, {
                    data: 'holiday_date'
                }, {
                    data: 'holiday_type'
                }, {
                    data: 'is_per_year'
                }, {
                    data: 'color'
                }, {
                    data: 'rate',
                    render: function(data, type, row){
                        return (data / 100) + ' ' + '('+data+'%)';
                    }
                }, {
                    data: 'id'
                }],
                "columnDefs": [{
                    targets: 3,
                    data: null,
                    render: function(data, type, row) {
                        return (data) ? 'Per Year' : 'Not Per Year';
                    }
                }, {
                    targets: 4,
                    data: null,
                    render: function(data, type, row) {
                        return '<strong class="btn btn-app" style="color:'+row.color+';background-color: '+row.color+'">'+row.color+'</strong>';
                    }
                }, {
                    targets: 6,
                    data: null,
                    render: function(data, type, row) {
                        return `<div class="btn-group">
                            <button type="button" class="btn btn-primary btn-flat">Action</button>
                            <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                                <a data-id="${row.id}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                                <a data-id="${row.id}" title="Delete" style="cursor: pointer;" class="dropdown-item delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                        </div>`;
                    }
                }],
            });

            // Form submit
            $(document).on('submit', '#form', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                if(url === base_url + 'admin/holidays/add'){
                    $('#save-button').html('Saving <i class="fas fa-spinner"></i>');
                }else{
                    $('#save-button').html('Updating <i class="fas fa-spinner"></i>');
                }
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    type: 'json',
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $('#modal').modal('hide');
                        calendar.evoCalendar('destroy');
                        response(data.title, data.icon);
                        datatable_instance.ajax.url(base_url +
                                'admin/holidays/get-all-holidays')
                            .load();
                    },
                    error: function(xhr, status, error) {
                        if(url === base_url + 'admin/holidays/add'){
                            $('#save-button').html('Save');
                        }else{
                            $('#save-button').html('Update Changes');
                        }
                        var title = '';
                        var validation = JSON.parse(xhr.responseText);
                        title = validation.errors ? validation.errors[Object.keys(validation
                            .errors)[0]] : validation.title;
                        response(title, 'error');
                        datatable_instance.ajax.url(base_url +
                                'admin/holidays/get-all-holidays')
                            .load();
                    }
                });
            });

            // Click Add
            $(document).on('click', '#add', function() {
                url = base_url + 'admin/holidays/add';
                $('#save-button').html('Save');
                $('#modal-title').html('Add New Holiday');
                $('#modal').modal('show');
            });

            // Click Edit
            datatable_instance.on('click', '.edit', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                url = base_url + 'admin/holidays/edit/' + dataId;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        $('#holiday_name').val(data.holiday_name);
                        $('#holiday_date').val(data.holiday_date);
                        $('#holiday_type>option[value="' + data.holiday_type + '"]').prop(
                            'selected', true);
                        $('#is_per_year').prop('checked', data.is_per_year);
                        $('#save-button').html('Update Changes');
                        $('#modal-title').html('Edit Holiday');
                        $('#color').val(data.color);
                        $('#rate').val(data.rate);
                        $('span.input-group-text > i.fa-square').css('color',data.color);
                        //$('.my-colorpicker2').colorpicker({color : data.color});
                        $('.my-colorpicker2').colorpicker('setValue', data.color);
                        $('#modal').modal('show');
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.url(base_url +
                                'admin/holidays/get-all-holidays')
                            .load();
                        response(error, 'error');
                    }
                });
            });

            // Hidden modal
            $('#modal').on('hidden.bs.modal', function() {
                url = '';
                $('#save-button').html('');
                $('#modal-title').html('');
                $('.my-colorpicker2').colorpicker('setValue', color);
                $('span.input-group-text > i.fa-square').css('color',color);
                $('#form')[0].reset();
            });

            // Delete Function
            datatable_instance.on('click', '.delete', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = base_url + 'admin/holidays/delete/' + dataId;
                Swal.fire({
                    title: 'Delete Holiday?',
                    text: 'Are You Sure you want to delete this Holiday?',
                    icon: 'warning',
                    animation: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                }).then(function(result) {
                    if (result.isConfirmed) {
                        let timerInterval;
                        Swal.fire({
                            title: 'Deleting...',
                            timer: 10000,
                            animation: false,
                            timerProgressBar: false,
                            didOpen: () => {
                                Swal.showLoading();
                                const b = Swal.getHtmlContainer().querySelector('b');
                                timerInterval = setInterval(() => {
                                    b.textContent = Swal.getTimerLeft()
                                }, 100);

                                $.ajax({
                                    url: href,
                                    type: 'DELETE',
                                    method: 'DELETE',
                                    dataType: 'JSON',
                                    success: function(data, textStatus, jqXHR) {
                                        Swal.close();
                                        calendar.evoCalendar('destroy');
                                        datatable_instance.ajax.url(
                                            base_url +
                                            'admin/holidays/get-all-holidays'
                                        ).load();
                                        response(data.title, data.icon);
                                    },
                                    error: function(xhr, status, error) {
                                        datatable_instance.ajax.url(
                                            base_url +
                                            'admin/holidays/get-all-holidays'
                                        ).load();
                                        response(error, 'error');
                                    }
                                });
                            },
                            willClose: () => {
                                clearInterval(timerInterval);
                            }
                        });
                    }
                });
            });

            // Evo Calendar
            var calendar = $('#calendar').evoCalendar({
                settingName: "Calendar",
                theme: 'Royal Navy',
                language: 'en',
                todayHighlight: true,
                sidebarDisplayDefault: true,
                sidebarToggler: true,
                eventDisplayDefault: true,
                eventListToggler: true
            });

            // Set Evo Calendar again after Destroy
            calendar.on('destroy', function(e) {
                calendar = $('#calendar').evoCalendar({
                    settingName: "Calendar",
                    theme: 'Royal Navy',
                    language: 'en',
                    todayHighlight: true,
                    sidebarDisplayDefault: true,
                    sidebarToggler: true,
                    eventDisplayDefault: true,
                    eventListToggler: true
                });
                setCalendar();
            });

         // Get Holidays
            function getHolidays() {
                var events = [];
                $.ajax({
                    url: base_url + 'admin/holidays/get-all-holidays',
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    global: false,
                    async: false,
                    success: function(data, textStatus, jqXHR) {
                        $.map(data.data, function(data) {
                            events.push({
                                id: data.id,
                                name: data.holiday_name,
                                date: data.holiday_date,
                                type: data.holiday_type,
                                badge: data.holiday_type,
                                everyYear: data.is_per_year,
                                color: data.color
                            });
                        });
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.url(base_url +
                                'admin/holidays/get-all-holidays')
                            .load();
                        response(error, 'error');
                    }
                });
                return events;
            }
            // Set Calendar
            function setCalendar() {
                $('#calendar').evoCalendar('addCalendarEvent', getHolidays());
            }
            setCalendar();

            // Sweetalert
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            function response(title, icon) {
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }
        });
    </script>
@endsection
