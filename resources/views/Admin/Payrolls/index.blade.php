@extends('Admin.layout.default')
@section('title', 'Admin.Payroll')
@section('content')
    
    @php
        $months = [
            ['month_name' => ucwords('january'),'month' => 1],
            ['month_name' => ucwords('february'),'month' => 2],
            ['month_name' => ucwords('march'),'month' => 3],
            ['month_name' => ucwords('april'),'month' => 4],
            ['month_name' => ucwords('may'),'month' => 5],
            ['month_name' => ucwords('june'),'month' => 6],
            ['month_name' => ucwords('july'),'month' => 7],
            ['month_name' => ucwords('august'),'month' => 8],
            ['month_name' => ucwords('september'),'month' => 9],
            ['month_name' => ucwords('october'),'month' => 10],
            ['month_name' => ucwords('november'),'month' => 11],
            ['month_name' => ucwords('december'),'month' => 12]
        ];
    @endphp

    <link rel="stylesheet" href="{{asset('yearpicker/css/yearpicker.css')}}">
    <script src="{{asset('yearpicker/js/yearpicker.js')}}"></script>

    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-md modal-lg">
            <form action="" method="" id="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="table-responsive">
                                    <table id="attendances-datatable" class="table table-bordered table-striped" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Date</th>

                                                <th>Time In Am</th>
                                                <th>Time Out Am</th>
                                                <th>Time In Pm</th>
                                                <th>Time Out Pm</th>

                                                <th>Excuse Time In Am</th>
                                                <th>Excuse Time Out Am</th>
                                                <th>Excuse Time In Pm</th>
                                                <th>Excuse Time Out Pm</th>
                                                <th>Total Hours</th>
                                                <th>Hours</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Datatable --}}
    <div class="card card-primary card-outline">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="year">Year</label>
                        <input type="search" title="Select Year" name="year" id="year" class="form-control rounded-0" placeholder="Enter Year">
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="month">Month</label>

                        <select name="month" id="month" title="Select Month"
                            class="custom-select form-control-border border-width-2">
                            <?php foreach ($months as $month):?>
                                <option value="{{ $month['month_name'] }}" {{ (date('F') == $month['month_name']) ? "selected":"" }}>{{ $month['month_name'] }}</option>
                            <?php endforeach;?> 
                        </select>
                    </div>
                </div>
            </div>  
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3" id="employment-status-select">

                </div>
            </div>

            <hr class="mt-3 mb-3">

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>ID Number</th>
                                <th>Total Hours</th>
                                <th>Total Excuse Hours</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Script --}}
    <script>
        $(function() {
            var datas;

            var url = '';

            var month = moment().format('Y - MMMM');
            var title_name = '';

            var weekends = ['saturday', 'sunday'];

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy:true,
                processing:false,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                pagingType: 'full_numbers',
                ajax:{
                    url:base_url+'admin/payrolls/get-attendances',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        datatable.find('tbody').html(
                            '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                    },
                },
                columnDefs: [
                    {
                        targets: 5,
                        data: null,
                        orderable:false,
                        searchable:false,
                        render: function(data,type,row){
                            return `<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-flat">Action</button>
                                <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a data-id="${row.user_id}" name="${row.user.profile.firstname}" title="View Lists" style="cursor: pointer;" class="dropdown-item view"><i class="fa fa-table" aria-hidden="true"></i> View Attendance</a>
                                </div>
                            </div>`;
                        }
                    }
                ],
                columns: [
                    { data : 'user.profile.firstname' },
                    { data : 'user.profile.employee_role.position' },
                    { data : 'user.profile.employee_id_number' },
                    { data : 'total_hours' },
                    { data : 'total_excuse_hours' },
                    { data : 'id' },
                ],
            });

            datatable.on('click', '.view', function () {
                var dataId = $(this).attr('data-id');
                title_name = $(this).attr('name');
                $.ajax({
                    url: `${base_url}admin/payrolls/get-estimated-salary/${dataId}/${$('#month').val()}/${$('#year').val()}`,
                    method: 'GET',
                    dataType: 'JSON',
                    async: true,
                    success: function(data, textStatus, jqXHR) {
                        datas = data;
                    },
                    error: function(xhr, status, error) {
                        response(error, 'error');
                    }
                });

                $.ajax({
                    url: `${base_url}admin/payrolls/get-contribution-rate/${dataId}/${$('#month').val()}/${$('#year').val()}`,
                    method: 'GET',
                    dataType: 'JSON',
                    async: true,
                    success: function(data, textStatus, jqXHR) {
                        contributionDatas = data;
                    },
                    error: function(xhr, status, error) {
                        response(error, 'error');
                    }
                });
                
                var attendances_table = $('#attendances-datatable').DataTable({
                    "order": [
                        [2, 'asc']
                    ],
                    "dom": 'lBrtip',
                    "destroy": true,
                    "processing": false,
                    "serchDelay": 3500,
                    "deferRender": true,
                    "pagingType": 'full_numbers',
                    "ordering": false,
                    "responsive": true,
                    "language": {
                        "emptyTable": "No Payroll Data found"
                    },
                    "ajax": {
                        url: `${base_url}admin/payrolls/get-attendance-lists/${dataId}/${$('#month').val()}/${$('#year').val()}`,
                        method: 'GET',
                        dataType: 'JSON',
                        beforeSend: function() {
                            $('#attendances-datatable > tbody').html(
                                '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                            );
                        },
                        complete: function() {
                            // Enable export buttons after the AJAX request is complete
                            attendances_table.buttons().enable();
                        }
                    },
                    "columns": [{
                        data: null,
                        render: function(data, meta, row) {
                            return (row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution') ? '':row.user.profile.lastname + ', ' + row.user.profile.firstname +
                                ', ' + row.user.profile.middlename;
                        }
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            return (row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution') ? '':row.user.profile.employee_role.position + ' ' + row.user.profile
                                .employee_role.level;
                        }
                    }, {
                        data: null,
                        type: 'date',
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.attendance_date == null) ? "--/--":row.attendance_date;
                        }
                        // Divider Normal time in
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.timein_am == null) ? "--/--":row.timein_am;
                            // return row.timein_am;
                        }
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.timeout_am == null) ? "--/--":row.timeout_am;
                        }
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.timein_pm == null) ? "--/--":row.timein_pm;
                        }
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.timeout_pm == null) ? "--/--":row.timeout_pm;
                        }
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.excuse.timeout_am == null) ? "--/--":row.excuse.timeout_am;
                        }
                    }, { // Divider excuse time
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.excuse.timein_am == null) ? "--/--":row.excuse.timein_am;
                        }
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.excuse.timeout_pm == null) ? "--/--":row.excuse.timeout_pm;
                        }
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.excuse.timein_pm == null) ? "--/--":row.excuse.timein_pm;
                        }
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.attendance_total_hours == null) ? "--/--":row.attendance_total_hours;
                        }
                    }, {
                        data: null,
                        render: function(data, meta, row) {
                            if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution'){
                                return '';
                            }
                            return (row.minus_hours == null) ? "--/--":row.minus_hours;
                        }
                    }],
                    "buttons": [{
                        extend: 'excelHtml5',
                        attr: {
                            id: 'excel'
                        },
                        text: '<i class="far fa-file-excel"></i> .xlsx',
                        customize: function(xlsx) {
                            // Get salary computations
                            var excelMap = {
                                0: 'A', 1: 'B', 2: 'D', 3: 'E', 4: 'F', 5: 'G', 6: 'H', 7: 'I', 8: 'J', 9: 'K', 10: 'L', 11: 'M', 12: 'N',
                            };
                            // console.log(xlsx.xl['styles.xml']);
                            var new_style = xmlCode;
                            xlsx.xl['styles.xml'] = $.parseXML(new_style);

                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('c[r=A1]', sheet).attr('s', '24');
                            var count = 3;
                            var i = -2,
                                j = 0,
                                firstCountToMerge = null,
                                lastCountToMerge = null,
                                lastLoopJ = null,
                                lastLoopTD = null;
                            attendances_table.rows().every(function(rowIdx, tableLoop, rowLoop){
                                var data = this.data();
                                if(data.id == null){ // If data.id is equal to null
                                    if(data.is_non_working == true){
                                        for (td = 0; td < Object.keys(excelMap).length; td++) {
                                            $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '68');
                                        }
                                    }else{
                                        for (td = 0; td < Object.keys(excelMap).length; td++) {
                                            $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '27');
                                        }
                                    }
                                }else if(data.id == ''){ // If id is equal to empty string
                                    var mergeCellCount = parseInt($('mergeCells', sheet).attr('count'));
                                    $('mergeCells', sheet).attr('count', mergeCellCount++);
                                    $('mergeCells', sheet).append('<mergeCell ref="A'+count+':L'+count+'"></mergeCell>');
                                    $('c[r=A'+count+'] t', sheet).text(data.text);
                                    $('c[r=A'+count+']', sheet).attr('s', '24');
                                }else if(data.id == 'wfh'){
                                    // Change text node
                                    $('c[r=A'+count+'] t', sheet).text(data.A);
                                    $('c[r=B'+count+'] t', sheet).text(data.B);
                                    $('c[r=C'+count+'] t', sheet).text(data.C);
                                    $('c[r=D'+count+'] t', sheet).text(data.D);
                                    $('c[r=E'+count+'] t', sheet).text(data.E);
                                    // Change cell format
                                    $('c[r=A'+count+']', sheet).attr('s', '2');
                                    $('c[r=B'+count+']', sheet).attr('s', '2');
                                    $('c[r=C'+count+']', sheet).attr('s', '2');
                                    $('c[r=D'+count+']', sheet).attr('s', '2');
                                    $('c[r=E'+count+']', sheet).attr('s', '2');
                                }else if(data.id == 'empty'){
                                    var excelLetters = {
                                        0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F', 6: 'G', 7: 'H', 8: 'I', 9: 'J',
                                    };
                                    // Generate line for the computation of salary
                                    if(i >= 0){
                                        for (td = 0; td < Object.keys(excelLetters).length; td++) {
                                            if(i == 1){
                                                $('c[r='+excelLetters[td]+''+count+'] t', sheet).text(datas[i][td]);
                                                $('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '18');
                                            }else if(i == 2){
                                                $('c[r='+excelLetters[td]+''+count+'] t', sheet).text(datas[i][td]);
                                                $('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
                                            }else{
                                                $('c[r='+excelLetters[td]+''+count+'] t', sheet).text(datas[i][td]);
                                                $('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
                                            }
                                        }
                                    }
                                    i++;
                                }else if(data.id == 'contribution') {
                                    var excelLetters = {
                                        0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F',
                                    };

                                    // Generate line for the computation of salary
                                    if(contributionDatas.length < 8){
                                        if(contributionDatas[j] != null){
                                            if(j >= 0){
                                                for (td = 0; td < Object.keys(excelLetters).length; td++) {
                                                    if(j == 1){
                                                        $('c[r='+excelLetters[td]+''+count+'] t', sheet).text(contributionDatas[j][td]);
                                                        $('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '18');
                                                    }else if(j == 2){
                                                        firstCountToMerge = count;
                                                        $('c[r='+excelLetters[td]+''+count+'] t', sheet).text(contributionDatas[j][td]);
                                                        $('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
                                                    }else if(j > 2){
                                                        lastCountToMerge = count;
                                                        lastLoopJ = j;
                                                        lastLoopTD = td;
                                                        $('c[r='+excelLetters[td]+''+count+'] t', sheet).text(contributionDatas[j][td]);
                                                        $('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
                                                    }else{
                                                        $('c[r='+excelLetters[td]+''+count+'] t', sheet).text(contributionDatas[j][td]);
                                                        $('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
                                                    }
                                                }
                                            }
                                        }
                                            
                                    }else{
                                        var mergeCellCount = parseInt($('mergeCells', sheet).attr('count'));
                                        $('mergeCells', sheet).attr('count', mergeCellCount++);
                                        $('mergeCells', sheet).append('<mergeCell ref="A'+count+':F'+count+'"></mergeCell>');
                                        $('c[r=A'+count+'] t', sheet).text('Contributions limit is up to 5 only');
                                        $('c[r=A'+count+']', sheet).attr('s', '25');
                                    }
                                    
                                    j++;
                                }else{ // If id not null
                                    if(data.is_non_working == true){
                                        for (td = 0; td < Object.keys(excelMap).length; td++) {
                                            $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '53');
                                        }
                                    }else if (data.is_work_from_home == 1){
                                        for (td = 0; td < 11; td++) {
                                            $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '30');
                                        }
                                    }else{
                                        for (td = 0; td < 11; td++) {
                                            $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '3');
                                        }
                                    }
                                }
                                count++;
                            });
                            if(firstCountToMerge != null &&
                            lastCountToMerge != null &&
                            lastLoopJ != null &&
                            lastLoopTD != null){
                                var mergeCellCount = parseInt($('mergeCells', sheet).attr('count'));
                                $('mergeCells', sheet).attr('count', mergeCellCount++);
                                $('mergeCells', sheet).append('<mergeCell ref="F'+firstCountToMerge+':F'+lastCountToMerge+'"></mergeCell>');
                                $('c[r=F'+firstCountToMerge+'] t', sheet).text(contributionDatas[lastLoopJ][lastLoopTD]);
                                $('c[r=F'+firstCountToMerge+']', sheet).attr('s', '70');
                            }
                            // console.log(sheet);

                        },
                        createEmptyCells: true,
                        title: `${moment().format('MMMM')}_${moment().format('YYYY')}_${title_name}`,
                        tag: 'button',
                        className: 'btn btn-flat btn-success rounded-bottom border-top-0 border-default px-5',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title: 'Export To Excel',
                                text: 'Are You Sure?',
                                icon: 'info',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function(result) {
                                if (result.isConfirmed) {
                                    setTimeout(function() {
                                        $.fn.dataTable.ext.buttons.excelHtml5.action
                                            .call(dt.button(this), e, dt, node,
                                                config);
                                    }, 1000);
                                }
                            });
                        },
                        footer: false
                    }],
                    "rowCallback": function(row, data, index) {
                        if (data.id == null) {
                            $(row).css("background-color", "yellow");
                        }
                        if(data.is_non_working){
                            $(row).css("color", "red");
                        }
                        if(data.id == 'empty' || data.id == '' || data.id == 'wfh'){
                            $(row).hide();
                        }
                    },
                });

                attendances_table.buttons().disable();
                
                $('#modal').modal('show');

            });

            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            function response(title, icon) {
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }

            $('#year').yearpicker({
                onShow:null,
                markAsBold: true,
                onChange : function(value){
                    if(value != null){
                        table.ajax.url(`${base_url}admin/payrolls/get-attendances/${$('#month').val()}/${value}`).load();
                    }
                },
                allowCustomValue: true,
            });

            $('#month').on('change', function(e){
                e.preventDefault();
                table.ajax.url(`${base_url}admin/payrolls/get-attendances/${$(this).val()}/${$('#year').val()}`).load();
            });

            $(document).on('shown.bs.modal', '#modal', function () {
               $('#attendances-datatable').DataTable().columns.adjust().responsive.recalc();
            });

            var xmlCode =`<?xml version="1.0" encoding="UTF-8"?>
                <styleSheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"
                    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac"
                    xmlns:x14ac="https://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">
                    <numFmts count="2">
                        <numFmt numFmtId="171" formatCode="d/mm/yyyy;@"/>
                        <numFmt numFmtId="172" formatCode="m/d/yyyy;@"/>
                    </numFmts>
                    <fonts count="10" x14ac:knownFonts="1">
                        <font>
                            <sz val="11"/>
                            <color theme="1"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                        <font>
                            <sz val="11"/>
                            <color theme="1"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                        <font>
                            <b/>
                            <sz val="11"/>
                            <color theme="1"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                        <font>
                            <sz val="11"/>
                            <color theme="0"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                        <font>
                            <i/>
                            <sz val="11"/>
                            <color theme="1"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                        <font>
                            <sz val="11"/>
                            <color rgb="FFC00000"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                        <font>
                            <sz val="11"/>
                            <color rgb="FF006600"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                        <font>
                            <sz val="11"/>
                            <color rgb="FF990033"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                        <font>
                            <sz val="11"/>
                            <color rgb="FF663300"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                        <font>
                            <b/>
                            <sz val="11"/>
                            <color rgb="FFC00000"/>
                            <name val="Calibri"/>
                            <family val="2"/>
                            <scheme val="minor"/>
                        </font>
                    </fonts>
                    <fills count="16">
                        <fill>
                            <patternFill patternType="none"/>
                        </fill>
                        <fill>
                            <patternFill patternType="gray125"/>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FFC00000"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FFFF0000"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FFFFC000"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FFFFFF00"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FF92D050"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FF00B050"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FF00B0F0"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FF0070C0"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FF002060"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FF7030A0"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor theme="1"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FF99CC00"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FFFF9999"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                        <fill>
                            <patternFill patternType="solid">
                                <fgColor rgb="FFFFCC00"/>
                                <bgColor indexed="64"/>
                            </patternFill>
                        </fill>
                    </fills>
                    <borders count="2">
                        <border>
                            <left/>
                            <right/>
                            <top/>
                            <bottom/>
                            <diagonal/>
                        </border>
                        <border>
                            <left style="thin">
                                <color indexed="64"/>
                            </left>
                            <right style="thin">
                                <color indexed="64"/>
                            </right>
                            <top style="thin">
                                <color indexed="64"/>
                            </top>
                            <bottom style="thin">
                                <color indexed="64"/>
                            </bottom>
                            <diagonal/>
                        </border>
                    </borders>
                    <cellStyleXfs count="3">
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="0"/>
                        <xf numFmtId="9" fontId="1" fillId="0" borderId="0" applyFont="0" applyFill="0" applyBorder="0" applyAlignment="0" applyProtection="0"/>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
                            <alignment vertical="center"/>
                        </xf>
                    </cellStyleXfs>
                    <cellXfs count="73">
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0"/>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="2" fillId="0" borderId="0" xfId="0" applyFont="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="4" fillId="0" borderId="0" xfId="0" applyFont="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="4" fillId="0" borderId="0" xfId="0" applyFont="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="4" fillId="0" borderId="0" xfId="0" applyFont="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyBorder="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
                            <alignment vertical="top" wrapText="1"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyBorder="1" applyAlignment="1">
                            <alignment vertical="top" wrapText="1"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="2" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="3" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="4" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="5" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="6" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="7" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="8" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="9" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="10" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="11" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="12" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="2" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="3" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="4" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="5" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="6" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="7" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="8" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="9" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="10" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="11" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="12" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="2" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="3" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="4" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="5" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="6" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="7" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="8" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="9" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="10" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="11" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="3" fillId="12" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top" textRotation="90"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="center" textRotation="255"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
                            <alignment textRotation="45"/>
                        </xf>
                        <xf numFmtId="0" fontId="5" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="5" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="5" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="5" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="5" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="5" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="right" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="6" fillId="13" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="6" fillId="13" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyBorder="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="7" fillId="14" borderId="0" xfId="1" applyNumberFormat="1" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="7" fillId="14" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyBorder="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="8" fillId="15" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="8" fillId="15" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyBorder="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyBorder="1" applyAlignment="1">
                            <alignment vertical="top"/>
                        </xf>
                        <xf numFmtId="171" fontId="0" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="172" fontId="0" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="171" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="172" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="171" fontId="9" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="172" fontId="9" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="171" fontId="9" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="172" fontId="9" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
                            <alignment horizontal="center" vertical="top"/>
                        </xf>
                        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
                            <alignment horizontal="center" vertical="center"/>
                        </xf>
                    </cellXfs>
                    <cellStyles count="3">
                        <cellStyle name="Procent" xfId="1" builtinId="5"/>
                        <cellStyle name="Standaard" xfId="0" builtinId="0"/>
                        <cellStyle name="MergedCell" xfId="2"/>
                    </cellStyles>
                    <dxfs count="0"/>
                    <tableStyles count="0" defaultTableStyle="TableStyleMedium2" defaultPivotStyle="PivotStyleLight16"/>
                    <colors>
                        <mruColors>
                            <color rgb="FF663300"/>
                            <color rgb="FFFFCC00"/>
                            <color rgb="FF990033"/>
                            <color rgb="FF006600"/>
                            <color rgb="FFFF9999"/>
                            <color rgb="FF99CC00"/>
                        </mruColors>
                    </colors>
                    <extLst>
                        <ext uri="{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}"
                            xmlns:x14="https://schemas.microsoft.com/office/spreadsheetml/2009/9/main">
                            <x14:slicerStyles defaultSlicerStyle="SlicerStyleLight1"/>
                        </ext>
                    </extLst>
                </styleSheet>`;
        });
    </script>

@endsection
