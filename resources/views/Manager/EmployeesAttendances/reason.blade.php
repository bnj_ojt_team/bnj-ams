<?php

use Dompdf\Dompdf;

$attendance_date = $reason->attendance->attendance_date;
$profile_name = $reason->attendance->user->profile->firstname.' '.$reason->attendance->user->profile->middlename.' '.$reason->attendance->user->profile->lastname;

class XPDF extends Dompdf{

}

$html = '';
// instantiate and use the dompdf class
$dompdf = new XPDF();

$html .= '<h1>Reason</h1>';
$html .= '<small style="position: absolute; float: right; top: 2.0em;">'.$profile_name.'</small>';
$html .= '<small style="position: relative; float: right; bottom: 1.8em;">'.$attendance_date.'</small>';

$html .= '<div style="width: 100%; height: auto; padding:1em;">';
$html .= $reason->content;
$html .= '</div>';

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('letter', 'portrait');

$dompdf->addInfo('Title', $reason->attendance->user->profile->firstname);
// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream(uniqid().'.pdf',['Attachment' => false]);

exit;
