@extends('Manager.layout.default')
@section('title', 'Manager.Employees Attendances')
@section('content')

    @php
        $months = [
            ['month_name' => ucwords('january'),'month' => 1],
            ['month_name' => ucwords('february'),'month' => 2],
            ['month_name' => ucwords('march'),'month' => 3],
            ['month_name' => ucwords('april'),'month' => 4],
            ['month_name' => ucwords('may'),'month' => 5],
            ['month_name' => ucwords('june'),'month' => 6],
            ['month_name' => ucwords('july'),'month' => 7],
            ['month_name' => ucwords('august'),'month' => 8],
            ['month_name' => ucwords('september'),'month' => 9],
            ['month_name' => ucwords('october'),'month' => 10],
            ['month_name' => ucwords('november'),'month' => 11],
            ['month_name' => ucwords('december'),'month' => 12]
        ];
    @endphp

    <link rel="stylesheet" href="{{asset('yearpicker/css/yearpicker.css')}}">
    <script src="{{asset('yearpicker/js/yearpicker.js')}}"></script>



    {{-- Datatable --}}
    <div class="row">
        <div class="col-12">

            <!-- /.card -->

            <div class="card">
                <div class="card-header">
                    {{--<h3 class="card-title">Attendances Lists</h3>--}}
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="profiles">Employees</label>
                                        <select name="profiles" id="profiles" class="form-control rounded-0">
                                            <option value="">Choose Employee</option>
                                            @foreach($profiles as $key => $value)
                                                <option value="{{$value}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="year">Year</label>
                                        <input type="search" title="Select Year" name="year" id="year" class="form-control rounded-0" placeholder="Enter Year">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="month">Month</label>

                                        <select name="month" id="month" title="Select Month"
                                            class="custom-select form-control-border border-width-2">
                                            <?php foreach ($months as $month):?>
                                                <option value="{{ $month['month'] }}" {{ (date('F') == $month['month_name']) ? "selected":"" }}>{{ $month['month_name'] }}</option>
                                            <?php endforeach;?> 
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12" id="row-dt-buttons">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-bordered table-striped">
                                    <thead class="text-center">
                                        <tr>
                                            <th>First Name</th>
                                            <th>Year</th>
                                            <th>Day</th>
                                            <th>TimeIn(AM)</th>
                                            <th>TimeOut(AM)</th>
                                            <th>TimeIn(PM)</th>
                                            <th>TimeOut(PM)</th>
                                            <th>Excuse(AM)</th>
                                            <th>Excuse(AM)</th>
                                            <th>Excuse(PM)</th>
                                            <th>Excuse(PM)</th>
                                            <th>Total Excuse Hour</th>
                                            <th>Total Hours (WFH)</th>
                                            <th>Total Hours</th>
                                            <th>Reports</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

    {{-- Script --}}
    <script>
        $(function() {
            var url = '';

            // Datatable
            var datatable_instance = $("#datatable").DataTable({

                "order": [
                    [0, 'Asc']
                ],
                "search": {
                    "regex": true
                },
                "dom": "lBrtip",
                "responsive": true,
                "language": {
                    "emptyTable": "No Attendances Data found!"
                },

                "ajax": {
                    url: base_url + 'manager/employees-attendances/get-attendances',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function() {
                        $('#datatable > tbody').html(
                            '<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
                        );
                    },
                    complete: function() {
                        // Enable export buttons after the AJAX request is complete
                        datatable_instance.buttons().enable();
                    }
                },
                initComplete: function() {
                    // Apply the search
                    var api = this.api();

                    const year = $('#year');
                    const month = $('#month');
                    var val = '';

                    year.yearpicker({
                        onShow: null,
                        onChange: function(value) {
                            if (value != null) {
                                if (month.val()) {
                                    val = value + '/' + month.val();
                                } else {
                                    val = value + '/';
                                }
                                return api.columns([1]).search(val, true, true).draw();
                            }
                        },
                        markAsBold: true,
                        allowCustomValue: true,
                    });

                    month.on('change', function() {
                        val = '/' + this.value + '/';

                        if (year.val() && this.value.length == 0) {
                            val = year.val() + '/';
                            return api.columns([1]).search(val, true, true).draw();
                        }

                        if (year.val()) {
                            val = year.val() + '/' + this.value;
                            return api.columns([1]).search(val, true, true).draw();
                        }
                        return api.columns([1]).search(val, true, true).draw();

                        //return api.columns([2]).search(this.value, true, false).draw();
                    });

                    year.on('keyup keydown', function(e) {

                        if (e.key.match(/^([0-9]{1,})$/g) || e.keyCode == 8) {
                            if (month.val()) {
                                val = this.value + '/' + month.val();
                            } else {
                                val = this.value + '/';
                            }
                            return api.columns([1]).search(val, true, true).draw();
                        } else {
                            return e.preventDefault();
                        }

                    });

                    $('#profiles').on('change', function() {
                        return api.columns([0]).search((this.value)? '^' + (this.value) + '$': '' , true, false).draw();
                    });

                },
                buttons:[
                    {
                        extend: 'excelHtml5',
                        attr:  {
                            id: 'excel'
                        },
                        title: function filename() {
                            var profiles = ($('#profiles').val())? $('#profiles').val(): 'Attendances';
                            var year = ($('#year').val())? $('#year').val(): moment().format('Y');
                            var month = ($('#month').val())? $('#month option:selected').text(): moment().format('MMMM');
                            return profiles+' '+month+' '+year;
                        },
                        text: '<i class="far fa-file-excel"></i> .xlsx',
                        tag: 'button',
                        className:'btn btn-flat btn-success rounded-top border-top-0 border-dark px-5',
                        exportOptions: {
                            columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title:'Export To Excel',
                                text:'Are You Sure?',
                                icon: 'info',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    setTimeout(function(){
                                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                                    }, 1000);
                                }
                            });
                        },
                        footer:true
                    },
                ],
                "columns": [
                    { data: 'id'},
                    { data: 'created_at'},
                    { data: 'created_at'},
                    { data: 'timein_am',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'timeout_am',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'timein_pm',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'timeout_pm',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'excuse.timein_am',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'excuse.timeout_am',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'excuse.timein_pm',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'excuse.timeout_pm',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'excuse.excuse_total_hours',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'id'},

                    { data: 'excuse.excuse_total_hours',
                    render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
                },
                    { data: 'id'}
                ],
                "columnDefs": [
                    {
                        targets: 0,
                        data: null,
                        render: function(data, type, row) {
                            return row.user.profile.firstname;
                        }
                    },
                    {
                        targets: 1,
                        data: null,
                        render: function(data, type, row) {
                            return moment(row.attendance_date).format('Y/M/DD');
                        }
                    },
                    {
                        targets: 2,
                        data: null,
                        render: function(data, type, row) {
                            return moment(row.attendance_date).format('dddd');
                        }
                    },
                    {
                        targets: 12,
                        data: null,
                        render: function(data, type, row) {
                            var total_work_hours = 0;
                            // console.log(row.accomplishment);
                            if(row.accomplishment != null){
                                $.map(row.accomplishment.home_attendances,function (data) {
                                    total_work_hours += parseFloat(data.total_work_hours);
                                });
                            }

                            return parseFloat(total_work_hours);
                        }
                    },
                    {
                        targets: 14,
                        data: null,
                        render: function(data, type, row) {

                            var accomplishment = '';
                            if(row.accomplishment){
                                accomplishment +='<a data-id="'+row.id+'" title="View Accomplishment" class="btn btn-danger accomplishments">' +
                                    '<i class="fa fa-file-pdf"></i></a>';
                            }

                            var reason = '';
							$.map(row.reason, function(data) {
								reason += ' | <a data-id="' + data.id + '" title="' + (data
										.report_type) + '" class="btn btn-info reason">' +
									'<i class="fa fa-file-word"></i></a>';
							});

                            return accomplishment + reason;
                        }
                    },
                ],
            });
            datatable_instance.buttons().disable();

            // Click Add
            $(document).on('click', '#add', function() {
                url = base_url + 'manager/quotes/add';
                $('#save-button').html('Save');
                $('#modal-title').html('Add new Quote');
                $('#modal').modal('show');
            });

            // Click Edit
            datatable_instance.on('click', '.edit', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                url = base_url + 'manager/quotes/edit/' + dataId;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        $('#quotes').val(data.quotes);
                        $('#is_active').prop('checked', data.is_active);
                        $('#save-button').html('Update Changes');
                        $('#modal-title').html('Edit Quote');
                        $('#modal').modal('show');
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.url(base_url + 'manager/quotes/get-all-quotes')
                            .load();
                        response(error, 'error');
                    }
                });
            });

            // Form submit
            $(document).on('submit', '#form', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                if(url === base_url + 'manager/quotes/add'){
                    $('#save-button').html('Saving <i class="fas fa-spinner"></i>');
                }else{
                    $('#save-button').html('Updating <i class="fas fa-spinner"></i>');
                }
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    type: 'json',
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $('#modal').modal('hide');
                        response(data.title, data.icon);
                        datatable_instance.ajax.url(base_url + 'manager/quotes/get-all-quotes')
                            .load();
                    },
                    error: function(xhr, status, error) {
                        if(url === base_url + 'manager/quotes/add'){
                            $('#save-button').html('Save');
                        }else{
                            $('#save-button').html('Update Changes');
                        }
                        var title = '';
                        var validation = JSON.parse(xhr.responseText);
                        title = validation.errors ? validation.errors[Object.keys(validation
                            .errors)[0]] : validation.title;
                        response(title, 'error');
                        datatable_instance.ajax.url(base_url + 'manager/quotes/get-all-quotes')
                            .load();
                    }
                });
            });

            // Hidden modal
            $('#modal').on('hidden.bs.modal', function() {
                url = '';
                $('#save-button').html('');
                $('#modal-title').html('');
                $('#form')[0].reset();
            });

            // Onclick active
            datatable_instance.on('click', '.active', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var dataProcess = $(this).attr('data-process');
                // var href = 'isActive/'+dataId;
                var href = base_url + 'manager/quotes/is-active/' + dataId + '/' + dataProcess;
                $.ajax({
                    url: href,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data, textStatus, jqXHR) {
                        datatable_instance.ajax.url(base_url + 'manager/quotes/get-all-quotes')
                            .load();
                        response(data.title, data.icon);
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.url(base_url + 'manager/quotes/get-all-quotes')
                            .load();
                        response(error, 'error');
                    }
                })
            });

            // Delete Function
            datatable_instance.on('click', '.delete', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = base_url + 'manager/quotes/delete/' + dataId;
                Swal.fire({
                    title: 'Delete Quote?',
                    text: 'Are You Sure you want to delete this Quote?',
                    icon: 'warning',
                    animation: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                }).then(function(result) {
                    if (result.isConfirmed) {
                        let timerInterval;
                        Swal.fire({
                                title: 'Deleting!',
                                timer: 10000,
                                animation: false,
                                timerProgressBar: false,
                                didOpen: () => {
                                Swal.showLoading();
                        const b = Swal.getHtmlContainer().querySelector('b');
                        timerInterval = setInterval(() => {
                            b.textContent = Swal.getTimerLeft()
                    }, 100);

                        $.ajax({
                            url: href,
                            type: 'DELETE',
                            method: 'DELETE',
                            dataType: 'JSON',
                            success: function(data, textStatus, jqXHR) {
                                Swal.close();
                                datatable_instance.ajax.url(
                                    base_url +
                                    'manager/quotes/get-all-quotes'
                                ).load();
                                response(data.title, data.icon);
                            },
                            error: function(xhr, status, error) {
                                datatable_instance.ajax.url(
                                    base_url +
                                    'manager/quotes/get-all-quotes'
                                ).load();
                                response(error, 'error');
                            }
                        });
                    },
                        willClose: () => {
                            clearInterval(timerInterval);
                        }
                    });
                    }
                });
            });

            // Accomplishment Function
            datatable_instance.on('click', '.accomplishments', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = base_url + 'manager/employees-attendances/accomplishment/' + dataId;
                Swal.fire({
                    title: 'View Accomplishment?',
                    text: 'Are You Sure?',
                    icon: 'info',
                    animation: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                }).then(function(result) {
                    if (result.isConfirmed){
                        window.open(href);
                    }
                });
            });

            // Reason Function
            datatable_instance.on('click', '.reason', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = base_url + 'manager/employees-attendances/reason/' + dataId;
                Swal.fire({
                    title: 'View Reason?',
                    text: 'Are You Sure?',
                    icon: 'info',
                    animation: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                }).then(function(result) {
                    if (result.isConfirmed){
                        window.open(href);
                    }
                });
            });

            // Sweetalert
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            function response(title, icon) {
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }

        });
    </script>
@endsection
