<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">{{ ucwords(__('BNJ-AMS')) }}</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ (auth()->user()->image == null) ? asset('preview.png'):asset('storage/avatars/'.auth()->user()->image) }}" class="img-circle elevation-2" alt="User Image" style="height: 2.1rem; width: 2.1rem;">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ auth()->user()->username }}</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-header">{{ strtoupper(__('generals')) }}</li>
                <li class="nav-item">
                    <a href="{{ route('manager.dashboard.index') }}" class="nav-link {{ (url()->current() == route('manager.dashboard.index')) ? "active":"" }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{ ucfirst(__('Dashboard')) }}
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('manager.employees.index') }}" class="nav-link {{ (url()->current() == route('manager.employees.index')) ? "active":"" }}">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                           {{ ucfirst(__('Employees')) }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('manager.employees-attendances.index') }}" class="nav-link {{ (url()->current() == route('manager.employees-attendances.index')) ? "active":"" }}">
                        <i class="nav-icon fas fa-clock"></i>
                        <p>
                            {{ ucfirst(__('Employees Attendances')) }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('manager.attendances.index') }}" class="nav-link {{ (url()->current() == route('manager.attendances.index')) ? "active":"" }}">
                        <i class="nav-icon fas fa-clock"></i>
                        <p>
                            {{ ucfirst(__('My Attendances')) }}
                        </p>
                    </a>
                </li>

                {{-- Salary Loans and deduction --}}
                <li class="nav-item {{ in_array(url()->current(), [
                        route('manager.salary-deductions.index'),
                        route('manager.salary-loans.index')
                    ])
                        ? 'menu-is-opening menu-open'
                        : '' }}">
                    <a href="#"
                        class="nav-link {{ in_array(url()->current(), [
                            route('manager.salary-deductions.index'),
                            route('manager.salary-loans.index')
                        ])
                            ? 'active'
                            : '' }}">
                        <i class="nav-icon fas fa-hand-holding-usd"></i>
                        <p>
                            {{ ucfirst(__('Salary')) }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{ route('manager.salary-loans.index') }}" class="nav-link {{ (url()->current() == route('manager.salary-loans.index')) ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('Salary Loans')) }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('manager.salary-deductions.index') }}" class="nav-link {{ (url()->current() == route('manager.salary-deductions.index')) ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ ucfirst(__('Salary Deductions')) }}</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('manager.payroll.index') }}" class="nav-link {{ (url()->current() === route('manager.payroll.index')) ? 'active':'' }}">
                        <i class="nav-icon fas fa-money-check-alt"></i>
                        <p>
                            {{ ucfirst(__('Payroll')) }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('manager.employees-payroll.index') }}" class="nav-link {{ (url()->current() === route('manager.employees-payroll.index')) ? 'active':'' }}">
                        <i class="nav-icon fas fa-money-check-alt"></i>
                        <p>
                            {{ ucfirst(__('Employees Payroll')) }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('manager.contributions.index') }}" class="nav-link {{ (url()->current() === route('manager.contributions.index')) ? 'active':'' }}">
                        <i class="nav-icon fas fa-donate"></i>
                        <p>
                            {{ ucfirst(__('Contributions')) }}
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
