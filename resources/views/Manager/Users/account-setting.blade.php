@extends('Manager.layout.default')
@section('title', 'Manager.Account Setting')
@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link btn border-bottom-0 border-left-0 border-right-0 border-primary rounded-top active" href="#account_tab"
                                        data-toggle="tab">
                                        Account
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link btn border-bottom-0 border-left-0 border-right-0 border-primary rounded-top" href="#profile_tab" data-toggle="tab">
                                        Profile
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link btn border-bottom-0 border-left-0 border-right-0 border-primary rounded-top" href="#contact_tab" data-toggle="tab">
                                        Contact
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link btn border-bottom-0 border-left-0 border-right-0 border-primary rounded-top" href="#password_tab" data-toggle="tab">
                                        Password
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                {{-- Account Panel --}}
                                <div class="active tab-pane" id="account_tab">
                                    <form action="" method="" id="account-form">
                                        @csrf
                                        {{-- User Model Forms --}}
                                        {{-- Content Here --}}
                                        <div class="row">
                                            <div class="card w-100">
                                                <div class="card-header bg-secondary">
                                                    <h3 class="card-title">Update Account</h3>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="input-group mb-3 d-flex justify-content-center col-4">
                                                            <center>
                                                                <img src="{{ ($user->image == null) ? asset('preview.png'):asset('storage/avatars/'.$user->image) }}"
                                                                    alt="" id="file-preview"
                                                                    style="height: 4rem; width: 4rem; border-radius: 50%; border: 1px solid rgb(184, 184, 184);"><br>
                                                                <label for="">Avatar</label>
                                                                <button type="button" class="btn btn-secondary btn-block rounded-0"
                                                                    id="change-profile-image">Change Profile Image</button>
                                                                <input name="file" type="file" class="form-control rounded-0"
                                                                    id="file" placeholder="Full name" accept="image/*"
                                                                    style="display: none;">
                                                            </center>
                                                        </div>
                                                        <div class="col-8">

                                                            <label>Username</label>
                                                            <div class="input-group mb-3">
                                                                <input type="text" class="form-control rounded-0" name="username"
                                                                    id="username" placeholder="Enter Username"
                                                                    value="{{ $user->username }}">
                                                                <div class="input-group-append">
                                                                    <div class="input-group-text">
                                                                        <span class="fas fa-user"></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <label>Email</label>
                                                            <div class="input-group mb-3">
                                                                <input type="text" class="form-control rounded-0" name="email"
                                                                    id="email" placeholder="Enter Email"
                                                                    value="{{ $user->email }}">
                                                                <div class="input-group-append">
                                                                    <div class="input-group-text">
                                                                        <span class="fas fa-envelope"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary btn-block rounded-0">Update
                                                                Changes</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {{-- Profile Panel --}}
                                <div class="tab-pane" id="profile_tab">
                                    <form action="" method="" id="profile-form">
                                        @csrf
                                        {{-- User Model Forms --}}
                                        {{-- Content Here --}}
                                        <div class="row">
                                            <div class="card w-100">
                                                <div class="card-header bg-secondary">
                                                    <h3 class="card-title">Employee's Profile</h3>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="form-group">
                                                                <label for="">Firstname</label>
                                                                <input type="text" name="profile[firstname]"
                                                                    class="form-control rounded-0" id="firstname"
                                                                    placeholder="Enter Firstname" value="{{ $user->profile->firstname }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="form-group">
                                                                <label for="">Middlename</label>
                                                                <input type="text" name="profile[middlename]"
                                                                    class="form-control rounded-0" id="middlename"
                                                                    placeholder="Enter Middlename" value="{{ $user->profile->middlename }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="form-group">
                                                                <label for="">Lastname</label>
                                                                <input type="text" name="profile[lastname]"
                                                                    class="form-control rounded-0" id="lastname"
                                                                    placeholder="Enter Lastname" value="{{ $user->profile->lastname }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="">ID Number</label>
                                                                <input type="number" name="profile[employee_id_number]"
                                                                    class="form-control rounded-0" id="employee_id_number"
                                                                    placeholder="Enter ID Number"  value="{{ $user->profile->employee_id_number }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="">Birthdate</label>
                                                                <div class="input-group date" id="datepickerbirthdate"
                                                                    data-target-input="nearest">
                                                                    <input type="text"
                                                                        class="form-control datetimepicker-input rounded-0"
                                                                        data-target="#datepickerbirthdate" name="profile[birthdate]"
                                                                        id="birthdate" placeholder="Date Of Birth" value="{{ $user->profile->birthdate }}">
                                                                    <div class="input-group-append" data-target="#datepickerbirthdate"
                                                                        data-toggle="datetimepicker">
                                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="">Date Employed</label>
                                                                <div class="input-group date" id="datepickerdateemployed"
                                                                    data-target-input="nearest">
                                                                    <input type="text"
                                                                        class="form-control datetimepicker-input rounded-0"
                                                                        data-target="#datepickerdateemployed"
                                                                        value="{{ $user->profile->date_employed }}"
                                                                        name="profile[date_employed]" id="date_employed"
                                                                        placeholder="Date Employed" disabled>
                                                                    <div class="input-group-append" data-target="#datepickerdateemployed"
                                                                        data-toggle="datetimepicker">
                                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="">Employment Status</label>
                                                                <input type="text" name="profile[employment_status]" class="form-control rounded-0" value="{{ $user->profile->employment_status }}" disabled>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="">Contact Number</label>
                                                                <input type="text" class="form-control rounded-0"
                                                                    name="profile[contact_number]" id="contact_number"
                                                                    placeholder="Contact Number"
                                                                    value="{{ $user->profile->contact_number }}"
                                                                    data-inputmask="'mask': ['9999-999-9999', '+63999-999-9999']"
                                                                    data-mask>
                                                                <label for="">Position</label>
                                                                <input type="text" name="profile[employee_role_id]" class="form-control rounded-0" value="{{ $user->profile->employeeRole->position.' '.$user->profile->employeeRole->level }}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="">Address</label>
                                                                <textarea name="profile[address]" class="form-control rounded-0" id="address" placeholder="Enter Address"
                                                                    row="3">{{ $user->profile->address }}</textarea>
                                                            </div>
                                                            <button type="submit"
                                                                class="btn btn-primary btn-block rounded-0">Update
                                                                Changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {{-- Contact Panel --}}
                                <div class="tab-pane" id="contact_tab">
                                    <form action="" method="" id="contact-form">
                                        @csrf
                                        {{-- User Model Forms --}}
                                        {{-- Content Here --}}
                                        <div class="row">
                                            <div class="card w-100">
                                                <div class="card-header bg-secondary">
                                                    <h3 class="card-title">Update Contact</h3>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="">Fullname</label>
                                                                <input type="text" name="contact[fullname]"
                                                                    class="form-control rounded-0" id="fullname"
                                                                    placeholder="Enter Fullname" value="{{ $user->contact->fullname }}">
                                                                <label for="">Relation</label>
                                                                <input type="text" name="contact[relation]"
                                                                    class="form-control rounded-0" id="relation"
                                                                    placeholder="Enter Relation" value="{{ $user->contact->relation }}">
                                                                <label for="">Phone Number</label>
                                                                <input type="text" class="form-control rounded-0"
                                                                    name="contact[phone_number]" id="phone_number"
                                                                    placeholder="Phone Number"
                                                                    data-inputmask="'mask': ['9999-999-9999', '+63999-999-9999']"
                                                                    data-mask value="{{ $user->contact->phone_number }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="">Address</label>
                                                                <textarea name="contact[address]" class="form-control rounded-0" id="contact-address" placeholder="Enter Address"
                                                                    row="3">{{ $user->contact->address }}</textarea>
                                                            </div>
                                                            <button type="submit"
                                                                class="btn btn-primary btn-block rounded-0">Update
                                                                Changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {{-- Password Panel --}}
                                <div class="tab-pane" id="password_tab">
                                    <div class="row">
                                        <div class="card w-100">
                                            <div class="card-header bg-secondary">
                                                <h3 class="card-title">Change Password</h3>
                                            </div>
                                            <div class="card-body">
                                                <form action="" method="" id="password-form">
                                                    @csrf
                                                    {{-- User Model Forms --}}
                                                    {{-- Content Here --}}
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <label>Current Password</label>
                                                            <div class="input-group mb-3">
                                                                <input type="password" class="form-control rounded-0"
                                                                    name="current-password" id="current-password"
                                                                    placeholder="Enter Current Password">
                                                                <div class="input-group-append">
                                                                    <div class="input-group-text">
                                                                        <span class="fas fa-eye-slash"
                                                                            id="current-password-eye"></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <label>New Password</label>
                                                            <div class="input-group mb-3">
                                                                <input type="password" class="form-control rounded-0"
                                                                    name="password" id="password"
                                                                    placeholder="Enter New Password">
                                                                <div class="input-group-append">
                                                                    <div class="input-group-text">
                                                                        <span class="fas fa-eye-slash"
                                                                            id="password-eye"></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <label>Confirm Password</label>
                                                            <div class="input-group mb-3">
                                                                <input type="password" class="form-control rounded-0"
                                                                    name="confirm-password" id="confirm-password"
                                                                    placeholder="Re Enter Password">
                                                                <div class="input-group-append">
                                                                    <div class="input-group-text">
                                                                        <span class="fas fa-eye-slash"
                                                                            id="confirm-password-eye"></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <button type="reset" class="btn btn-danger rounded-0 col-2">Reset
                                                                Form</button>
                                                            <button type="submit"
                                                                class="btn btn-primary col-5 float-right rounded-0">Update
                                                                Changes</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function() {
            // Account Form
            $('[type=submit]').on('click', function(){
                $(this).html('Updating <i class="fas fa-spinner"></i>');
            });

            // Image upload on click and preview
            $('#file').on('change', function(event) {
                var image = URL.createObjectURL(event.target.files[0]);
                $('#file-preview').attr('src', image);
                $('#profile-circle').attr('src', image);
            });
            $('#change-profile-image').on('click', function() {
                $('#file').click();
            });

            // Forms Submit
            $(document).on('submit', '#password-form, #account-form, #profile-form, #contact-form', function(e) {
                e.preventDefault();
                let data = new FormData(this);
                url = base_url + 'manager/users/edit';
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    type: 'json',
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $('#modal').modal('hide');
                        $('#password-form')[0].reset();
                        response(data.title, data.icon);
                        $('[type=submit]').html('Update Changes');
                    },
                    error: function(xhr, status, error) {
                        var title = '';
                        var validation = JSON.parse(xhr.responseText);
                        title = validation.errors ? validation.errors[Object.keys(validation
                            .errors)[0]] : validation.title;
                        response(title, 'error');
                        $('[type=submit]').html('Update Changes');
                    }
                });
            });

            // Current Password click
            $('#current-password-eye').on('click', function() {
                if ($(this).attr('class') == 'fas fa-eye-slash') {
                    $('#current-password').attr('type', 'text');
                    $(this).attr('class', 'fas fa-eye');
                } else {
                    $('#current-password').attr('type', 'password');
                    $(this).attr('class', 'fas fa-eye-slash');
                }
            });
            // New Password click
            $('#password-eye').on('click', function() {
                if ($(this).attr('class') == 'fas fa-eye-slash') {
                    $('#password').attr('type', 'text');
                    $(this).attr('class', 'fas fa-eye');
                } else {
                    $('#password').attr('type', 'password');
                    $(this).attr('class', 'fas fa-eye-slash');
                }
            });
            // Password Confirm click
            $('#confirm-password-eye').on('click', function() {
                if ($(this).attr('class') == 'fas fa-eye-slash') {
                    $('#confirm-password').attr('type', 'text');
                    $(this).attr('class', 'fas fa-eye');
                } else {
                    $('#confirm-password').attr('type', 'password');
                    $(this).attr('class', 'fas fa-eye-slash');
                }
            });

            // Sweetalert
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            function response(title, icon) {
                Toast.fire({
                    icon: icon,
                    title: title
                })
            }
        });
    </script>
@endsection
