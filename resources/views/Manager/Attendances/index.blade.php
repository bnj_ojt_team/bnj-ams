@extends('Manager.layout.default')
@section('title', 'Manager.Attendances')
@section('content')
    <link rel="stylesheet" href="{{url('public/timepicker/css/timepicker.css')}}">
    <link rel="stylesheet" href="{{url('public/timepicker/css/timepicker.min.css')}}">
    <script src="{{url('public/timepicker/js/timepicker.js')}}"></script>
    <script src="{{url('public/timepicker/js/timepicker.min.js')}}"></script>
	{{-- Months dropdown --}}
	@php
		$months = [
		    'January' => 'January',
		    'February' => 'February',
		    'March' => 'March',
		    'April' => 'April',
		    'May' => 'May',
		    'June' => 'June',
		    'July' => 'July',
		    'August' => 'August',
		    'September' => 'September',
		    'October' => 'October',
		    'November' => 'November',
		    'December' => 'December',
		];

		$years = date('Y');
	@endphp

	{{-- Datatable clickable row's css --}}
	<style type="text/css">
		.ck-editor__editable[role="textbox"] {
			/* editing area */
			min-height: 200px;
		}

		.ck-content .image {
			/* block images */
			max-width: 80%;
			margin: 20px auto;
		}

		.datatable_row,
		.datatable_row_disabled {
			width: 100%;
			min-height: 25px;
		}
	</style>
	{{-- Modal --}}
	<div class="modal fade" id="modal">
		<div class="modal-dialog modal-lg">
			<form action="" method="" id="form">
				@csrf
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="card w-100">
								<div class="card-header bg-secondary">
									<h3 class="card-title"></h3>
								</div>
								<div class="card-body">
									{{-- Content Here --}}
									<div class="form-group">
										<label id="ck-editor-label"></label>
										<textarea name="editor" id="editor"></textarea>
									</div>
									<div class="form-group" id="report_type_group">
										<label>Report Type</label>
										<input type="text" id="report_type" name="report_type" class="form-control" readonly>
									</div>
									{{-- Work From Home Fields --}}
									<div id="work-from-home-group">

									</div>
									{{-- Work From Home Fields End --}}
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer justify-content-between">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" id="save-button"></button>
					</div>
				</div>
			</form>
		</div>
	</div>

	{{-- Datatable --}}
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Attendance Table</h3>
		</div>
		<div class="card-body">
			<table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">

				{{-- Datatable year and month filter --}}
				<div class="col-lg-6 col-md-12 col-sm-12">
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label for="year">Year</label>
                                {{-- <input type="text" title="Select Year" name="year" id="year" class="form-control rounded-0" placeholder="Enter Year"> --}}
                                <div class="form-group">
                                    <div class="input-group date" id="year-picker" data-target-input="nearest">
                                        <input type="text" name="year" class="form-control datetimepicker-input rounded-0"
                                               id="year" data-target="#year-picker" required>
                                        <div class="input-group-append" data-target="#year-picker"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="month">Month</label>
								<select name="months" id="months" class="custom-select form-control-border border-width-2">
									@foreach ($months as $item)
										<option value="{{ $item }}" {{ $month == $item ? 'selected' : '' }}>{{ $item }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
				<thead>
					{{-- 13 rows --}}
					<tr>
						<th>Date</th>
						<th>Day</th>
						<th>
							<img src="{{ asset('dist/img/time_in.png') }}" style="width: 2rem; height:2rem;" class="img"
								data-toggle="tooltip" title="Time In (AM)">
						</th>
						<th>
							<img src="{{ asset('dist/img/time_out.png') }}"style="width: 2rem; height:2rem;" class="img"
								data-toggle="tooltip" title="Time Out (AM)">
						</th>
						<th>
							<img src="{{ asset('dist/img/time_in.png') }}"style="width: 2rem; height:2rem;" class="img"
								data-toggle="tooltip" title="Time In (PM)">
						</th>
						<th>
							<img src="{{ asset('dist/img/time_out.png') }}"style="width: 2rem; height:2rem;" class="img"
								data-toggle="tooltip" title="Time Out (PM)">
						</th>
						<th>
							<img src="{{ asset('dist/img/time_out.png') }}"style="width: 2rem; height:2rem;" class="img"
								data-toggle="tooltip" title="Time Out Excuse (AM)">
						</th>
						<th>
							<img src="{{ asset('dist/img/time_in.png') }}"style="width: 2rem; height:2rem;" class="img"
								data-toggle="tooltip" title="Time In Excuse (AM)">
						</th>
						<th>
							<img src="{{ asset('dist/img/time_out.png') }}"style="width: 2rem; height:2rem;" class="img"
								data-toggle="tooltip" title="Time Out Excuse (PM)">
						</th>
						<th>
							<img src="{{ asset('dist/img/time_in.png') }}"style="width: 2rem; height:2rem;" class="img"
								data-toggle="tooltip" title="Time In Excuse (PM)">
						</th>
						<th>Total Work From Home Hours</th>
						<th>Total Excuses Hour</th>
						<th>Total Work Hours</th>
						<th>Reason</th>
						<th>Accomplishment</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<script>
		$(function() {
			var action = '';
            var wfhacount = 0;

            $(document).on('change', '#is_work_from_home', function(){
                if($(this).is(':checked')){
                    $('.work-from-home-attendance').append(GenerateWFHAInput());
                    $('.work-from-home-attendance').append(appendWorkFromHomeAttendance(wfhacount));
                    $('#work-from-home-group').append(
                        '<button type="button" class="btn btn-danger rounded-0 col-9 float-right" id="minus_wfha_input"><i class="fas fa-minus-circle"></i></button>'
                    );
                    instantiateTimePicker(wfhacount);
                    wfhacount += 1;
                }else{
                    $('#minus_wfha_input').remove();
                    $('.work-from-home-attendance').html('');
                    wfhacount = 0;
                }
            });

            // On click on add new work from home attendance input
            $(document).on('click', '#add_new_wfha_field', function(){
                $('.work-from-home-attendance').append(appendWorkFromHomeAttendance(wfhacount));
                instantiateTimePicker(wfhacount);
                wfhacount += 1;
            });

            // On click on add new work from home attendance input
            $(document).on('click', '#minus_wfha_input', function(){
                if(wfhacount > 1){
                    wfhacount -= 1;
                    $("#wfha-row-"+wfhacount).remove();
                }
            });

			// Datatable instance
			var datatable_instance = $("#datatable").DataTable({
				"responsive": true,
				"lengthChange": true,
				"autoWidth": true,
				"html": true,
				"dom": "frltip",
				"order": [
					[0, 'asc']
				],
				"ordering": false,
				"processing": false,
				"searching": false,
				"ajax": {
					url: base_url + 'manager/attendances/index',
					type: "GET",
					dataType: 'JSON',
					beforeSend: function() {
						$('#datatable > tbody').html(
							'<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
						);
					},
				},
				"columns": [{
						data: 'attendance_date'
					},
					{
						data: 'day'
					},
					{
						data: 'timein_am',
						render: function(data, meta, row) {
							if (data != null) {
								return '<div class="datatable_row" input-name="timein_am" input-type="time" data-date="' +
									row
									.attendance_date + '">' + moment(data, "HH:mm").format(
										"HH:mm") + '</div>';
							} else if (row.is_greater_than_today) {
								return '<div class="datatable_row_disabled" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							} else {
								return '<div class="datatable_row" input-name="timein_am" input-type="time" data-date="' +
									row
									.attendance_date + '">--:-- --</div>';
							}
						}
					},
					{
						data: 'timeout_am',
						render: function(data, meta, row) {
							if (data != null) {
								return '<div class="datatable_row" input-name="timeout_am" input-type="time" data-date="' +
									row
									.attendance_date + '">' + moment(data, "HH:mm").format(
										"HH:mm") + '</div>';
							} else if (row.is_greater_than_today) {
								return '<div class="datatable_row_disabled" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							} else {
								return '<div class="datatable_row" input-name="timeout_am" input-type="time" data-date="' +
									row
									.attendance_date + '">--:-- --</div>';
							}
						}
					},
					{
						data: 'timein_pm',
						render: function(data, meta, row) {
							if (data != null) {
								return '<div class="datatable_row" input-name="timein_pm" input-type="time" data-date="' +
									row
									.attendance_date + '">' + moment(data, "HH:mm").format(
										"HH:mm") + '</div>';
							} else if (row.is_greater_than_today) {
								return '<div class="datatable_row_disabled" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							} else {
								return '<div class="datatable_row" input-name="timein_pm" input-type="time" data-date="' +
									row
									.attendance_date + '">--:-- --</div>';
							}
						}
					},
					{
						data: 'timeout_pm',
						render: function(data, meta, row) {
							if (data != null) {
								return '<div class="datatable_row" input-name="timeout_pm" input-type="time" data-date="' +
									row
									.attendance_date + '">' + moment(data, "HH:mm").format(
										"HH:mm") + '</div>';
							} else if (row.is_greater_than_today) {
								return '<div class="datatable_row_disabled" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							} else {
								return '<div class="datatable_row" input-name="timeout_pm" input-type="time" data-date="' +
									row
									.attendance_date + '">--:-- --</div>';
							}
						}
					},
					{
						data: 'excuse_timeout_am',
						render: function(data, meta, row) {
							if (data != null) {
								return '<div class="datatable_row" input-name="excuse[timeout_am]" input-type="time" data-date="' +
									row.attendance_date + '">' + moment(data, "HH:mm").format(
										"HH:mm") + '</div>';
							} else if (row.is_greater_than_today) {
								return '<div class="datatable_row_disabled" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							} else {
								return '<div class="datatable_row" input-name="excuse[timeout_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							}
						}
					},
					{
						data: 'excuse_timein_am',
						render: function(data, meta, row) {
							if (data != null) {
								return '<div class="datatable_row" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">' + moment(data, "HH:mm").format(
										"HH:mm") + '</div>';
							} else if (row.is_greater_than_today) {
								return '<div class="datatable_row_disabled" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							} else {
								return '<div class="datatable_row" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							}
						}
					},
					{
						data: 'excuse_timeout_pm',
						render: function(data, meta, row) {
							if (data != null) {
								return '<div class="datatable_row" input-name="excuse[timeout_pm]" input-type="time" data-date="' +
									row.attendance_date + '">' + moment(data, "HH:mm").format(
										"HH:mm") + '</div>';
							} else if (row.is_greater_than_today) {
								return '<div class="datatable_row_disabled" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							} else {
								return '<div class="datatable_row" input-name="excuse[timeout_pm]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							}
						}
					},
					{
						data: 'excuse_timein_pm',
						render: function(data, meta, row) {
							if (data != null) {
								return '<div class="datatable_row" input-name="excuse[timein_pm]" input-type="time" data-date="' +
									row.attendance_date + '">' + moment(data, "HH:mm").format(
										"HH:mm") + '</div>';
							} else if (row.is_greater_than_today) {
								return '<div class="datatable_row_disabled" input-name="excuse[timein_am]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							} else {
								return '<div class="datatable_row" input-name="excuse[timein_pm]" input-type="time" data-date="' +
									row.attendance_date + '">--:-- --</div>';
							}
						}
					},
                    {
						data: 'work_from_home_hours',
						render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
					},
					{
						data: 'excuse_total_hours',
						render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
					},
					{
						data: 'total_hours',
						render: function(data, meta, row){
							if(data == null || data == '' || data == 0){
								return '--.--';
							}else{
								return data;
							}
						}
					},
					{
						data: 'reason',
						render: function(data, meta, row) {
							if (row.is_greater_than_today) {
								return generateReasonButton(row.attendance_date,
									'datatable_row_disabled disabled', row.id);
							} else if (data == null) {
								return generateReasonButton(row.attendance_date, '', row.id);
							} else {
								return generateReasonButton(row.attendance_date, '', row.id);
							}
						}
					},
					{
						data: 'accomplishment',
						render: function(data, meta, row) {
							if (row.is_greater_than_today) {
								return '<button class="btn btn-success btn-flat datatable_row_disabled disabled" relation="accomplishment" data-date="' +
									row
									.attendance_date + '">Create Accomplishment</button>';
							} else if (data == null) {
								return '<button class="btn btn-success btn-flat accomplishment" relation="accomplishment" data-date="' +
									row
									.attendance_date + '">Create Accomplishment</button>';
							} else {
								return '<button class="btn btn-success btn-flat accomplishment-view" data-id="' +
									row.id + '" relation="accomplishment" data-date="' +
									row
									.attendance_date + '">View Accomplishment</button>';
							}
						}
					},
				],
				rowCallback: function(row, data, index) {
					// console.log(data);
					if (data.is_holiday == 1) {
						$(row).css({
							'color': '#AC0B1A'
						});
					} else if (data.is_holiday == 2) {
						$(row).css("background-color", "#2f943b");
					}
					if(data.is_greater_than_today){
						$(row).css("color", "#7a7a7a");
					}
				},
                buttons: false,
			});

            // Add new attendance work from home input
            function GenerateWFHAInput(){
                return '<button type="button" id="add_new_wfha_field" class="btn btn-primary btn-block rounded-0">'+
                            '<i class="fas fa-plus-circle"></i>'+
                        '</button>';
            }

            // Work from home events
            function generateWorkFromHomeGroup(){
                return  '<div class="row">'+
                            '<div class="col-3">'+
                                '<div class="custom-control custom-checkbox">'+
                                    '<input type="checkbox" class="custom-control-input" name="is_work_from_home" id="is_work_from_home">'+
                                    '<label for="is_work_from_home" class="custom-control-label">Work From Home</label>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-9 work-from-home-attendance">'+

                            '</div>'+
                        '</div>';
            }

            function instantiateTimePicker(count = null){
                $('#time-in-'+count).datetimepicker({
                    use24hours: true,
                    format: 'HH:mm',
                    pickDate: false,
                });
                // $('#time-out-'+count).datetimepicker({
                //     format: 'DD-MM-YYYY hh:mm',
                //     pickDate: true,
                //     sideBySide: true, 
                //     debug: true
                // });
            }

            function appendWorkFromHomeAttendance(count = null){
                return '<div class="row" id="wfha-row-'+count+'">'+
                            '<div class="col-6">'+
                                '<label for="time-in-'+count+'">Time In</label>'+
                                '<div class="form-group">'+
                                    '<div class="input-group date" id="time-in-'+count+'" data-target-input="nearest">'+
                                        '<input type="time" class="form-control datetimepicker-input rounded-0 wfha" id="time-in-input-'+count+'" data-target="#time-in-'+count+'" required data-input="timein">'+
                                        '<div class="input-group-append" data-target="#time-in-'+count+'" data-toggle="datetimepicker">'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-6">'+
                                '<label for="time-out-'+count+'">Time Out</label>'+
                                '<div class="form-group">'+
                                    '<div class="input-group date" id="time-out-'+count+'" data-target-input="nearest">'+
                                        '<input type="datetime-local" class="form-control rounded-0 wfha" id="time-out-input-'+count+'" data-target="#time-out-'+count+'" required data-input="timeout">'+
                                        '<div class="input-group-append" data-target="#tim-out-'+count+'" data-toggle="datetimepicker">'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
            }

			// On double click on datatable_row_disabled
			$('#datatable').on('dblclick', '.datatable_row_disabled', function() {
				response('You cant input on future dates row. ', 'info');
			});

			// Inline input
			var __oldValue, __newValue, __currentClickedField, __rowId, __inputName, __inputType, __data, __relation,
				__dataDate;
			$('#datatable').on('dblclick', '.datatable_row', function() {
				__data = new Object();
				__rowId = $(this).data('id');
				__inputName = $(this).attr('input-name');
				__inputType = $(this).attr('input-type');
				__dataDate = $(this).attr('data-date');
				if (__inputType === 'time') {
					const timeValue = moment($(this).html(), "HH:mm:ss").format("HH:mm:ss");
					if (timeIsValid(timeValue)) {
						__oldValue = timeValue;
					} else {
						__oldValue = '';
					}
				} else {
					__oldValue = $(this).html();
				}
				$(this).html('<input type="' + __inputType +
					'" class="form-control rounded-0 datatable_row_input">');
				$('.datatable_row_input').val(__oldValue).focus();
				__currentClickedField = $(this);
				__data['id'] = __rowId;
				__data['attendance_date'] = __dataDate;
			});

			// On row out focus
			$('#datatable').on('blur', '.datatable_row_input', function(__event) {
				__newValue = $('.datatable_row_input').val();
				if (timeIsValid(__newValue) == true) {
					__currentClickedField.html(moment(__newValue, "HH:mm").format("HH:mm"));
				} else {
					__currentClickedField.html(__newValue);
				}
				if (__oldValue != __newValue) {
					__data[__inputName] = __newValue;

					if ((__inputName == 'timeout_am' || __inputName == 'timeout_pm')) {
						if (__inputName == 'timeout_am') {
							__data['timein_am'] = $('div[input-name="timein_am"][data-date="' + __dataDate +
								'"]').html();
						} else {
							__data['timein_pm'] = $('div[input-name="timein_pm"][data-date="' + __dataDate +
								'"]').html();
						}
					}

					if ((__inputName == 'excuse[timein_am]' || __inputName == 'excuse[timein_pm]')) {
						if (__inputName == 'excuse[timein_am]') {
							__data['excuse[timeout_am]'] = $(
									'div[input-name="excuse[timeout_am]"][data-date="' + __dataDate + '"]')
								.html();
						} else {
							__data['excuse[timeout_pm]'] = $(
									'div[input-name="excuse[timeout_pm]"][data-date="' + __dataDate + '"]')
								.html();
						}
					} else if ((__inputName == 'excuse[timeout_am]' || __inputName == 'excuse[timeout_pm]')) {
						if (__inputName == 'excuse[timeout_am]') {
							__data['timein_am'] = $('div[input-name="timein_am"][data-date="' + __dataDate +
								'"]').html();
						} else {
							__data['timein_pm'] = $('div[input-name="timein_pm"][data-date="' + __dataDate +
								'"]').html();
						}
					}
					__onInput(__data);

				}else if(__oldValue == ''){
					__currentClickedField.html('--:-- --');
				}
			});
			$(document).on('keyup', function(__e) {
				if (__e.type == 'keyup' && __e.keyCode === 13) {
					$('.datatable_row_input').blur();
				}
			});

			// On datatable row out focus save data into database
			function __onInput(data) {

				$.ajax({
					url: base_url + 'manager/attendances/set-attendance',
					method: 'POST',
					data: data,
					dataType: "JSON",
					success: function(responseData, textStatus, jqXHR) {
						__rowId = null;
						__data = '';
						datatable_instance.ajax.reload(null, false);
						response(responseData.title, responseData.icon);
					},
					error: function(xhr, status, error) {
						__rowId = null;
						__data = '';
						__currentClickedField.html('');
						var title = '';
						var validation = JSON.parse(xhr.responseText);
						title = validation.errors ? validation.errors[Object.keys(validation
							.errors)[0]] : validation.title;
						datatable_instance.ajax.reload(null, false);
						response(title, 'error');
					}
				});
			}

			// On click view reason or accomplishment
			$('#datatable').on('click', '.reason', function(e) {
				e.preventDefault();
				$('#work-from-home-group').html('');
				action = 'reason';
				__rowId = $(this).data('id');
				__dataDate = $(this).attr('data-date');
				__relation = $(this).attr('relation');
				thisInputReportType = $(this).attr('data-type');
				$('#report_type_group').removeAttr('style');
				$('#report_type').attr('requried', 'required');
				$.ajax({
					url: base_url + 'manager/attendances/get-reason-data/' + __rowId + '/' +
						thisInputReportType,
					method: 'GET',
					dataType: 'JSON',
					success: function(data, textStatus, jqXHR) {
						if (data.report_type == null) {
							$('#report_type').val(thisInputReportType);
							editor.setData(data.content);
						} else {
							$('#report_type').val(data.report_type);
							editor.setData(data.content);
						}
						$('#save-button').html('Update Excuse Report');
						$('#modal-title').html('Excuse Report');
						$('.card-title').html('Excuse Report');
						$('#ck-editor-label').html('Excuse');
						$('#modal').modal({
							backdrop: 'static',
							keyboard: false
						}, 'show');
					},
					error: function(xhr, status, error) {
						datatable_instance.ajax.reload();
						response(error, 'error');
					}
				});
			});
			$('#datatable').on('click', '.accomplishment-view', function() {
				action = 'accomplishment';
                $('#work-from-home-group').html(generateWorkFromHomeGroup());
				__rowId = $(this).data('id');
				__dataDate = $(this).attr('data-date');
				__relation = $(this).attr('relation');
				$('#report_type_group').attr('style', 'display: none;');
				$('#report_type').removeAttr('required');
				$.ajax({
					url: base_url + 'manager/attendances/get-accomplishment-data/' + __rowId,
					method: 'GET',
					dataType: 'JSON',
					success: function(data, textStatus, jqXHR) {
						// console.log(data);
						editor.setData(data.content);
                        if(data.attendance.is_work_from_home == 1   ){
                            $('#is_work_from_home').prop('checked', true);
                            if(data.home_attendances != null){
                                $('.work-from-home-attendance').append(GenerateWFHAInput());
                                $('#work-from-home-group').append(
                                    '<button type="button" class="btn btn-danger rounded-0 col-9 float-right" id="minus_wfha_input"><i class="fas fa-minus-circle"></i></button>'
                                );
                                instantiateTimePicker(wfhacount);
                                $.map(data.home_attendances, function(value, index){
                                    $('.work-from-home-attendance').append(appendWorkFromHomeAttendance(wfhacount));
                                    instantiateTimePicker(wfhacount);
                                    $('#time-in-input-'+wfhacount).val(value.time_in);
                                    $('#time-out-input-'+wfhacount).val(value.time_out);
                                    wfhacount += 1;
                                });
                            }
                        }
						$('#save-button').html('Update Accomplishment Report');
						$('#modal-title').html('Accomplishment Report');
						$('.card-title').html('Accomplishment Report');
						$('#ck-editor-label').html('Accomplishment');
						$('#modal').modal({
							backdrop: 'static',
							keyboard: false
						}, 'show');
					},
					error: function(xhr, status, error) {
						datatable_instance.ajax.reload();
						response(error, 'error');
					}
				});
			});

			// Datatable on click accomplishment
			$('#datatable').on('click', '.accomplishment', function() {
				action = 'accomplishment';
				$('#work-from-home-group').html(generateWorkFromHomeGroup());
				__rowId = $(this).data('id');
				__relation = $(this).attr('relation');
				__dataDate = $(this).attr('data-date');
				$('#report_type_group').attr('style', 'display: none;');
				$('#report_type').removeAttr('required');
				$('#save-button').html('Save Accomplishment Report');
				$('#modal-title').html('Accomplishment Report');
				$('.card-title').html('Accomplishment Report');
				$('#ck-editor-label').html('Accomplishment');
				$('#modal').modal({
					backdrop: 'static',
					keyboard: false
				}, 'show');
			});

			// Form on submit
			$('#form').on('submit', function(__e) {
				__e.preventDefault();
				__data = new Object();
				__data['id'] = __rowId;
				__data['attendance_date'] = __dataDate;

				var CKEditorData = editor.getData();
				__data[__relation] = {};
				if (action == 'reason') {
					__data[__relation]['report_type'] = $('#report_type').val();
				}else if(action == 'accomplishment'){
                    var count = 0;
                    __data['is_work_from_home'] = $('#is_work_from_home').is(":checked");
                    __data[__relation]['home_attendances'] = {};
                    $('.wfha').each(function(){
                        if($(this).attr('data-input') == 'timein'){
                            __data[__relation]['home_attendances'][count] = {};
                            __data[__relation]['home_attendances'][count]['time_in'] = $(this).val();
                        }else if($(this).attr('data-input') == 'timeout'){
                            __data[__relation]['home_attendances'][count]['time_out'] = $(this).val();
                            count += 1;
                        }
                    });

                }
				__data[__relation]['content'] = CKEditorData;
				$('#modal').modal('hide');
				__onInput(__data);
			});

			// Hidden modal
			$('#modal').on('hidden.bs.modal', function() {
				action = '';
				$('#save-button').html('');
				$('#modal-title').html('');
				$('#form')[0].reset();
				__rowId = null;
				__data = '';
                wfhacount = 0;
				editor.setData('<p>Some text.</p>');
			});

			// Check if valid date
			function timeIsValid(time) {
				var REGEX12 = new RegExp(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])[\s]([ap][m])$/);
				var REGEX24 = new RegExp(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]:[0-9][0-9])$/);
				if (time.match(REGEX12) || time.match(REGEX24)) {
					return true;
				} else {
					return false;
				}
			}

			// Datatable tooltip
			$('#datatable').on('mouseover', function() {
				$('[data-toggle="tooltip"]').tooltip({
					trigger: 'hover',
					html: true
				});
			});

			// year and months dropdown on change
            $("#year-picker").datetimepicker({
                use24hours: true,
                format: 'YYYY',
                pickDate: true,
            });
			$(document).on('change blur', '#year, #months', function() {
                var yearValue = ($('#year').val() == '') ? new Date().getFullYear():$('#year').val();
				datatable_instance.ajax.url(base_url + 'manager/attendances/index/' + yearValue +
					'/' + $('#months').val()).load();
			});
            $('#year').val(new Date().getFullYear());

			// Sweetalert
			var Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 7000
			});

			function response(title, icon) {
				Toast.fire({
					icon: icon,
					title: title
				})
			}

			var editor;

			// This sample still does not showcase all CKEditor 5 features (!)
			// Visit https://ckeditor.com/docs/ckeditor5/latest/features/index.html to browse all the features.
			CKEDITOR.ClassicEditor.create($('#editor')[0], {
				// https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
				toolbar: {
					items: [
						'exportPDF', 'exportWord', '|',
						'findAndReplace', 'selectAll', '|',
						'heading', '|',
						'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript',
						'removeFormat', '|',
						'bulletedList', 'numberedList', 'todoList', '|',
						'outdent', 'indent', '|',
						'undo', 'redo',
						'-',
						'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
						'alignment', '|',
						'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock',
						'htmlEmbed', '|',
						'specialCharacters', 'horizontalLine', 'pageBreak', '|',
						'textPartLanguage', '|',
						'sourceEditing'
					],
					shouldNotGroupWhenFull: true
				},
				// Changing the language of the interface requires loading the language file using the <script> tag.
				// language: 'es',
				list: {
					properties: {
						styles: true,
						startIndex: true,
						reversed: true
					}
				},
				// https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
				heading: {
					options: [{
							model: 'paragraph',
							title: 'Paragraph',
							class: 'ck-heading_paragraph'
						},
						{
							model: 'heading1',
							view: 'h1',
							title: 'Heading 1',
							class: 'ck-heading_heading1'
						},
						{
							model: 'heading2',
							view: 'h2',
							title: 'Heading 2',
							class: 'ck-heading_heading2'
						},
						{
							model: 'heading3',
							view: 'h3',
							title: 'Heading 3',
							class: 'ck-heading_heading3'
						},
						{
							model: 'heading4',
							view: 'h4',
							title: 'Heading 4',
							class: 'ck-heading_heading4'
						},
						{
							model: 'heading5',
							view: 'h5',
							title: 'Heading 5',
							class: 'ck-heading_heading5'
						},
						{
							model: 'heading6',
							view: 'h6',
							title: 'Heading 6',
							class: 'ck-heading_heading6'
						}
					]
				},
				// https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
				placeholder: 'Welcome to CKEditor 5!',
				// https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
				fontFamily: {
					options: [
						'default',
						'Arial, Helvetica, sans-serif',
						'Courier New, Courier, monospace',
						'Georgia, serif',
						'Lucida Sans Unicode, Lucida Grande, sans-serif',
						'Tahoma, Geneva, sans-serif',
						'Times New Roman, Times, serif',
						'Trebuchet MS, Helvetica, sans-serif',
						'Verdana, Geneva, sans-serif'
					],
					supportAllValues: true
				},
				// https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
				fontSize: {
					options: [10, 12, 14, 'default', 18, 20, 22],
					supportAllValues: true
				},
				// Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
				// https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
				htmlSupport: {
					allow: [{
						name: /.*/,
						attributes: true,
						classes: true,
						styles: true
					}]
				},
				// Be careful with enabling previews
				// https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
				htmlEmbed: {
					showPreviews: true
				},
				// https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
				link: {
					decorators: {
						addTargetToExternalLinks: true,
						defaultProtocol: 'https://',
						toggleDownloadable: {
							mode: 'manual',
							label: 'Downloadable',
							attributes: {
								download: 'file'
							}
						}
					}
				},
				// https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
				mention: {
					feeds: [{
						marker: '@',
						feed: [
							'@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes',
							'@chocolate', '@cookie', '@cotton', '@cream',
							'@cupcake', '@danish', '@donut', '@dragée', '@fruitcake',
							'@gingerbread', '@gummi', '@ice', '@jelly-o',
							'@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum',
							'@pudding', '@sesame', '@snaps', '@soufflé',
							'@sugar', '@sweet', '@topping', '@wafer'
						],
						minimumCharacters: 1
					}]
				},
				// The "super-build" contains more premium features that require additional configuration, disable them below.
				// Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
				removePlugins: [
					// These two are commercial, but you can try them out without registering to a trial.
					// 'ExportPdf',
					// 'ExportWord',
					'CKBox',
					'CKFinder',
					'EasyImage',
					// This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
					// https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
					// Storing images as Base64 is usually a very bad idea.
					// Replace it on production website with other solutions:
					// https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
					// 'Base64UploadAdapter',
					'RealTimeCollaborativeComments',
					'RealTimeCollaborativeTrackChanges',
					'RealTimeCollaborativeRevisionHistory',
					'PresenceList',
					'Comments',
					'TrackChanges',
					'TrackChangesData',
					'RevisionHistory',
					'Pagination',
					'WProofreader',
					// Careful, with the Mathtype plugin CKEditor will not load when loading this sample
					// from a local file system (file://) - load this site via HTTP server if you enable MathType
					'MathType'
				]
			}).then(function(data) {
				editor = data;
			}).catch(function(error) {
				console.error(error);
			});

			setTimeout(function() {
				editor.setData('<p>Some text.</p>');
			}, 3000);

			// Create reason buttons
			function generateReasonButton(reasonDate, isDisabled, id) {
				return '<div class="btn-group">' +
					'<button type="button" class="btn btn-success btn-flat reason ' + isDisabled +
					'" data-type="Absent" relation="reason" data-date="' + reasonDate + '" data-id="' + id +
					'">Absent</button>' +
					'<div class="btn-group">' +
					'<button type="button" class="btn btn-success dropdown-toggle dropdown-icon btn-flat ' +
					isDisabled + '" data-toggle="dropdown">' +
					'Late' +
					'</button>' +
					'<div class="dropdown-menu">' +
					'<a class="dropdown-item reason" href="#" data-type="Late AM" relation="reason" data-date="' +
					reasonDate + '" data-id="' + id + '">Late AM</a>' +
					'<a class="dropdown-item reason" href="#" data-type="Late PM" relation="reason" data-date="' +
					reasonDate + '" data-id="' + id + '">Late PM</a>' +
					'</div>' +
					'</div>' +
					'<div class="btn-group">' +
					'<button type="button" class="btn btn-success dropdown-toggle dropdown-icon btn-flat ' +
					isDisabled + '" data-toggle="dropdown">' +
					'Excuse' +
					'</button>' +
					'<div class="dropdown-menu">' +
					'<a class="dropdown-item reason" href="#" data-type="Excuse AM" relation="reason" data-date="' +
					reasonDate + '" data-id="' + id + '">Excuse AM</a>' +
					'<a class="dropdown-item reason" href="#" data-type="Excuse AM" relation="reason" data-date="' +
					reasonDate + '" data-id="' + id + '">Excuse PM</a>' +
					'</div>' +
					'</div>' +
					'</div>';
			}

		});
	</script>

@endsection
