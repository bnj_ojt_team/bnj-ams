@extends('Manager.layout.default')
@section('title', 'Manager.Dashboard')
@section('content')
<style>

.calendar-events {
        padding: 70px 20px 60px 20px;
        -webkit-box-shadow: -5px 0 18px -3px rgba(135, 115, 193, 0.5);
                box-shadow: -5px 0 18px -3px rgba(135, 115, 193, 0.5);
        z-index: 1;
    }
    .event-hide .calendar-events {
        -webkit-transform: translateX(100%);
            -ms-transform: translateX(100%);
                transform: translateX(100%);
        -webkit-box-shadow: none;
        box-shadow: none;
    }
    
 </style>

    <div class="row">

        <!-- ./col -->
        <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{$totalemployees}}</h3>

                    <p>Total Employees</p>
                </div>
                <div class="icon">
                    <i class="fas fa-users"></i>
                </div>
                <a href="{{route('admin.employees.index')}}" class="small-box-footer"><i class="fa fa-hand-point-right"></i> Show All</a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{ $totalPresent->count() }}</h3>
                    <p>Present Today</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-tie"></i>
                </div>
                <a href="#" class="small-box-footer"><i class="fa fa-hand-point-right"></i> Show All</a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-12">
            <!-- small box -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{ $totalemployees - $totalPresent->count() }}</h3>

                    <p>Absent Today</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-slash"></i>
                </div>
                <a href="#" class="small-box-footer"><i class="fa fa-hand-point-right"></i> Show All</a>
            </div>
        </div>

    </div>

<div class="col-md-12 col-lg-12">
    <div class="card card-secondary card-outline">
        <div class="card-header">
            <h5 class="m-0">Quote For Today...</h5>
        </div>
        <div class="card-body">

            <p class="card-text" style="font-size: 1.2em;font-weight: bolder;">
                — {{ $quote }}
            </p>
            {{--<a href="#" class="btn btn-primary">Go somewhere</a>--}}
        </div>
    </div>
</div>

    {{-- Accordion --}}
    <div class="col-md-12 col-lg-12">
        <div class="card card-info shadow-none" >
            <div class="card-header">
                <h3 class="card-title">Calendar view</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-calendar-day"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                {{-- Evo Calendar --}}
                <div class="hero">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>

        

    {{-- Quotes Script --}}
    <script>
       
          var color = $('span.input-group-text > i.fa-square').css('color');
      // Evo Calendar
      var calendar = $('#calendar').evoCalendar({
                settingName: "Calendar",
                theme: 'Royal Navy',
                language: 'en',
                todayHighlight: true,
                sidebarDisplayDefault: true,
                sidebarToggler: true,
                eventDisplayDefault: true,
                eventListToggler: true
            });

            // Set Evo Calendar again after Destroy
            calendar.on('destroy', function(e) {
                calendar = $('#calendar').evoCalendar({
                    settingName: "Calendar",
                    theme: 'Royal Navy',
                    language: 'en',
                    todayHighlight: true,
                    sidebarDisplayDefault: true,
                    sidebarToggler: true,
                    eventDisplayDefault: true,
                    eventListToggler: true
                });
                setCalendar();
            });
      // Get Holidays
      function getHolidays() {
                var events = [];
                $.ajax({
                    url: base_url + 'manager/dashboard/get-all-holidays',
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    global: false,
                    async: false,
                    success: function(data, textStatus, jqXHR) {
                        $.map(data.data, function(data) {
                            events.push({
                                id: data.id,
                                name: data.holiday_name,
                                date: data.holiday_date,
                                type: data.holiday_type,
                                everyYear: data.is_per_year,
                                color: data.color
                            });
                        });
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.url(base_url +
                                'manager/dashboard/get-all-holidays')
                            .load();
                        response(error, 'error');
                    }
                });
                return events;
            }
             // Get Birthdays
             function getBirthdays() {
                var events = [];
                $.ajax({
                    url: base_url + 'manager/employees/get-birthdays',
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    global: false,
                    async: false,
                    success: function(data, textStatus, jqXHR) {
                        $.map(data, function(data) {
                            events.push({
                                id: data.id,
                                name: 'Birthday',
                                date: data.birthdate,
                                description: 'Birthday of '+data.firstname+' '+data.lastname,
                                badge:'Birthday',
                                type: 'holiday',
                                everyYear: true,
                                color: '#8DCBE6'
                            });
                        });
                    },
                    error: function(xhr, status, error) {
                        response(error, 'error');
                    }
                });
                return events;
            }     
            // Set Calendar
            function setCalendar() {
                $('#calendar').evoCalendar('addCalendarEvent', getHolidays()).evoCalendar('addCalendarEvent', getBirthdays());
            }
            setCalendar();
        

    </script>
@endsection
