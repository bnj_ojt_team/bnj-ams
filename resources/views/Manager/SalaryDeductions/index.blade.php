@extends('Manager.layout.default')
@section('title', 'Manager.Salary Deductions')
@section('content')

	{{-- Modal --}}
	<div class="modal fade" id="modal">
		<div class="modal-dialog modal-md">
			<form action="" method="" id="form">
				@csrf
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="modal-title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							{{-- Content Here --}}
							{{-- Deduction --}}
							<label for="deduction">Deduction Amount</label>
							<div class="col-lg-12">
								<div class="input-group">
									<input type="number" pattern="[0-9]" name="deduction" class="form-control rounded-0" id="deduction"
										placeholder="Deduction Amount">
								</div>
							</div>

						</div>
					</div>
					<div class="modal-footer justify-content-between">
						<button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary rounded-0" id="save-button"></button>
					</div>
				</div>
			</form>
		</div>
	</div>

	{{-- Datatable --}}
	<div class="card card-primary card-outline">
		<div class="card-header">
			<button class="btn btn-primary col-2 float-right rounded-0" id="add"> New Salary Deduction </button>
		</div>
		<div class="card-body">
			<table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
				<thead>
					<tr>
						<th>Deduction Amount</th>
						<th>Date</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<script>
		$(function() {
			const formatter = new Intl.NumberFormat('en-US', {
				style: 'currency',
				currency: 'PHP',
			});

            $('input[type="number"]').on('keypress',function (e) {
                if(!e.key.match(/^([0-9])$/g)){
                    e.preventDefault();
                }
            });

			var url = '';
			// Datatable
			var datatable_instance = $("#datatable").DataTable({
				"order": [
					[0, 'Asc']
				],
				"ordering": false,
				"responsive": true,
				"language": {
					"emptyTable": "No Salary Deductions Data found"
				},
				"ajax": {
					url: base_url + 'manager/salary-deductions/index',
					method: 'GET',
					dataType: 'JSON',
					beforeSend: function() {
						$('#datatable > tbody').html(
							'<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
						);
					},
				},
				"columns": [{
					data: 'deduction',
					className: 'text-right',
					render: function(data, meta, row) {
						return formatter.format(data);
					}
				}, {
					data: 'date',
					render: function(data, meta, row) {
						return data;
					}
				}, {
					data: 'id',
					render: function(data, meta, row){
						return `<div class="btn-group">
                            <button type="button" class="btn btn-primary btn-flat">Action</button>
                            <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                                <a data-id="${data}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                                <a data-id="${data}" title="Delete" style="cursor: pointer;" class="dropdown-item delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                        </div>`;
                    }
				}]
			});

			$('#add').on('click', function(e) {
				e.preventDefault();
				url = base_url + 'manager/salary-deductions/add';
				$('#save-button').html('Request');
				$('#modal-title').html('Request A Salary Deduction');
				$('#modal').modal('show');
			});

			// Form submit
			$(document).on('submit', '#form', function(e) {
				e.preventDefault();
				var data = new FormData(this);
				if (url === base_url + 'manager/salary-deductions/add') {
					$('#save-button').html('Requesting <i class="fas fa-spinner"></i>');
				} else {
					$('#save-button').html('Updating Request <i class="fas fa-spinner"></i>');
				}
				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					type: 'json',
					contentType: false,
					processData: false,
					cache: false,
					success: function(data, textStatus, jqXHR) {
						$('#modal').modal('hide');
						response(data.title, data.icon);
						datatable_instance.ajax.url(base_url + 'manager/salary-deductions/index')
							.load();
					},
					error: function(xhr, status, error) {
						if (url === base_url + 'manager/salary-deductions/add') {
							$('#save-button').html('Save');
						} else {
							$('#save-button').html('Update Changes');
						}
						var title = '';
						var validation = JSON.parse(xhr.responseText);
						title = validation.errors ? validation.errors[Object.keys(validation
							.errors)[0]] : validation.title;
						response(title, 'error');
						datatable_instance.ajax.url(base_url + 'manager/salary-deductions/index')
							.load();
					}
				});
			});

			// Click Edit
			datatable_instance.on('click', '.edit', function(e) {
				e.preventDefault();
				var dataId = $(this).attr('data-id');
				url = base_url + 'manager/salary-deductions/edit/' + dataId;
				$.ajax({
					url: url,
					method: 'GET',
					dataType: 'JSON',
					success: function(data, textStatus, jqXHR) {
						$('#deduction').val(data.deduction);
						$('#save-button').html('Update Changes');
						$('#modal-title').html('Update Deduction Request');
						$('#modal').modal('show');
					},
					error: function(xhr, status, error) {
						datatable_instance.ajax.url(base_url + 'manager/salary-deductions/index')
							.load();
						response(error, 'error');
					}
				});
			});


			// Delete Function
			datatable_instance.on('click', '.delete', function(e) {
				e.preventDefault();
				var dataId = $(this).attr('data-id');
				var href = base_url + 'manager/salary-deductions/delete/' + dataId;
				Swal.fire({
					title: 'Delete Salary Deduction?',
					text: 'Are You Sure you want to delete this Salary Deduction?',
					icon: 'warning',
					animation: true,
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes',
					closeOnConfirm: false,
					closeOnCancel: false,
				}).then(function(result) {
					if (result.isConfirmed) {
						let timerInterval;
						Swal.fire({
							title: 'Deleting...',
							timer: 10000,
							animation: false,
							timerProgressBar: false,
							didOpen: () => {
								Swal.showLoading();
								const b = Swal.getHtmlContainer().querySelector('b');
								timerInterval = setInterval(() => {
									b.textContent = Swal.getTimerLeft()
								}, 100);

								$.ajax({
									url: href,
									type: 'DELETE',
									method: 'DELETE',
									dataType: 'JSON',
									success: function(data, textStatus, jqXHR) {
										Swal.close();
										datatable_instance.ajax.url(base_url +
												'manager/salary-deductions/index'
												)
											.load();
										response(data.title, data.icon);
									},
									error: function(xhr, status, error) {
										datatable_instance.ajax.url(base_url +
												'manager/salary-deductions/index'
												)
											.load();
										response(error, 'error');
									}
								});
							},
							willClose: () => {
								clearInterval(timerInterval);
							}
						});
					}
				});
			});

			// Hidden modal
			$('#modal').on('hidden.bs.modal', function() {
				url = '';
				$('#save-button').html('');
				$('#modal-title').html('');
				$('#form')[0].reset();
			});

			// Sweetalert
			var Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 4000
			});

			function response(title, icon) {
				Toast.fire({
					icon: icon,
					title: title
				})
			}
		});
	</script>
@endsection
