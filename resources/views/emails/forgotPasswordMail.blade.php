<x-mail::message>
Good Day,
<x-mail::panel color="success">
Please click the button below to proceed to reseting password.
</x-mail::panel>
<x-mail::button :url="$url" color="primary">
Password Reset
</x-mail::button>
<x-mail::panel color="error">
If you have trouble clicking the button above, you may copy and paste the link below:
{{ $url }}
</x-mail::panel>
Brain Network Japan, Brain IT Consultancy<br>
{{ config('app.name') }}
</x-mail::message>
