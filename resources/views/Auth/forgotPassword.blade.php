@extends('Auth.layout.default')
@section('title', 'Forgot Password')
@section('content')
    <style>
        .fa-spinner {
            animation: rotation 2s infinite linear;
        }
        @keyframes rotation {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(359deg);
            }
        }
    </style>
    <div class="login-box">
        <div class="login-logo">
            <a href="../../index2.html"><b>BNJ</b>-AMS</a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>
                <form action="{{ route('auth.user.forgot-password') }}" method="post">
                    @csrf
                    @if(session('status'))
                        <center>
                            <label class="text-center text-danger">  {{ session('status') }} </label>
                        </center>
                    @endif
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control rounded-0" placeholder="Email" value="{{ old('email') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block rounded-0" id="forgot-password-button">Request new password</button>
                        </div>
                    </div>
                </form>

                <p class="mt-3 mb-1">
                    <a href="{{ route('auth.user.index') }}">Login</a>
                </p>
                {{-- <p class="mb-0">
                    <a href="{{ route('auth.user.register') }}" class="text-center">Register a new membership</a>
                </p> --}}
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $('#forgot-password-button').on('click', function(){
                $(this).html('Requesting <i class="fas fa-spinner"></i>');
            });
        });
    </script>
@endsection
