<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{url('public/logo.png')}}" type="image/x-icon" style="border-radius: 50%;">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    {{-- Custom CSS --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans&family=Poppins:ital,wght@0,300;0,500;1,100&family=Roboto+Mono:wght@300&family=Roboto:wght@900&display=swap" rel="stylesheet">
    <style>
        *{
            font-family: 'DM Sans', sans-serif;
        }
        .fa-spinner {
            animation: rotation 2s infinite linear;
        }
        @keyframes rotation {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(359deg);
            }
        }
    </style>
</head>
<body class="hold-transition login-page">
    @yield('content')
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    {{-- Custom Script --}}
    <script>
        $('#password-eye').on('click', function(){
            if ($(this).attr('class') == 'fas fa-eye-slash') {
                $('#password').attr('type', 'text');
                $(this).attr('class', 'fas fa-eye');
            }else{
                $('#password').attr('type', 'password');
                $(this).attr('class', 'fas fa-eye-slash');
            }
        });

        $('#confirm-password-eye').on('click', function(){
            if ($(this).attr('class') == 'fas fa-eye-slash') {
                $('#confirm-password').attr('type', 'text');
                $(this).attr('class', 'fas fa-eye');
            }else{
                $('#confirm-password').attr('type', 'password');
                $(this).attr('class', 'fas fa-eye-slash');
            }
        });
    </script>
</body>
</html>
