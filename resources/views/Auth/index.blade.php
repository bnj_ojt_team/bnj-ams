@extends('Auth.layout.default')
@section('title', 'Authenticate')
@section('content')
    <div class="login-box">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="../../index2.html" class="h1"><b>BNJ</b>-AMS</a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Sign in to start your session</p>
                @if(session('status'))
                    <center>
                        <label class="text-center text-danger">  {{ session('status') }} </label>
                    </center>
                @endif
                <form action="{{ route('auth.user.authenticate') }}" method="post" id="auth-form">
                    @csrf
                    <div class="input-group mb-3">
                        <input type="text" name="login" class="form-control rounded-0 @error('login') is-invalid @enderror" placeholder="Email" value="{{ old('login') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @error('login')
                            <div class="container text-danger">{{ "*".$message }}</div>
                        @enderror
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" id="password" class="form-control rounded-0" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-eye-slash" id="password-eye"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" name="remember" id="remember">
                                <label for="remember">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-primary btn-block rounded-0" id="sign-in">Sign In</button>
                        </div>
                    </div>
                </form>
                <p class="mb-1">
                    <a href="{{ route('auth.user.forgot-password') }}">I forgot my password</a>
                </p>
                {{-- <p class="mb-0">
                    <a href="{{ route('auth.user.register') }}" class="text-center">Register a new membership</a>
                </p> --}}
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $('#sign-in').on('click', function(){
                $(this).html('Authenticating <i class="fas fa-spinner"></i>');
            });
        });
    </script>
@endsection
