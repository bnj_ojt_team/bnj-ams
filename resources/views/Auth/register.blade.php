@extends('Auth.layout.default')
@section('title', 'Register')
@section('content')
    <style>
        .fa-spinner {
            animation: rotation 2s infinite linear;
        }
        @keyframes rotation {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(359deg);
            }
        }
    </style>
    <div class="register-box">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="../../index2.html" class="h1"><b>BNJ</b>-AMS</a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Register a new membership</p>
                <form action="{{ route('auth.user.register') }}" method="post" id="auth-form" enctype="multipart/form-data">
                    @csrf
                    <div class="input-group mb-3 d-flex justify-content-center">
                        <center>
                            <img src="{{ asset('preview.png') }}" alt="" id="file-preview" @error('username') style="height: 4rem; width: 4rem; border-radius: 50%; border: 2px solid rgb(255, 0, 0) !important;" @enderror style="height: 4rem; width: 4rem; border-radius: 50%; border: 1px solid rgb(184, 184, 184);"><br>
                            <label for="">Avatar</label>
                            @error('file')
                                <div class="container text-danger">{{ "*".$message }}</div>
                            @enderror
                        </center>
                        <input name="file" type="file" class="form-control rounded-0" id="file" placeholder="Full name" accept="image/*" style="display: none;">
                    </div>
                    <div class="input-group mb-3">
                        <input name="username" type="text" class="form-control rounded-0 @error('username') is-invalid @enderror" placeholder="Username" value="{{ old('username') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        @error('username')
                            <div class="container text-danger">{{ "*".$message }}</div>
                        @enderror
                    </div>

                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control rounded-0 @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @error('email')
                            <div class="container text-danger">{{ "*".$message }}</div>
                        @enderror
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" id="password" class="form-control rounded-0 @error('password') is-invalid @enderror" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-eye-slash" id="password-eye"></span>
                            </div>
                        </div>
                        @error('password')
                            <div class="container text-danger">{{ "*".$message }}</div>
                        @enderror
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="confirm-password" id="confirm-password" class="form-control rounded-0 @error('confirm-password') is-invalid @enderror" placeholder="Retype password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-eye-slash" id="confirm-password-eye"></span>
                            </div>
                        </div>
                        @error('confirm-password')
                            <div class="container text-danger">{{ "*".$message }}</div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-6">
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-primary btn-block" id="sign-up">Register</button>
                        </div>
                    </div>
                </form>
                <a href="{{ route('auth.user.index') }}" class="text-center">I already have a membership</a>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $('#sign-up').on('click', function(){
                $(this).html('Registering <i class="fas fa-spinner"></i>');
            });

            $('#file').on('change',function (event) {
                var image = URL.createObjectURL(event.target.files[0]);
                $('#file-preview').attr('src',image);
            });

            $('#file-preview').on('click', function(){
                $('#file').click();
            });
        });

    </script>
@endsection
