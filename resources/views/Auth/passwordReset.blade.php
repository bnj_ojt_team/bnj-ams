@extends('Auth.layout.default')
@section('title', 'Reset Password')
@section('content')
    <style>
        .fa-spinner {
            animation: rotation 2s infinite linear;
        }
        @keyframes rotation {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(359deg);
            }
        }
    </style>
    <div class="login-box">
        <div class="login-logo">
            <a href="../../index2.html"><b>BNJ</b>-AMS</a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">You are only one step a way from your new password, recover your password now.</p>

                <form action="{{ route('auth.user.password-reset', $token) }}" method="post">
                    @csrf
                    <div class="input-group mb-3">
                        <input type="password" name="password" id="password" class="form-control rounded-0 @error('password') is-invalid @enderror" placeholder="Password" value="{{ old('password') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-eye-slash" id="password-eye"></span>
                            </div>
                        </div>
                        @error('password')
                            <div class="container text-danger">{{ "*".$message }}</div>
                        @enderror
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="confirm-password" id="confirm-password" class="form-control rounded-0 @error('confirm-password') is-invalid @enderror" placeholder="Confirm Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-eye-slash" id="confirm-password-eye"></span>
                            </div>
                        </div>
                        @error('confirm-password')
                            <div class="container text-danger">{{ "*".$message }}</div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block rounded-0" id="change-password">Change password</button>
                        </div>
                    </div>
                </form>

                <p class="mt-3 mb-1">
                    <a href="{{ route('auth.user.index') }}">Login</a>
                </p>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $('#change-password').on('click', function(){
                $(this).html('Changing Password <i class="fas fa-spinner"></i>');
            });
        });
    </script>
@endsection
