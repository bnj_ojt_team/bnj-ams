@extends('errors.layout')
@section('title', 'Page Expired')
@section('content')

	<section class="content">
		<div class="error-page">
			<h2 class="headline text-warning"> 419</h2>

			<div class="error-content">
				<h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page Expired.</h3>

				<p>
					We could not find the page you were looking for.
					Meanwhile, you may <a href="{{ route('auth.user.index') }}">return to dashboard</a>
				</p>
			</div>
		</div>
	</section>

@endsection