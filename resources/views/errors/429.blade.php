@extends('errors.layout')
@section('title', 'Too Many Requests')
@section('content')

	<section class="content">
		<div class="error-page">
			<h2 class="headline text-warning"> 429</h2>

			<div class="error-content">
				<h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Too Many Requests.</h3>

				<p>
					We could not find the page you were looking for.
					Meanwhile, you may <a href="{{ route('auth.user.index') }}">return to dashboard</a>
				</p>
			</div>
		</div>
	</section>

@endsection