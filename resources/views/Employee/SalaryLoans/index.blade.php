@extends('Employee.layout.default')
@section('title', 'Employee.Salary Loans')
@section('content')

	{{-- Modal --}}
	<div class="modal fade" id="modal">
		<div class="modal-dialog modal-lg">
			<form action="" method="" id="form">
				@csrf
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="modal-title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							{{-- Content Here --}}
							{{-- Amount --}}
							<label for="amount">Amount</label>
							<div class="col-lg-12">
								<div class="input-group">
									<input type="number" name="amount" class="form-control rounded-0" id="amount" placeholder="Loan Amount">
								</div>
							</div>

							<label for="reason">Reason</label>
							<div class="col-lg-12">
								<div class="input-group">
									<textarea name="reason" id="reason" cols="30" rows="10" class="form-control rounded-0"></textarea>
								</div>
							</div>

						</div>
					</div>
					<div class="modal-footer justify-content-between">
						<button type="button" class="btn btn-default rounded-0" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary rounded-0" id="save-button"></button>
					</div>
				</div>
			</form>
		</div>
	</div>

	{{-- Datatable --}}
	<div class="card card-primary card-outline">
		<div class="card-header">
			<button class="btn btn-primary rounded-0 float-right col-2" id="add"> New Loan </button>
		</div>
		<div class="card-body">
			<table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
				<thead>
					<tr>
						<th>Loan Amount</th>
						<th>Deduction</th>
						<th>Balance</th>
						<th>Message</th>
						<th>Status</th>
						<th>Date</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<script>
		$(function() {
			const formatter = new Intl.NumberFormat('en-US', {
				style: 'currency',
				currency: 'PHP',
			});

			$('input[type="number"]').on('keypress', function(e) {
				if (!e.key.match(/^([0-9])$/g)) {
					e.preventDefault();
				}
			});

			var url = '';
			// Datatable
			var datatable_instance = $("#datatable").DataTable({
				"order": [
					[0, 'Asc']
				],
				"ordering": false,
				"responsive": true,
				"language": {
					"emptyTable": "No Salary Loans Data found"
				},
				"ajax": {
					url: base_url + 'employee/salary-loans/index',
					method: 'GET',
					dataType: 'JSON',
					beforeSend: function() {
						$('#datatable > tbody').html(
							'<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
						);
					},
				},
				"columns": [{
					data: 'amount',
					className: 'text-right',
					name: 'location',
					render: function(data, meta, row) {
						return formatter.format(data);
					}
				}, {
					data: 'salary_deduction',
					className: 'text-right',
					render: function(data, meta, row) {
						if (data == null) {
							return formatter.format(0);
						} else {
							return formatter.format(row.salary_deduction.deduction);
						}
					}
				}, {
					data: 'balance',
					className: 'text-right',
					render: function(data, meta, row) {
						return formatter.format(data);
					}
				}, {
					data: 'reason',
					render: function(data, meta, row) {
						return data;
					}
				}, {
					data: 'status',
					render: function(data, meta, row) {
						return data;
					}
				}, {
					data: 'date',
					render: function(data, meta, row) {
						return data;
					}
				}, {
					data: 'id',
					render: function(data, meta, row) {
						if (row.status === 'Pending') {
							return `<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-flat">Action</button>
                                <button type="button" class="btn btn-primary btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <a data-id="${data}" title="Edit" style="cursor: pointer;" class="dropdown-item edit"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit</a>

                                    <a data-id="${data}" title="Delete" style="cursor: pointer;" class="dropdown-item delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                </div>
                            </div>`;
						} else {
							return '--|--';
						}

					}
				}],
			});

			$('#add').on('click', function(e) {
				e.preventDefault();
				url = base_url + 'employee/salary-loans/add';
				$('#save-button').html('Request');
				$('#modal-title').html('Request A Salary Loan');
				$('#modal').modal('show');
			});

			// Form submit
			$(document).on('submit', '#form', function(e) {
				e.preventDefault();
				var data = new FormData(this);
				if (url === base_url + 'employee/salary-loans/add') {
					$('#save-button').html('Requesting <i class="fas fa-spinner"></i>');
				} else {
					$('#save-button').html('Updating Request <i class="fas fa-spinner"></i>');
				}
				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					type: 'json',
					contentType: false,
					processData: false,
					cache: false,
					success: function(data, textStatus, jqXHR) {
						$('#modal').modal('hide');
						response(data.title, data.icon);
						datatable_instance.ajax.url(base_url + 'employee/salary-loans/index')
							.load();
					},
					error: function(xhr, status, error) {
						if (url === base_url + 'employee/salary-loans/add') {
							$('#save-button').html('Save');
						} else {
							$('#save-button').html('Update Changes');
						}
						var title = '';
						var validation = JSON.parse(xhr.responseText);
						title = validation.errors ? validation.errors[Object.keys(validation
							.errors)[0]] : validation.title;
						response(title, 'error');
						datatable_instance.ajax.url(base_url + 'employee/salary-loans/index')
							.load();
					}
				});
			});

			// Click Edit
			datatable_instance.on('click', '.edit', function(e) {
				e.preventDefault();
				var dataId = $(this).attr('data-id');
				url = base_url + 'employee/salary-loans/edit/' + dataId;
				$.ajax({
					url: url,
					method: 'GET',
					dataType: 'JSON',
					success: function(data, textStatus, jqXHR) {
						$('#amount').val(data.amount);
						$('#reason').val(data.reason);
						$('#save-button').html('Update Changes');
						$('#modal-title').html('Update Loan Request');
						$('#modal').modal('show');
					},
					error: function(xhr, status, error) {
						datatable_instance.ajax.url(base_url + 'employee/salary-loans/index')
							.load();
						response(error, 'error');
					}
				});
			});


			// Delete Function
			datatable_instance.on('click', '.delete', function(e) {
				e.preventDefault();
				var dataId = $(this).attr('data-id');
				var href = base_url + 'employee/salary-loans/delete/' + dataId;
				Swal.fire({
					title: 'Delete Salary Loan?',
					text: 'Are You Sure you want to delete this Salary Loan?',
					icon: 'warning',
					animation: true,
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes',
					closeOnConfirm: false,
					closeOnCancel: false,
				}).then(function(result) {
					if (result.isConfirmed) {
						let timerInterval;
						Swal.fire({
							title: 'Deleting...',
							timer: 10000,
							animation: false,
							timerProgressBar: false,
							didOpen: () => {
								Swal.showLoading();
								const b = Swal.getHtmlContainer().querySelector('b');
								timerInterval = setInterval(() => {
									b.textContent = Swal.getTimerLeft()
								}, 100);

								$.ajax({
									url: href,
									type: 'DELETE',
									method: 'DELETE',
									dataType: 'JSON',
									success: function(data, textStatus, jqXHR) {
										Swal.close();
										datatable_instance.ajax.url(base_url +
												'employee/salary-loans/index')
											.load();
										response(data.title, data.icon);
									},
									error: function(xhr, status, error) {
										datatable_instance.ajax.url(base_url +
												'employee/salary-loans/index')
											.load();
										response(error, 'error');
									}
								});
							},
							willClose: () => {
								clearInterval(timerInterval);
							}
						});
					}
				});
			});


			// Hidden modal
			$('#modal').on('hidden.bs.modal', function() {
				url = '';
				$('#save-button').html('');
				$('#modal-title').html('');
				$('#form')[0].reset();
			});

			// Sweetalert
			var Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 4000
			});

			function response(title, icon) {
				Toast.fire({
					icon: icon,
					title: title
				})
			}
		});
	</script>
@endsection
