@extends('Employee.layout.default')
@section('title', 'Employee.Payroll')
@section('content')
    
    @php
        $months = [
            ['month_name' => ucwords('january'),'month' => 1],
            ['month_name' => ucwords('february'),'month' => 2],
            ['month_name' => ucwords('march'),'month' => 3],
            ['month_name' => ucwords('april'),'month' => 4],
            ['month_name' => ucwords('may'),'month' => 5],
            ['month_name' => ucwords('june'),'month' => 6],
            ['month_name' => ucwords('july'),'month' => 7],
            ['month_name' => ucwords('august'),'month' => 8],
            ['month_name' => ucwords('september'),'month' => 9],
            ['month_name' => ucwords('october'),'month' => 10],
            ['month_name' => ucwords('november'),'month' => 11],
            ['month_name' => ucwords('december'),'month' => 12]
        ];
    @endphp

    <link rel="stylesheet" href="{{asset('yearpicker/css/yearpicker.css')}}">
    <script src="{{asset('yearpicker/js/yearpicker.js')}}"></script>

	{{-- Datatable --}}
	<div class="card card-primary card-outline">
		<div class="card-header">
			<div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="year">Year</label>
                        <input type="search" title="Select Year" name="year" id="year" class="form-control rounded-0" placeholder="Enter Year">
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="month">Month</label>

                        <select name="month" id="month" title="Select Month"
                            class="custom-select form-control-border border-width-2">
                            <?php foreach ($months as $month):?>
                                <option value="{{ $month['month_name'] }}" {{ (date('F') == $month['month_name']) ? "selected":"" }}>{{ $month['month_name'] }}</option>
                            <?php endforeach;?> 
                        </select>
                    </div>
                </div>
            </div> 
		</div>
		<div class="card-body">
			<table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Position</th>
						<th>Date</th>

						<th>Time In Am</th>
						<th>Time Out Am</th>
						<th>Time In Pm</th>
						<th>Time Out Pm</th>

						<th>Excuse Time In Am</th>
						<th>Excuse Time Out Am</th>
						<th>Excuse Time In Pm</th>
						<th>Excuse Time Out Pm</th>
						<th>Total Hours</th>
						<th>Hours</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<script>
		$(function() {
			getContributionRate();
			var datas;
			var nameOfEmployee = '{{ auth()->user()->load(['profile'])->profile->firstname }}';
            $.ajax({
				url: base_url + 'employee/payroll/get-estimated-salary',
				method: 'GET',
				dataType: 'JSON',
				async: true,
				success: function(data, textStatus, jqXHR) {
					datas = data;
				},
				error: function(xhr, status, error) {
					response(error, 'error');
				}
			});

            function getContributionRate(){
            	$.ajax({
					url: `${base_url}employee/payroll/get-contribution-rate/${$('#month').val()}/${$('#year').val()}`,
					method: 'GET',
					dataType: 'JSON',
					async: true,
					success: function(data, textStatus, jqXHR) {
						contributionDatas = data;
					},
					error: function(xhr, status, error) {
						response(error, 'error');
					}
				});
            }

            // Added this variables
            var outsideApi = null,
            	datatableOutsideData = null,
            	tableRedraw = false;

			// Datatable
			var datatable_instance = $("#datatable").DataTable({
				"order": [
					[2, 'asc']
				],
				"dom": 'lBrtip',
				"destroy": true,
				"processing": false,
				"serchDelay": 3500,
				"deferRender": true,
				"pagingType": 'full_numbers',
				"ordering": false,
				"responsive": true,
				"language": {
					"emptyTable": "No Payroll Data found"
				},
				"ajax": {
					url: base_url + 'employee/payroll/index',
					method: 'GET',
					dataType: 'JSON',
					beforeSend: function() {
						$('#datatable > tbody').html(
							'<tr><td align="top" class="text-primary" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fas fa-2x fa-sync-alt fa-spin"></i></td></tr>'
						);
					},
                    complete: function() {
                        // Enable export buttons after the AJAX request is complete
                        datatable_instance.buttons().enable();
                    }
				},
                initComplete: function() {
                	tableRedraw = false;
                    var api = this.api();
                    outsideApi = this.api();
                    var data = api.rows().data();
                    datatableOutsideData = api.rows().data();

                    // Filter out hidden rows from the DataTable's dataset
                    var specificIds = ['empty','','wfh','contribution','deductions-header','deductions','space','overtime-collection','overtime-merge','overtime'];
                    var visibleData = data.filter(function(row) {
                        return !specificIds.includes(row.id);
                    });
                    api.clear().rows.add(visibleData).draw();
                },
				"columns": [{
					data: null,
					render: function(data, meta, row) {
						return (row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime') ? '':row.user.profile.lastname + ', ' + row.user.profile.firstname +
							', ' + row.user.profile.middlename;
					}
				}, {
					data: null,
					render: function(data, meta, row) {
						return (row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime') ? '':row.user.profile.employee_role.position + ' ' + row.user.profile
							.employee_role.level;
					}
				}, {
					data: null,
					type: 'date',
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.attendance_date == null) ? "--/--":row.attendance_date;
					}
					// Divider Normal time in
				}, {
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.timein_am == null) ? "--/--":row.timein_am;
						// return row.timein_am;
					}
				}, {
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.timeout_am == null) ? "--/--":row.timeout_am;
					}
				}, {
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.timein_pm == null) ? "--/--":row.timein_pm;
					}
				}, {
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.timeout_pm == null) ? "--/--":row.timeout_pm;
					}
				}, {
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.excuse.timeout_am == null) ? "--/--":row.excuse.timeout_am;
					}
				}, { // Divider excuse time
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.excuse.timein_am == null) ? "--/--":row.excuse.timein_am;
					}
				}, {
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.excuse.timeout_pm == null) ? "--/--":row.excuse.timeout_pm;
					}
				}, {
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.excuse.timein_pm == null) ? "--/--":row.excuse.timein_pm;
					}
				}, {
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.attendance_total_hours == null) ? "--/--":row.attendance_total_hours;
					}
				}, {
					data: null,
					render: function(data, meta, row) {
                        if(row.id === '' || row.id === 'wfh' || row.id === 'empty' || row.id === 'contribution' || data.id === 'deductions-header' || data.id === 'deductions' || data.id == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
                            return '';
                        }
						return (row.minus_hours == null) ? "--/--":row.minus_hours;
					}
				}],
				"buttons": [{
						extend: 'excelHtml5',
						attr: {
							id: 'excel'
						},
						customize: function(xlsx) {
							customizeExcel(xlsx);
						},
						createEmptyCells: true,
						title: `${moment().format('MMMM')}_${moment().format('YYYY')}_${nameOfEmployee}`,
						text: '<i class="far fa-file-excel"></i> .xlsx',
						tag: 'button',
						className: 'btn btn-flat btn-success rounded-bottom border-top-0 border-default px-5',
						exportOptions: {
							columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
						},
						action: function(e, dt, node, config) {
							Swal.fire({
								title: 'Export To Excel',
								text: 'Are You Sure?',
								icon: 'info',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Yes'
							}).then(function(result) {
								if (result.isConfirmed) {
									response('Exporting, Please Wait...', 'info');
									ShowHiddenRows();
									setTimeout(function() {
                                        $.fn.dataTable.ext.buttons.excelHtml5.action
                                            .call(dt.button(this), e, dt, node,
                                                config);
                                        RehideHiddenRows();
                                    }, 300);
								}
							});
						},
						footer: false
					},
				],
				"rowCallback": function(row, data, index) {
					if (data.id == null) {
						$(row).css("background-color", "yellow");
					}
					if(data.is_non_working){
						$(row).css("color", "red");
					}
					if(data.is_work_from_home){ // Added this if line
						$(row).css("background-color", "#0275D8");
					}
					if(data.id == 'empty' || data.id == '' || data.id == 'wfh' || data.id  == 'contribution' || data.id  == 'deductions-header' || data.id  == 'deductions' || data.id  == 'space' || data.id == 'overtime-collection' || data.id == 'overtime-merge' || data.id == 'overtime'){
						$(row).hide();
					}
				},
			});

			datatable_instance.buttons().disable();

			// When table is fully loaded
			$('#datatable').on('draw.dt', function() {
				if(tableRedraw){
					datatableOutsideData = datatable_instance.data();
					tableRedraw = false;
					RehideHiddenRows();
				} 
	        });
	        
	        function RehideHiddenRows(){
				var api = outsideApi;
                var data = datatableOutsideData;
                var specificIds = ['empty','','wfh','contribution','deductions-header','deductions','space','overtime-collection','overtime-merge','overtime'];
                var visibleData = data.filter(function(row) {
                    return !specificIds.includes(row.id);
                });
                api.clear().rows.add(visibleData).draw();
			}

			function ShowHiddenRows(){
				var api = outsideApi;
                var data = datatableOutsideData;
                var visibleData = data.filter(function(row) {
                    return true;
                });
                api.clear().rows.add(visibleData).draw();
			}

			// Datatable customize excel option
			function customizeExcel(xlsx){
				var excelMap = {
					0: 'A', 1: 'B', 2: 'D', 3: 'E', 4: 'F', 5: 'G', 6: 'H', 7: 'I', 8: 'J', 9: 'K', 10: 'L', 11: 'M', 12: 'N',
				};
				var new_style = xmlCode;
				xlsx.xl['styles.xml'] = $.parseXML(new_style);
				var sheet = xlsx.xl.worksheets['sheet1.xml'];
				$('c[r=A1]', sheet).attr('s', '24');
                var count = 3;
                var i = -2,
                	j = 0,
                	firstCountToMerge = null,
                	lastCountToMerge = null,
                	lastLoopJ = null,
                	lastLoopTD = null,
                	deductionFirstMerge = null,
                	deductionLastMerge = null,
                	deductionLastValue = null;
                datatable_instance.rows().every(function(rowIdx, tableLoop, rowLoop){
                    var data = this.data();
                    if(data.id == null){ // If data.id is equal to null
                        if(data.is_non_working == true){
                            for (td = 0; td < Object.keys(excelMap).length; td++) {
                                $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '68');
                                // $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '68');
                            }
                        }else{
                            for (td = 0; td < Object.keys(excelMap).length; td++) {
                                $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '27');
                            }
                        }
                    }else if(data.id == ''){ // If id is equal to empty string
                    	var mergeCellCount = parseInt($('mergeCells', sheet).attr('count'));
                    	$('mergeCells', sheet).attr('count', mergeCellCount++);
                    	$('mergeCells', sheet).append('<mergeCell ref="A'+count+':M'+count+'"></mergeCell>');
                    	$('c[r=A'+count+'] t', sheet).text(data.text);
                    	$('c[r=A'+count+']', sheet).attr('s', '24');
                    }else if(data.id == 'wfh'){
                    	// Change text node
                    	$('c[r=A'+count+'] t', sheet).text(data.A);
                    	$('c[r=B'+count+'] t', sheet).text(data.B);
                    	$('c[r=C'+count+'] t', sheet).text(data.C);
                    	$('c[r=D'+count+'] t', sheet).text(data.D);
                    	$('c[r=E'+count+'] t', sheet).text(data.E);
                    	// Change cell format
                    	$('c[r=A'+count+']', sheet).attr('s', '2');
                    	$('c[r=B'+count+']', sheet).attr('s', '2');
                    	$('c[r=C'+count+']', sheet).attr('s', '2');
                    	$('c[r=D'+count+']', sheet).attr('s', '2');
                    	$('c[r=E'+count+']', sheet).attr('s', '2');
                	}else if(data.id == 'empty'){
                		var excelLetters = {
						    0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F', 6: 'G', 7: 'H', 8: 'I', 9: 'J',
						};
						// Generate line for the computation of salary
						if(i >= 0){
							for (td = 0; td < Object.keys(excelLetters).length; td++) {
								if(i == 1){
									$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(datas[i][td]);
									$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '18');
								}else if(i == 2){
									$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(datas[i][td]);
									$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
								}else{
									$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(datas[i][td]);
									$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
								}
							}
						}
						i++;
					}else if(data.id == 'contribution'){
						var excelLetters = {
						    0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F',
						};

						// Generate line for the computation of salary
						if(contributionDatas.length < 8){
							if(contributionDatas[j] != null){
								if(j >= 0){
									for (td = 0; td < Object.keys(excelLetters).length; td++) {
										if(j == 1){
											$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(contributionDatas[j][td]);
											$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '18');
										}else if(j == 2){
											firstCountToMerge = count;
											$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(contributionDatas[j][td]);
											$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
										}else if(j > 2){
											lastCountToMerge = count;
											lastLoopJ = j;
											lastLoopTD = td;
											$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(contributionDatas[j][td]);
											$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
										}else{
											$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(contributionDatas[j][td]);
											$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
										}
									}
								}
							}
								
						}else{
							var mergeCellCount = parseInt($('mergeCells', sheet).attr('count'));
                        	$('mergeCells', sheet).attr('count', mergeCellCount++);
                        	$('mergeCells', sheet).append('<mergeCell ref="A'+count+':F'+count+'"></mergeCell>');
                        	$('c[r=A'+count+'] t', sheet).text('Contributions limit is up to 5 only');
                        	$('c[r=A'+count+']', sheet).attr('s', '25');
						}
						
						j++;
                	}else if(data.id == "overtime-merge" || data.id == "overtime-collection" || data.id == "overtime" || data.id == 'space'){
                		var excelLetters = {
						    0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F',
						};
						// console.log(data);
                		if(data.id == "overtime-merge"){
                			var mergeCellCount = parseInt($('mergeCells', sheet).attr('count'));
                        	$('mergeCells', sheet).attr('count', mergeCellCount++);
                        	$('mergeCells', sheet).append('<mergeCell ref="A'+count+':M'+count+'"></mergeCell>');
                        	$('c[r=A'+count+'] t', sheet).text(data['text']);
                        	$('c[r=A'+count+']', sheet).attr('s', '24');
                		}else if(data.id == "overtime"){
							for (td = 0; td < Object.keys(excelLetters).length; td++) {
								$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(data[excelLetters[td]]);
								$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '2');
							}
                		}else if(data.id == 'space'){
                			// Do nothing if data.id is space to create a spacing
                		}else{
                			for (td = 0; td < Object.keys(excelLetters).length; td++) {
                				$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(data[excelLetters[td]]);
                				if(excelLetters[td] == 'C'){
                					$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '66');
                				}else{
                					$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '30');
                				}
								
							}
                		}
                	}else if(data.id == 'deductions' || data.id == 'deductions-header'){
                		var excelLetters = {
						    0: 'A', 1: 'B', 2: 'C',
						};
						if(data.id == 'deductions-header'){
							for (td = 0; td < Object.keys(excelLetters).length; td++) {
								$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(data[excelLetters[td]]);
                                $('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '18');
                            }
                            deductionFirstMerge = count + 1;
						}else if(data.id == 'deductions'){
							for (td = 0; td < Object.keys(excelLetters).length; td++) {
								$('c[r='+excelLetters[td]+''+count+'] t', sheet).text(data[excelLetters[td]]);
								if(excelLetters[td] == 'A'){ // Of column is date
									$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '66');
								}else{
									$('c[r='+excelLetters[td]+''+count+']', sheet).attr('s', '4');
								}  
                            }
                            deductionLastMerge = count;
                            deductionLastValue = data.C;
						}
						
                	}else{ // If id not null
                        if(data.is_non_working == true){
                            for (td = 0; td < Object.keys(excelMap).length; td++) {
                                $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '53');
                            }
                        }else if (data.is_work_from_home == 1){
                        	for (td = 0; td < 11; td++) {
                                $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '30');
                            }
                        }else{
                            for (td = 0; td < 11; td++) {
                                $('c[r='+excelMap[td]+''+count+']', sheet).attr('s', '3');
                            }
                        }
                    }

                    count++;
                });
				// Merged Cells
				if(firstCountToMerge != null &&
				lastCountToMerge != null &&
				lastLoopJ != null &&
				lastLoopTD != null){
					var mergeCellCount = parseInt($('mergeCells', sheet).attr('count'));
                	$('mergeCells', sheet).attr('count', mergeCellCount++);
                	$('mergeCells', sheet).append('<mergeCell ref="F'+firstCountToMerge+':F'+lastCountToMerge+'"></mergeCell>');
                	$('c[r=F'+firstCountToMerge+'] t', sheet).text(contributionDatas[lastLoopJ][lastLoopTD]);
                	$('c[r=F'+firstCountToMerge+']', sheet).attr('s', '70');
				}

				if(deductionFirstMerge != null &&
				deductionLastMerge != null &&
				deductionLastValue != null){
					var mergeCellCount = parseInt($('mergeCells', sheet).attr('count'));
                	$('mergeCells', sheet).attr('count', mergeCellCount++);
                	$('mergeCells', sheet).append('<mergeCell ref="C'+deductionFirstMerge+':C'+deductionLastMerge+'"></mergeCell>');
                	$('c[r=C'+deductionFirstMerge+'] t', sheet).text(deductionLastValue);
                	$('c[r=C'+deductionFirstMerge+']', sheet).attr('s', '70');
				}
			}

			// Sweetalert
			var Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 4000
			});

			function response(title, icon) {
				Toast.fire({
					icon: icon,
					title: title
				})
			}

			$('#year').yearpicker({
                onShow:null,
                markAsBold: true,
                onChange : function(value){
                    if(value != null){
                    	tableRedraw = true;
                    	datatable_instance.buttons().disable();
                    	$.ajax({
							url: `${base_url}employee/payroll/get-estimated-salary/${$('#month').val()}/${value}`,
							method: 'GET',
							dataType: 'JSON',
							async: true,
							success: function(data, textStatus, jqXHR) {
								datas = data;
							},
							error: function(xhr, status, error) {
								response(error, 'error');
							}
						});
                        datatable_instance.ajax.url(`${base_url}employee/payroll/index/${$('#month').val()}/${value}`).load();

                        getContributionRate();
                    }
                },
                allowCustomValue: true,
            });

			$('#year').on('keypress input', function (e) {
                e.preventDefault();
            })

            $('#month').on('change', function(e){
                e.preventDefault();
            	tableRedraw = true;
                datatable_instance.buttons().disable();
                $.ajax({
                	url: `${base_url}employee/payroll/get-estimated-salary/${$(this).val()}/${$('#year').val()}`,
					method: 'GET',
					dataType: 'JSON',
					async: true,
					success: function(data, textStatus, jqXHR) {
						datas = data;
					},
					error: function(xhr, status, error) {
						response(error, 'error');
					}
				});
                datatable_instance.ajax.url(`${base_url}employee/payroll/index/${$(this).val()}/${$('#year').val()}`).load();
                getContributionRate();
            });

			var xmlCode =`<?xml version="1.0" encoding="UTF-8"?>
				<styleSheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"
				    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac"
				    xmlns:x14ac="https://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">
				    <numFmts count="2">
				        <numFmt numFmtId="171" formatCode="d/mm/yyyy;@"/>
				        <numFmt numFmtId="172" formatCode="m/d/yyyy;@"/>
				    </numFmts>
				    <fonts count="10" x14ac:knownFonts="1">
				        <font>
				            <sz val="11"/>
				            <color theme="1"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				        <font>
				            <sz val="11"/>
				            <color theme="1"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				        <font>
				            <b/>
				            <sz val="11"/>
				            <color theme="1"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				        <font>
				            <sz val="11"/>
				            <color theme="0"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				        <font>
				            <i/>
				            <sz val="11"/>
				            <color theme="1"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				        <font>
				            <sz val="11"/>
				            <color rgb="FFC00000"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				        <font>
				            <sz val="11"/>
				            <color rgb="FF006600"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				        <font>
				            <sz val="11"/>
				            <color rgb="FF990033"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				        <font>
				            <sz val="11"/>
				            <color rgb="FF663300"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				        <font>
				            <b/>
				            <sz val="11"/>
				            <color rgb="FFC00000"/>
				            <name val="Calibri"/>
				            <family val="2"/>
				            <scheme val="minor"/>
				        </font>
				    </fonts>
				    <fills count="16">
				        <fill>
				            <patternFill patternType="none"/>
				        </fill>
				        <fill>
				            <patternFill patternType="gray125"/>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FFC00000"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FFFF0000"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FFFFC000"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FFFFFF00"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FF92D050"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FF00B050"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FF00B0F0"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FF0070C0"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FF002060"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FF7030A0"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor theme="1"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FF99CC00"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FFFF9999"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				        <fill>
				            <patternFill patternType="solid">
				                <fgColor rgb="FFFFCC00"/>
				                <bgColor indexed="64"/>
				            </patternFill>
				        </fill>
				    </fills>
				    <borders count="2">
				        <border>
				            <left/>
				            <right/>
				            <top/>
				            <bottom/>
				            <diagonal/>
				        </border>
				        <border>
				            <left style="thin">
				                <color indexed="64"/>
				            </left>
				            <right style="thin">
				                <color indexed="64"/>
				            </right>
				            <top style="thin">
				                <color indexed="64"/>
				            </top>
				            <bottom style="thin">
				                <color indexed="64"/>
				            </bottom>
				            <diagonal/>
				        </border>
				    </borders>
				    <cellStyleXfs count="3">
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="0"/>
				        <xf numFmtId="9" fontId="1" fillId="0" borderId="0" applyFont="0" applyFill="0" applyBorder="0" applyAlignment="0" applyProtection="0"/>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
				            <alignment vertical="center"/>
				        </xf>
				    </cellStyleXfs>
				    <cellXfs count="73">
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0"/>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="2" fillId="0" borderId="0" xfId="0" applyFont="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="4" fillId="0" borderId="0" xfId="0" applyFont="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="4" fillId="0" borderId="0" xfId="0" applyFont="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="4" fillId="0" borderId="0" xfId="0" applyFont="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyBorder="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
				            <alignment vertical="top" wrapText="1"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyBorder="1" applyAlignment="1">
				            <alignment vertical="top" wrapText="1"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="2" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="3" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="4" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="5" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="6" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="7" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="8" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="9" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="10" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="11" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="12" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="2" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="3" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="4" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="5" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="6" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="7" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="8" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="9" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="10" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="11" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="12" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="2" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="3" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="4" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="5" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="6" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="7" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="8" borderId="0" xfId="0" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="9" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="10" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="11" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="3" fillId="12" borderId="0" xfId="0" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top" textRotation="90"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="center" textRotation="255"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
				            <alignment textRotation="45"/>
				        </xf>
				        <xf numFmtId="0" fontId="5" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="5" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="5" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="5" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="5" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="5" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="right" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="6" fillId="13" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="6" fillId="13" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyBorder="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="7" fillId="14" borderId="0" xfId="1" applyNumberFormat="1" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="7" fillId="14" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyBorder="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="8" fillId="15" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="8" fillId="15" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyFill="1" applyBorder="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyBorder="1" applyAlignment="1">
				            <alignment vertical="top"/>
				        </xf>
				        <xf numFmtId="171" fontId="0" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="172" fontId="0" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="171" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="172" fontId="0" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="171" fontId="9" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="172" fontId="9" fillId="0" borderId="1" xfId="0" applyNumberFormat="1" applyFont="1" applyBorder="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="171" fontId="9" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="172" fontId="9" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" applyFont="1" applyAlignment="1">
				            <alignment horizontal="center" vertical="top"/>
				        </xf>
				        <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" applyAlignment="1">
				            <alignment horizontal="center" vertical="center"/>
				        </xf>
				    </cellXfs>
				    <cellStyles count="3">
				        <cellStyle name="Procent" xfId="1" builtinId="5"/>
				        <cellStyle name="Standaard" xfId="0" builtinId="0"/>
				        <cellStyle name="MergedCell" xfId="2"/>
				    </cellStyles>
				    <dxfs count="0"/>
				    <tableStyles count="0" defaultTableStyle="TableStyleMedium2" defaultPivotStyle="PivotStyleLight16"/>
				    <colors>
				        <mruColors>
				            <color rgb="FF663300"/>
				            <color rgb="FFFFCC00"/>
				            <color rgb="FF990033"/>
				            <color rgb="FF006600"/>
				            <color rgb="FFFF9999"/>
				            <color rgb="FF99CC00"/>
				        </mruColors>
				    </colors>
				    <extLst>
				        <ext uri="{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}"
				            xmlns:x14="https://schemas.microsoft.com/office/spreadsheetml/2009/9/main">
				            <x14:slicerStyles defaultSlicerStyle="SlicerStyleLight1"/>
				        </ext>
				    </extLst>
				</styleSheet>`;
		});
	</script>
@endsection
