<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <p class="container1"style="font-size: 3rem; color: black;">
        <div class="digital-clock">00:00:00</div> | Start Working Today!
    </p>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-user"></i>
                {{ auth()->user()->username }}
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header">Account Settings</span>
                <a href="{{ route('employee.users.account-setting') }}"
                    class="dropdown-item {{ url()->current() == route('employee.users.account-setting') ? 'active' : '' }}">
                    <i class="fas fa-user mr-2"></i> {{ ucfirst(__('My Account')) }}
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('auth.user.logout') }}" class="dropdown-item">
                    <i class="fas fa-sign-out-alt mr-2"></i> {{ ucfirst(__('Logout')) }}
                </a>
                <div class="dropdown-divider"></div>
            </div>
        </li>
    </ul>
</nav>
