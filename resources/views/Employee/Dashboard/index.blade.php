@extends('Employee.layout.default')
@section('title', 'Employee.Dashboard')
@section('content')
     <div class="row">
        {{-- Accordion --}}

        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h5 class="m-0">Quote For Today...</h5>
                </div>
                <div class="card-body">

                    <p class="card-text" style="font-size: 1.2em;font-weight: bolder;">
                        — {{ $quote }}
                    </p>
                    {{--<a href="#" class="btn btn-primary">Go somewhere</a>--}}
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card card-primary shadow-none">
                <div class="card-header">
                    <h3 class="card-title">Calendar View</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-calendar-day"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    {{-- Evo Calendar --}}
                    <div class="hero">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var color = $('span.input-group-text > i.fa-square').css('color');
        // Evo Calendar
        var calendar = $('#calendar').evoCalendar({
                settingName: 'Calendar',
                theme: 'Royal Navy',
                language: 'en',
                todayHighlight: true,
                sidebarDisplayDefault: true,
                sidebarToggler: true,
                eventDisplayDefault: true,
                eventListToggler: true
            });

            // Set Evo Calendar again after Destroy
            calendar.on('destroy', function(e) {
                calendar = $('#calendar').evoCalendar({
                    settingName: 'Calendar',
                    theme: 'Royal Navy',
                    language: 'en',
                    todayHighlight: false,
                    sidebarDisplayDefault: true,
                    sidebarToggler: true,
                    eventDisplayDefault: true,
                    eventListToggler: true
                });
                setCalendar();
            });

                  // Get Holidays
            function getHolidays() {
                var events = [];
                $.ajax({
                    url: base_url + 'employee/dashboard/get-all-holidays',
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    global: false,
                    async: false,
                    success: function(data, textStatus, jqXHR) {
                        $.map(data.data, function(data) {
                            events.push({
                                id: data.id,
                                name: data.holiday_name,
                                date: data.holiday_date,
                                type: data.holiday_type,
                                everyYear: data.is_per_year,
                                color: data.color
                            });
                        });
                    },
                    error: function(xhr, status, error) {
                        datatable_instance.ajax.url(base_url +
                                'employee/dashboard/get-all-holidays')
                            .load();
                        response(error, 'error');
                    }
                });
                return events;
            }

              // Get Birthdays
              function getBirthdays() {
                var events = [];
                $.ajax({
                    url: base_url + 'employee/dashboard/get-birthdays',
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    global: false,
                    async: false,
                    success: function(data, textStatus, jqXHR) {
                        $.map(data, function(data) {
                            events.push({
                                id: data.id,
                                name: 'Birthday',
                                date: data.birthdate,
                                description: 'Birthday of '+data.firstname+' '+data.lastname,
                                badge:'Birthday',
                                type: 'holiday',
                                everyYear: true,
                                color: '#8DCBE6'
                            });
                        });
                    },
                    error: function(xhr, status, error) {
                        response(error, 'error');
                    }
                });
                return events;
            }

            // Set Calendar
            function setCalendar() {
                $('#calendar').evoCalendar('addCalendarEvent', getHolidays()).evoCalendar('addCalendarEvent', getBirthdays());
            }
            setCalendar();
    </script>
@endsection
