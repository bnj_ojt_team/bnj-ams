<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use App\Models\EmployeeRole;

class UsersController extends Controller
{
    // Account Settings Function
    public function accountSetting(){
        $user = User::with('profile', 'profile.employeeRole', 'contact')->findOrFail(auth()->user()->id);
        return view('Employee.Users.account-setting', [
            'user' => $user
        ]);
    }

    // Edit user
    public function edit(Request $request){
        $user = User::with('profile', 'profile.employeeRole', 'contact')->findOrFail(auth()->user()->id);
        if($request->isMethod('post')){
            if($request->has('password')){
                $validator = $request->validate([
                    'current-password' => ['bail', 'required', function ($attribute, $value, $fail) use ($user) {
                        if (!\Hash::check($value, $user->password)) {
                            return $fail(__('The current password is incorrect.'));
                        }
                    }],
                    'password' => ['bail', 'required', 'min:8', function ($attribute, $value, $fail) use ($user) {
                        if (\Hash::check($value, $user->password)) {
                            return $fail(__('New password should not be the same as your current password.'));
                        }
                    }],
                    'confirm-password' => ['bail', 'required', 'same:password'],
                ],[
                    'password.required' => ucfirst('New password is required'),
                ]);
            }else if($request->has('contact')){
                $validator = $request->validate([
                    'contact.fullname' => ['bail', 'required', 'max:255'],
                    'contact.relation' => ['bail', 'required', 'max:255'],
                    'contact.address' => ['bail', 'required','max:65535'],
                    'contact.phone_number' => ['bail', 'required','max:255','regex:/^(09|\+639)[\d-]{9,12}/', Rule::unique('contacts','phone_number')->ignore($user->contact->id)],
                ]);
                $user->contact()->update($request->input('contact'));
            }else if($request->has('profile')) {
                $validator = $request->validate([
                    'profile.firstname' => ['bail', 'required', 'max:255'],
                    'profile.middlename' => ['bail', 'required', 'max:255'],
                    'profile.lastname' => ['bail', 'required', 'max:255'],
                    'profile.employee_id_number' => ['bail', 'required'],
                    'profile.birthdate' => ['bail', 'required', 'date'],
                    'profile.contact_number' => ['bail', 'required', 'max:255', 'regex:/^(09|\+639)[\d-]{9,12}/', Rule::unique('profiles', 'contact_number')->ignore($user->profile->id)],
                    'profile.address' => ['bail', 'required', 'max:65535'],
                ],
                [
                    'profile.contact_number.unique' => ['Contact Number are Already exist'],
                ]
            );
                $user->profile()->update($request->input('profile'));
            }else{
                $validator = $request->validate([
                    'username' => ['bail', 'required', Rule::unique('users','username')->ignore($user->id)],
                    'email' => ['bail', 'required', Rule::unique('users','email')->ignore($user->id), 'email'],
                    'file' => ['bail', 'nullable', 'image'],
                ],[
                    'email.email' => ['Please include "@" in Email Address!'],
                    'file.image' => ['File must be an image. Required File Type: JPEG, PNG, GIF & Etc. Only!'],
                ]);
            }
            $user->update($request->all());
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filepath = storage_path('app/public/avatars');
                $currentImage = $filepath.'/'.$user->image;
                if(File::isFile($currentImage)){
                    File::delete($currentImage);
                }
                $filename = uniqid().uniqid().uniqid().'.'.$file->getClientOriginalExtension();
                if(!File::isDirectory($filepath)){
                    File::makeDirectory($filepath);
                }
                if($file){
                    $file->move($filepath, $filename);
                }
                $user->image = $filename;
            }
            if($user->save() && $user->push()){
                return response()->json(['title' => 'User Account has updated. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'User Account could not be updated. ', 'icon' => 'error'], 404);
            }
        }
        return response()->json($user);
    }
}
