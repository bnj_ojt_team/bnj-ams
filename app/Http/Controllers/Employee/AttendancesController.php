<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Attendance;
use App\Models\HomeAttendance;
use App\Models\Holiday;
use App\Models\CompanySetting;
use App\Models\Reason;
use App\Models\Accomplishment;
use App\Rules\CheckFormatAnteMeridiem;
use App\Rules\CheckFormatPostMeridiem;
use DB;

class AttendancesController extends Controller
{
    public function index(Request $request, $year = null, $month = null){
        $year = ($year == null || $year == '') ? date('Y') : $year;
        $month = ($month == null) ? date('F') : $month;
        // if request wants json
    	if($request->wantsJson()){
    		$startDate = new Carbon('first day of '.$month.' '.$year);
            $endDate = new Carbon('last day of '.$month.' '.$year);
    		$collection = collect();
            $attendanceCollection = Attendance::with('excuse','reason','accomplishment', 'accomplishment.homeAttendances')->myAttendance()->get();
            $holidayCollection = Holiday::query()->get();
            // create collection base on dates
    		while($startDate->toDateString() <= $endDate->toDateString()){
    			$attendance = $attendanceCollection->where('attendance_date', '=', $startDate->toDateString())->first();
                $holiday = $holidayCollection->filter(function($item) use ($startDate) {
                    return Carbon::parse($item['holiday_date'])->format('m-d') == $startDate->format('m-d');
                })->first();
                $holidayReturn = 0;
                if(!empty($holiday)){
                    if($holiday->is_per_year == 1){
                        $holidayReturn = 1;
                    }else{
                        $holiday = $holidayCollection->filter(function($item) use ($startDate) {
                            return Carbon::parse($item['holiday_date'])->format('Y-m-d') == $startDate->format('Y-m-d');
                        })->first();
                        if(!empty($holiday)){
                            $holidayReturn = 1;
                        }
                    }
                }else if($startDate->format('l') == 'Saturday' || $startDate->format('l') == 'Sunday'){
                    $holidayReturn = 1;
                }else if(getCurrentDate()->toDateString() == $startDate->toDateString()){
                    $holidayReturn = 2;
                }
                // add data into new create collection
                if(!empty($attendance)){
                    $collection->push([
                        'id' => $attendance->id,
                        'attendance_date' => $startDate->toDateString(),
                        'day' => $startDate->format('l'),
                        'timein_am' => $attendance->timein_am,
                        'timeout_am' => $attendance->timeout_am,
                        'timein_pm' => $attendance->timein_pm,
                        'timeout_pm' => $attendance->timeout_pm,
                        'excuse_timein_am' => ($attendance->excuse == null) ? null:$attendance->excuse->timein_am,
                        'excuse_timeout_am' => ($attendance->excuse == null) ? null:$attendance->excuse->timeout_am,
                        'excuse_timein_pm' => ($attendance->excuse == null) ? null:$attendance->excuse->timein_pm,
                        'excuse_timeout_pm' => ($attendance->excuse == null) ? null:$attendance->excuse->timeout_pm,

                        'work_from_home_hours' =>
                            calculateWorkFromHomeHours($attendance->accomplishment->homeAttendances ?? null),
                        'excuse_total_hours' => ($attendance->excuse == null) ? null:$attendance->excuse->excuse_total_hours,
                        'total_hours' => $attendance->attendance_total_hours,

                        'reason' => ($attendance->reason == null) ? null:$attendance->reason,
                        'accomplishment' => ($attendance->accomplishment == null) ? null:$attendance->accomplishment->id,
                        'is_holiday' => $holidayReturn,
                        'is_greater_than_today' => ($startDate->toDateString() > getCurrentDate()->toDateString()) ? 1:0,
                    ]);
                }else{
                    $collection->push([
                        'id' => null,
                        'attendance_date' => $startDate->toDateString(),
                        'day' => $startDate->format('l'),
                        'timein_am' => null,
                        'timeout_am' => null,
                        'timein_pm' => null,
                        'timeout_pm' => null,
                        'excuse_timein_am' => null,
                        'excuse_timeout_am' => null,
                        'excuse_timein_pm' => null,
                        'excuse_timeout_pm' => null,

                        'work_from_home_hours' => null,
                        'excuse_total_hours' => null,
                        'total_hours' => null,

                        'reason' => null,
                        'accomplishment' => null,
                        'is_holiday' => $holidayReturn,
                        'is_greater_than_today' => ($startDate->toDateString() > getCurrentDate()->toDateString()) ? 1:0,
                    ]);
                }
    			$startDate->addDay();
    		}
    		return response()->json(['data' => $collection]);
    	}
        return view('Employee.Attendances.index', [
            'year' => $year,
            'month' => $month
        ]);
    }

    public function setAttendance(Request $request){
        if($request->isMethod('post')){
            // dd($request->all());
            $request->merge(['user_id' => auth()->user()->id]);
            if($request->has('accomplishment')){
                $request->merge(['is_work_from_home' => ($request->boolean('is_work_from_home') == true) ? 1:0]);
            }
            $validator = $request->validate([
                'timein_am' => ['bail', new CheckFormatAnteMeridiem],
                'timeout_am' => ['bail', 'after:timein_am', 'nullable'],
                'timein_pm' => ['bail', new CheckFormatPostMeridiem],
                'timeout_pm' => ['bail', 'after:timein_pm', 'nullable'],
                'excuse.timeout_am' => ['bail', new CheckFormatAnteMeridiem],
                'excuse.timein_am' => ['bail', 'after:excuse.timeout_am', 'nullable'],
                'excuse.timeout_pm' => ['bail', new CheckFormatPostMeridiem],
                'excuse.timein_pm' => ['bail', 'after:excuse.timeout_pm', 'nullable'],
                'accomplishment.home_attendances.*.timein' => ['bail', function($attribute, $value, $fail) use ($request){
                    if($request->has('accomplishment') && $value == ''){
                        $fail('You cant fool the system. ');
                    }
                }],
                'accomplishment.home_attendances.*.timeout' => ['bail', function($attribute, $value, $fail) use ($request){
                    if($request->has('accomplishment') && $value == ''){
                        $fail('You cant fool the system. ');
                    }
                }],
            ], [
                'timeout_am.after' => ucwords('The time out am must be a time after time in am.'),
                'timeout_pm.after' => ucwords('The time out pm must be a time after time in pm.'),
                'excuse.timein_am.after' => ucwords('The excuse time in am must be a time after excuse time out am.'),
                'excuse.timein_pm.after' => ucwords('The excuse time in pm must be a time after excuse time out pm.'),
                'excuse.timein_am.before' => ucwords('The excuse time in am must be a time before '
                    .Carbon::parse(getCompanySetting()->morning_time_out)->format('h:i A')),
                'excuse.timein_pm.before' => ucwords('The excuse time in pm must be a time before '
                    .Carbon::parse(getCompanySetting()->afternoon_time_out)->format('h:i A')),
            ]);

            if(Carbon::parse($request->input('attendance_date'))->toDateString() > getCurrentDate()->toDateString()){
                return response()->json(['title' => 'You cant input on future dates row.', 'icon' => 'info'], 200);
            }else{
                $attendance = Attendance::updateOrCreate([
                    'attendance_date' => $request->input('attendance_date'),
                    'user_id' => $request->input('user_id')
                ],$request->all());
                // if request has any excuses input
                if($request->has('excuse')){
                    $attendance->excuse()->updateOrCreate([
                        'attendance_id' => $attendance->id
                    ],$request->input('excuse'));
                }
                // if request has any reasons input
                if($request->has('reason')){
                    $attendance->reason()->updateOrCreate([
                        'attendance_id' => $attendance->id,
                        'report_type' => $request->input('reason.report_type')
                    ],$request->input('reason'));
                }
                // if request has any accomplishments input
                if($request->has('accomplishment')){
                    if($request->boolean('is_work_from_home') == true){
                        $accomplishment_array = $request->accomplishment;
                        $accomplishment_array['status'] = 'Pending';
                    }else{
                        $accomplishment_array = $request->accomplishment;
                        $accomplishment_array['status'] = 'Submitted';
                    }
                    $request->merge(['accomplishment' => $accomplishment_array]);
                    $accomplishment = $attendance->accomplishment()->updateOrCreate([
                        'attendance_id' => $attendance->id
                    ],$request->input('accomplishment'));
                    if($request->has('accomplishment.home_attendances')){
                        if($accomplishment->homeAttendances->count() > 0){
                            $ids = [];
                            foreach ($accomplishment->homeAttendances as $item) {
                                array_push($ids, $item->id);
                            }
                            $accomplishment->homeAttendances()->whereIn('id', $ids)->delete();
                        }

                        $accomplishment->homeAttendances()->createMany($request->input('accomplishment.home_attendances'));
                    }else{
                        if($accomplishment->homeAttendances->count() > 0){
                            $ids = [];
                            foreach ($accomplishment->homeAttendances as $item) {
                                array_push($ids, $item->id);
                            }
                            $accomplishment->homeAttendances()->whereIn('id', $ids)->delete();
                        }
                    }
                }
                $attendanceCollection = Attendance::with('excuse','reason','accomplishment')->myAttendance()->get();
                $attendance = $attendanceCollection->where('attendance_date', '=', Carbon::parse($request->input('attendance_date'))->toDateString())->first();
                computeHomeAttendances($request->input('attendance_date')); // This is a Helper to compute the attendance of work from homes
                // There are the helpers for the computation of the attendance and excuse attendance
                ParseExcuse(($attendance->excuse == null) ? null:$attendance->excuse->timein_am,
                            ($attendance->excuse == null) ? null:$attendance->excuse->timeout_am,
                            ($attendance->excuse == null) ? null:$attendance->excuse->timein_pm,
                            ($attendance->excuse == null) ? null:$attendance->excuse->timeout_pm,
                            $attendance->id);
                ParseAttendance($attendance->timein_am,
                            $attendance->timeout_am,
                            $attendance->timein_pm,
                            $attendance->timeout_pm,
                            $attendance->id);

                if($attendance->save()){
                    return response()->json(['title' => 'Attendance has been updated. ', 'icon' => 'success'], 200);
                }else{
                    return response()->json(['title' => 'Attendance could not be updated. ', 'icon' => 'error'], 404);
                }
            }
        }
    }

    // Get Reason data for edit
    public function getReasonData($id = null, $reportType = null){
        $reason = Reason::where('attendance_id', $id)
                        ->where('report_type', $reportType)
                        ->first();
        $collection = collect();
        if(empty($reason)){
            $collect = $collection->push([
                'report_type' => null,
                'content' => '<p>Some Text.</p>'
            ])->first();
            return response()->json($collect);
        }
        return response()->json($reason);
    }

    // Get Accomplishment data for edit
    public function getAccomplishmentData($id = null){
        $accomplishment = Accomplishment::with('homeAttendances', 'attendance')->where('attendance_id','=',$id)->firstOrFail();
        return response()->json($accomplishment);
    }
}
