<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quote;
use App\Models\Holiday;
use App\Models\Profile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;

class DashboardController extends Controller
{
    public function index(Request $request){
        $quote = '';

        $quotes = Quote::query()->where('is_active','=','1')->first();

        if(!empty($quotes)){
            $quote .= ' “ '.$quotes->quotes.' ” ';
        }else{
            Artisan::call('inspire');
            $quote .= Artisan::output();
        }

        return view('Employee.Dashboard.index', [
            'quote' => $quote
        ]);
    }

    public function getAllHolidays(){
        $holidays = Holiday::get();
        return response()->json(['data' => $holidays]);
    }

    public function getBirthdays(){
        $carbon = new Carbon('now');
        $data = Profile::query()
            ->select('profiles.*')
            ->selectRaw('profiles.birthdate as employee_birthdate')
            ->get();
        return response()->json($data);
    }

}
