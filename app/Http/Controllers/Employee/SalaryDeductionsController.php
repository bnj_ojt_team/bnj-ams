<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SalaryDeduction;
use App\Models\SalaryLoan;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;

class SalaryDeductionsController extends Controller
{
    public function index(Request $request){
        if($request->wantsJson()){
            $salaryDeductions = SalaryDeduction::with('salaryLoan')->thisMonthDeductions()->whereHas('salaryLoan', function (Builder $query) {
                $query->where('user_id', auth()->user()->id);
            })->get();
            $collection = collect();
            $last_key = $salaryDeductions->last();
            foreach($salaryDeductions as $item){
                $collection->push([
                    'deduction' => $item->deduction,
                    'date' => $item->date,
                    'id' => $item->id,
                ]);
            }
            return response()->json(['data' => $collection]);
        }
        return view('Employee.SalaryDeductions.index');
    }

    public function add(Request $request){
        if($request->isMethod('post')){
            $pendingLoan = SalaryLoan::myLoan()->where('status', 'Pending')->first();
            $hasLoan = SalaryLoan::myLoan()->orderBy('id', 'desc')->first();
            if($hasLoan == null){
                return response()->json(['title' => ucwords('No salary loans detected.'), 'icon' => 'error'], 404);
            }
            if($pendingLoan != null){
                return response()->json(['title' => ucwords('You have a pending loan request. Wait for the admin to approve it.'), 'icon' => 'error'], 404);
            }
            $validator = $request->validate([
                'deduction' => ['bail', function($attribute, $value, $fail) use ($hasLoan){
                    if($value > $hasLoan->balance){
                        $fail(ucwords('Your Salary Deduction amount is greater than your current Salary Loan balance.'));
                    }
                }]
            ]);
            $request->merge(['date' => Carbon::now()]);
            $newSalaryLoan = SalaryLoan::create([
                'user_id' => auth()->user()->id,
                'remainder' => 0,
                'amount' => 0,
                'reason' => 'N/A',
                'status' => 'Deduction',
                'date' => $request->input('date'),
                'balance' => (getMyLatestLaon()->balance - intval($request->input('deduction'))),
            ]);
            $deduction = $newSalaryLoan->salaryDeduction()->create($request->all());
            if($deduction){
                return response()->json(['title' => ucwords('Salary deduction has been requested. '), 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => ucwords('Could not make a request, Please try again later. '), 'icon' => 'error'], 404);
            }
        }
    }

    public function edit($id = null, Request $request){
        $salaryDeduction = SalaryDeduction::findOrFail($id);
        if($request->isMethod('post')){

            $salaryDeductions = SalaryDeduction::with('salaryLoan')->whereHas('salaryLoan', function (Builder $query) {
                $query->where('user_id', auth()->user()->id);
            })->get();
            $last_key = $salaryDeductions->last();
            if($id != $last_key->id){
                return response()->json(['title' => ucwords('Not found!!!. '), 'icon' => 'error'], 404);
            }
            $hasLoan = SalaryLoan::myLoan()->orderBy('id', 'desc')->first();
            $validator = $request->validate([
                'deduction' => ['bail', function($attribute, $value, $fail) use ($hasLoan){
                    if($value > $hasLoan->balance){
                        $fail(ucwords('Your Salary Deduction amount is greater than your current Salary Loan balance.'));
                    }
                }]
            ]);
            $request->merge(['date' => Carbon::now()]);
            $newSalaryLoan = getMyLatestLaon()->update([
                'user_id' => auth()->user()->id,
                'remainder' => 0,
                'amount' => 0,
                'reason' => 'N/A',
                'status' => 'Deduction',
                'date' => $request->input('date'),
                'balance' => (getMySecondLatestLoan()->balance - intval($request->input('deduction'))),
            ]);
            $isUpdated = $salaryDeduction->update($request->all());
            if($isUpdated){
                return response()->json(['title' => ucwords('Salary deduction request has been updated. '), 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => ucwords('Could not update request, Please try again later. '), 'icon' => 'error'], 404);
            }
        }
        return response()->json($salaryDeduction);
    }

    public function delete($id = null, Request $request){
        $salaryDeduction = SalaryDeduction::findOrFail($id);
        SalaryLoan::findOrFail($salaryDeduction->salary_loan_id)->delete();
        if($salaryDeduction->delete()){
            return response()->json(['title' => ucwords('Salary deduction request has been deleted. '), 'icon' => 'success'], 200);
        }else{
            return response()->json(['title' => ucwords('Could not delete request, Please try again later. '), 'icon' => 'error'], 404);
        }
    }

}
