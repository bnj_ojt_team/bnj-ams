<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\SalaryDeduction;
use App\Models\SalaryLoan;
use Illuminate\Http\Request;
use App\Models\Attendance;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use App\Models\Holiday;
use App\Models\User;
use DB;

class PayrollController extends Controller
{
    const NON_WORKING_DAYS = ['Saturday', 'Sat', 'Sunday', 'Sun'];
    const HOLIDAYS_TYPE = ['Regular Holiday',
                            'Non Working Holiday',
                            'Special Non Working Holiday'];

    public function index(Request $request, $month = null, $year = null){
        if($request->wantsJson()){
            $dateFilter = (($month == null) ? date('F'):$month).' '.(($year == null) ? date('Y'):$year);

            $collection = collect();
            $workFromHomeCollection = collect();
            $nonWorkingDaysCollection = collect();
            $overtimeHoursCollection = collect(); // Added Overtime Collection

            $sNonWorkingHolidaysCollection = collect();
            $nonWorkingHolidaysCollection = collect();
            $regularHolidaysCollection = collect();

            $thisMonthFirstDay = new Carbon('first day of '.$dateFilter);
            $thisMonthLastDay = new Carbon('last day of '.$dateFilter);
            $attendances = Attendance::with('excuse', 'user.profile.employeeRole', 'accomplishment', 'accomplishment.homeAttendances')->myAttendance()->get();

            // Loop through this months dates
            while($thisMonthFirstDay->toDateString() <= $thisMonthLastDay->toDateString()){

                // Replaced this block of code
                $validateAttendance = $attendances->where('attendance_date', '=', $thisMonthFirstDay->toDateString())->first();
                if($validateAttendance != null){
                    if(!$validateAttendance->is_work_from_home){
                        $attendanceCondition = $attendances->contains(function ($item) use ($thisMonthFirstDay) {
                            return ($item['attendance_date'] == $thisMonthFirstDay->toDateString() && ($item['timein_am'] != null || $item['timeout_am'] != null || $item['timein_pm'] != null || $item['timeout_pm'] != null));
                        });
                    }else{
                        $attendanceCondition = true;
                    } 
                }else{
                    $attendanceCondition = false;
                }
                $attendance = ($attendanceCondition) ? $attendances->where('attendance_date', '=', $thisMonthFirstDay->toDateString())->first() : null;

                // If attendance is non working day
                if(in_array($thisMonthFirstDay->format('l'), self::NON_WORKING_DAYS) ||
                    in_array($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate())){
                    if($attendance != null){ // If employee has attendance
                        if(in_array($thisMonthFirstDay->format('l'), self::NON_WORKING_DAYS)){
                            $attendance->toArray();
                            $attendance->minus_hours = (getCompanyHours() - $attendance->attendance_total_hours);
                            $attendance->is_non_working = true;
                            // Added this if block of code
                            if($attendance->is_overtime){
                                $date = $attendance->attendance_date;
                                $employeeTimeOut = Carbon::parse($attendance->timeout_pm);
                                $companyTimeOut = Carbon::parse(getCompanySetting()->afternoon_time_out);
                                $hours = $employeeTimeOut->floatDiffInHours($companyTimeOut);
                                $overtimeHoursCollection->push([
                                    'id' => 'overtime-collection',
                                    'A' => auth()->user()->load(['profile'])->profile->firstname.' '.auth()->user()->load(['profile'])->profile->middlename.' '.auth()->user()->load(['profile'])->profile->lastname,
                                    'B' => auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->position.' '.auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->level,
                                    'C' => (new Carbon($date))->format('m/d/Y'),
                                    'D' => $companyTimeOut->format('h:i:s A'),
                                    'E' => $employeeTimeOut->format('h:i:s A'),
                                    'F' => $hours,
                                ]);
                            }
                            $collection->push($attendance);
                        }else if(in_array($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate())){
                            $holidayKey = array_search($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate());
                            $holidayType = Holiday::where('id', $holidayKey)->get('holiday_type')->first();
                            if($holidayType->holiday_type === 'Special Non Working Holiday'){
                                $attendance->toArray();
                                $attendance->minus_hours = $attendance->attendance_total_hours;
                                $attendance->is_non_working = true;
                                // Added this if block of code
                                if($attendance->is_overtime){
                                    $date = $attendance->attendance_date;
                                    $employeeTimeOut = Carbon::parse($attendance->timeout_pm);
                                    $companyTimeOut = Carbon::parse(getCompanySetting()->afternoon_time_out);
                                    $hours = $employeeTimeOut->floatDiffInHours($companyTimeOut);
                                    $overtimeHoursCollection->push([
                                        'id' => 'overtime-collection',
                                        'A' => auth()->user()->load(['profile'])->profile->firstname.' '.auth()->user()->load(['profile'])->profile->middlename.' '.auth()->user()->load(['profile'])->profile->lastname,
                                        'B' => auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->position.' '.auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->level,
                                        'C' => (new Carbon($date))->format('m/d/Y'),
                                        'D' => $companyTimeOut->format('h:i:s A'),
                                        'E' => $employeeTimeOut->format('h:i:s A'),
                                        'F' => $hours,
                                    ]);
                                }
                                $sNonWorkingHolidaysCollection->push($attendance);
                            }else if($holidayType->holiday_type === 'Non Working Holiday'){
                                $attendance->toArray();
                                $attendance->minus_hours = $attendance->attendance_total_hours;
                                $attendance->is_non_working = true;
                                // Added this if block of code
                                if($attendance->is_overtime){
                                    $date = $attendance->attendance_date;
                                    $employeeTimeOut = Carbon::parse($attendance->timeout_pm);
                                    $companyTimeOut = Carbon::parse(getCompanySetting()->afternoon_time_out);
                                    $hours = $employeeTimeOut->floatDiffInHours($companyTimeOut);
                                    $overtimeHoursCollection->push([
                                        'id' => 'overtime-collection',
                                        'A' => auth()->user()->load(['profile'])->profile->firstname.' '.auth()->user()->load(['profile'])->profile->middlename.' '.auth()->user()->load(['profile'])->profile->lastname,
                                        'B' => auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->position.' '.auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->level,
                                        'C' => (new Carbon($date))->format('m/d/Y'),
                                        'D' => $companyTimeOut->format('h:i:s A'),
                                        'E' => $employeeTimeOut->format('h:i:s A'),
                                        'F' => $hours,
                                    ]);
                                }
                                $nonWorkingHolidaysCollection->push($attendance);
                            }else if($holidayType->holiday_type === 'Regular Holiday'){
                                $attendance->toArray();
                                $attendance->minus_hours = $attendance->attendance_total_hours;
                                $attendance->is_non_working = true;
                                // Added this if block of code
                                if($attendance->is_overtime){
                                    $date = $attendance->attendance_date;
                                    $employeeTimeOut = Carbon::parse($attendance->timeout_pm);
                                    $companyTimeOut = Carbon::parse(getCompanySetting()->afternoon_time_out);
                                    $hours = $employeeTimeOut->floatDiffInHours($companyTimeOut);
                                    $overtimeHoursCollection->push([
                                        'id' => 'overtime-collection',
                                        'A' => auth()->user()->load(['profile'])->profile->firstname.' '.auth()->user()->load(['profile'])->profile->middlename.' '.auth()->user()->load(['profile'])->profile->lastname,
                                        'B' => auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->position.' '.auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->level,
                                        'C' => (new Carbon($date))->format('m/d/Y'),
                                        'D' => $companyTimeOut->format('h:i:s A'),
                                        'E' => $employeeTimeOut->format('h:i:s A'),
                                        'F' => $hours,
                                    ]);
                                }
                                $regularHolidaysCollection->push($attendance);
                            }
                        }
                        
                    }
                }else{ // If attendance is working days 
                    if($attendance != null){ // If employee has attendance
                        $attendance->toArray();
                        $attendance->minus_hours = (getCompanyHours() - $attendance->attendance_total_hours);
                        $attendance->is_non_working = false;
                        // Added this if block of code
                        if($attendance->is_overtime){
                            $date = $attendance->attendance_date;
                            $employeeTimeOut = Carbon::parse($attendance->timeout_pm);
                            $companyTimeOut = Carbon::parse(getCompanySetting()->afternoon_time_out);
                            $hours = $employeeTimeOut->floatDiffInHours($companyTimeOut);
                            $overtimeHoursCollection->push([
                                'id' => 'overtime-collection',
                               'A' => auth()->user()->load(['profile'])->profile->firstname.' '.auth()->user()->load(['profile'])->profile->middlename.' '.auth()->user()->load(['profile'])->profile->lastname,
                                'B' => auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->position.' '.auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->level,
                                'C' => (new Carbon($date))->format('m/d/Y'),
                                'D' => $companyTimeOut->format('h:i:s A'),
                                'E' => $employeeTimeOut->format('h:i:s A'),
                                'F' => $hours,
                            ]);
                        }
                        $collection->push($attendance);
                    }else{ // If employee has no attendance for this day, meaning absent
                        $collection->push([
                            "id" => null,
                            "user_id" => null,
                            "attendance_date" => $thisMonthFirstDay->toDateString(),
                            "timein_am" => null,
                            "timeout_am" => null,
                            "timein_pm" => null,
                            "timeout_pm" => null,
                            "attendance_total_hours" => null,
                            "is_holiday" => null,
                            "is_present" => null,
                            "is_late" => null,
                            "is_work_from_home" => null,
                            "minus_hours" => getCompanyHours(),
                            "is_non_working" => false,
                            "excuse" => [
                                "attendance_id" => null,
                                "timein_am" => null,
                                "timeout_am" => null,
                                "timein_pm" => null,
                                "timeout_pm" => null,
                                "excuse_total_hours" => null,
                            ],
                            "user" => [
                                "profile" => [
                                    "firstname" => auth()->user()->load(['profile'])->profile->firstname,
                                    "middlename" => auth()->user()->load(['profile'])->profile->middlename,
                                    "lastname" => auth()->user()->load(['profile'])->profile->lastname,
                                    "employee_role" => [
                                        "position" => auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->position,
                                        "level" => auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->level,
                                    ]
                                ]
                            ]
                        ]);
                    }
                }

                $attendance = $attendances->where('attendance_date', '=', $thisMonthFirstDay->toDateString())->first();
                // Push work from home attendances into collection if there are work from home attendances
                if($attendance != null){
                    if($attendance->is_work_from_home === 1){
                        if($attendance->accomplishment->status == "Approved"){ // Added this line 
                            foreach ($attendance->accomplishment->homeAttendances as $value) {
                                $workFromHomeCollection->push([
                                    "id" => $attendance->id,
                                    "user_id" => $attendance->user_id,
                                    "attendance_date" => $thisMonthFirstDay->toDateString(),
                                    "timein_am" => $value->time_in,
                                    "timeout_am" => $value->time_out,
                                    "timein_pm" => null,
                                    "timeout_pm" => null,
                                    "attendance_total_hours" => Carbon::parse($value->time_in)->floatDiffInHours(Carbon::parse($value->time_out)),
                                    "is_holiday" => null,
                                    "is_present" => null,
                                    "is_late" => null,
                                    "is_work_from_home" => $attendance->is_work_from_home,
                                    "is_non_working" => false,
                                    "excuse" => [
                                        "attendance_id" => null,
                                        "timein_am" => null,
                                        "timeout_am" => null,
                                        "timein_pm" => null,
                                        "timeout_pm" => null,
                                        "excuse_total_hours" => null,
                                    ],
                                    "user" => [
                                        "profile" => [
                                            "firstname" => auth()->user()->load(['profile'])->profile->firstname,
                                            "middlename" => auth()->user()->load(['profile'])->profile->middlename,
                                            "lastname" => auth()->user()->load(['profile'])->profile->lastname,
                                            "employee_role" => [
                                                "position" => auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->position,
                                                "level" => auth()->user()->load(['profile.employeeRole'])->profile->employeeRole->level,
                                            ]
                                        ]
                                    ]
                                ]);
                            }
                        }
                    }
                }
                // Push work from home attendances into collection if there are work from home attendances end

                $thisMonthFirstDay->addDay();
            }
            // Loop through this months dates end

            $collection->push(['id' => "empty"]);
            // Merge non working days attendances with attendances collection if there are datas
            if($sNonWorkingHolidaysCollection->count() > 0){
                $collection->push([
                    "id" => '',
                    "text" => 'Special Non Working Holidays'
                ]);
                $collection = $collection->merge($sNonWorkingHolidaysCollection);
            }

            if($nonWorkingHolidaysCollection->count() > 0){
                $collection->push([
                    "id" => '',
                    "text" => 'Non Working Holidays'
                ]);
                $collection = $collection->merge($nonWorkingHolidaysCollection);
            }

            if($regularHolidaysCollection->count() > 0){
                $collection->push([
                    "id" => '',
                    "text" => 'Regular Holidays'
                ]);
                $collection = $collection->merge($regularHolidaysCollection);
            }
            // Merge non working days attendances with attendances collection if there are datas end

            $collection->push(['id' => "empty"]);
            // Merge work from home days attendances with attendances collection if there are datas
            if($workFromHomeCollection->count() > 0){
                $collection->push([
                    "id" => '',
                    "text" => 'Work From Home Hours'
                ]);

                // Append new data for header
                $collection->push([
                    "id" => 'wfh',
                    "A" => "Name",
                    "B" => "Position",
                    "C" => "Date",
                    "D" => "Time In",
                    "E" => "Time Out",
                ]);
                // Append new data for header end

                $collection = $collection->merge($workFromHomeCollection);
            }
            // Merge work from home days attendances with attendances collection if there are datas end
            
            // added this block of code appending overtime hours
            if ($overtimeHoursCollection->count() > 0) {
                $collection->push(['id' => 'space']);
                $collection->push([
                    "id" => 'overtime-merge',
                    "text" => "Overtime Hours"
                ]);

                $collection->push([
                    "id" => 'overtime',
                    "A" => "Date",
                    "B" => "Start Hour",
                    "C" => "End Hour",
                    "D" => "Total Hours",
                ]);

                $collection = $collection->merge($overtimeHoursCollection);
            }

            // Append three new data with empty id
            $collection->push(['id' => "empty"]);
            $collection->push(['id' => "empty"]);
            $collection->push(['id' => "empty"]);
            $collection->push(['id' => "contribution"]);
            $collection->push(['id' => "contribution"]);
            // Append new line for sss and pagibig
            // Add this new line of code
            $appendContributionsCount = count($this->getContributionRate($month, $year)->getData()) - 2;
            for ($i = 0; $i < $appendContributionsCount; $i++) { 
                $collection->push(['id' => "contribution"]);
            }
            // Append three new data with empty id end

            // Added this new block of code
            
            // dd(self::mergeSalaryDeductions()->count());
            if(self::mergeSalaryDeductions()->count() > 0){
                $length = count($this->getContributionRate($month, $year)->getData());
                $salaryWithContribution = floatval(str_replace(',', '', $this->getContributionRate($month, $year)->getData()[$length - 1][5]));
                $collection->push(['id' => 'space']);
                $collection->push([
                    'id' => 'deductions-header',
                    'A' => 'Date',
                    'B' => 'Deduction Amount',
                    'C' => 'Total Salary',
                ]);
                foreach (self::mergeSalaryDeductions() as $item) {
                    // dd($item->salaryDeduction);
                    $salaryWithContribution = ($salaryWithContribution) - ($item->salaryDeduction->deduction);
                    $collection->push([
                        'id' => 'deductions',
                        'A' => (new Carbon($item->salaryDeduction->date))->format('m/d/Y'),
                        'B' => excelFormat($item->salaryDeduction->deduction),
                        'C' => excelFormat($salaryWithContribution),
                    ]);
                }
            }
            

            return response()->json(['data' => $collection]);
        }
        // dd(count($this->getContributionRate()->getData()) - 2);
        return view('Employee.Payroll.index');
    }

    // Salary Contributions Rate Included in this function
    public function getEstimatedSalary($month = null, $year = null){
        $collection = collect();
        $dateFilter = (($month == null) ? date('F'):$month).' '.(($year == null) ? date('Y'):$year);

        // Instantiate new starting and ending date this month
        $thisMonthFirstDay = new Carbon('first day of '.$dateFilter);
        $startDate = new Carbon('first day of '.$dateFilter);
        $thisMonthLastDay = new Carbon('last day of '.$dateFilter);
        $endDate = new Carbon('last day of '.$dateFilter);
        // Get my attendances
        $attendances = Attendance::with('excuse', 'user.profile.employeeRole', 'accomplishment', 'accomplishment.homeAttendances')->myAttendance()->get();
        // Instantiate new starting and ending date this month end

        $collection->push(['', '', '', '', '', '', '', '', '', '', ]);

        $collection->push([
            'Total',
            'Basic Salary',
            'Days',
            '1 Day',
            '1 Hour',
            'Holiday Work',
            'Holiday Salary',
            'OT/AB',
            '',
            'Salary Without Contribution/s',
        ]);

        // Get hours every attendance work days 
        $totalHours = 0;
        $totalHolidayHours = 0;
        $holidaySalary = 0;
        $basicSalary = getMyBasicSalary(); // Basic Salary
        $overtimeSalary = 0; // Added this overtime salary line

        // Get count of working days in this month
        $holidays = [];
        foreach ($this->getHolidaysDate() as $date) {
            array_push($holidays, Carbon::parse($date));
        }
        $totalWorkingDays = Carbon::parse($startDate->format('Y-m-d'))->diffInDaysFiltered(function (Carbon $date) use ($holidays) {
            return $date->isWeekday() && !in_array($date, $holidays);
        }, Carbon::parse($endDate->format('Y-m-d'))->addDay());
        // Get count of working days in this month end


        // Get per day total salary
        $perDaySalary = round(($basicSalary/$totalWorkingDays), 4);
        // Get per day total salary end

        // Get per hour total salary
        $perHourSalary = round(($perDaySalary/getCompanyHours()), 4);
        // Get per hour total salary end

        // Loop through this months dates
        while($thisMonthFirstDay->toDateString() <= $thisMonthLastDay->toDateString()){
            $attendance = $attendances->where('attendance_date', '=', $thisMonthFirstDay->toDateString())->first();
            // If attendance is non working day
            if(in_array($thisMonthFirstDay->format('l'), self::NON_WORKING_DAYS) || // If sat or sun
                in_array($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate())){ // If non working holidays
                if($attendance != null){ // If employee has attendance
                    $hours = 0;
                    if(in_array($thisMonthFirstDay->format('l'), self::NON_WORKING_DAYS)){

                        if($attendance->is_work_from_home === 1){ // If non working days is work from home
                            if($attendance->accomplishment->status == "Approved"){ // Added this line 
                                foreach ($attendance->accomplishment->homeAttendances as $value) {
                                    $time_in = Carbon::parse($value->time_in);
                                    $time_out = Carbon::parse($value->time_out);
                                    $hours += ($time_in->floatDiffInHours($time_out));
                                }
                            }
                                
                        }
                        if($attendance->attendance_total_hours != null){ // If not work from home
                            $hours += $attendance->attendance_total_hours;
                        }
                            
                        // Holiday Salary
                        $holidaySalary += round(($perHourSalary*$hours) * 2, 4);
                        // dd($thisMonthFirstDay->format('l'));
                        // Holiday Salary end
                        // Added Overtime salary
                        if($attendance->is_overtime){
                            $employeeTimeOut = Carbon::parse($attendance->timeout_pm);
                            $companyTimeOut = Carbon::parse(getCompanySetting()->afternoon_time_out);
                            $overtimeSalary += round(($employeeTimeOut->floatDiffInHours($companyTimeOut)) * $perHourSalary * 2, 0);
                        }

                    }else if(array_search($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate())){
                        $holidayKey = array_search($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate());
                        $holidayRate = Holiday::where('id', $holidayKey)->get('rate')->first();

                        if($attendance->is_work_from_home === 1){ // If non working days is work from home
                            if($attendance->accomplishment->status == "Approved"){ // Added this line 
                                foreach ($attendance->accomplishment->homeAttendances as $value) {
                                    $time_in = Carbon::parse($value->time_in);
                                    $time_out = Carbon::parse($value->time_out);
                                    $hours += ($time_in->floatDiffInHours($time_out));
                                }
                            }
                                
                        }
                        if($attendance->attendance_total_hours != null){ // If not work from home
                            $hours += $attendance->attendance_total_hours;
                        }

                        // Holiday Salary
                        // var_dump(round(($perHourSalary*$hours*($holidayRate->rate/100)), 4));
                        $holidaySalary += round(($perHourSalary*$hours*($holidayRate->rate/100)), 4);
                        // var_dump($holidaySalary);
                        // Holiday Salary end
                        // Added Overtime salary
                        if($attendance->is_overtime){
                            $employeeTimeOut = Carbon::parse($attendance->timeout_pm);
                            $companyTimeOut = Carbon::parse(getCompanySetting()->afternoon_time_out);
                            $overtimeSalary += round(($employeeTimeOut->floatDiffInHours($companyTimeOut)) * $perHourSalary*($holidayRate->rate/100), 0);
                        }
                    }

                    $totalHolidayHours += $hours;
                }
            }else{
                if($attendance != null){ // If employee has attendance meaning present
                    if($attendance->is_work_from_home === 1){ // If working days is work from home
                        if($attendance->accomplishment->status == "Approved"){ // Added this line 
                            foreach ($attendance->accomplishment->homeAttendances as $value) {
                                $time_in = Carbon::parse($value->time_in);
                                $time_out = Carbon::parse($value->time_out);
                                $totalHours += ($time_in->floatDiffInHours($time_out));
                            }
                        }
                    }else{ // If not work from home
                        $totalHours -= (getCompanyHours() - $attendance->attendance_total_hours);

                        // Added Overtime salary
                        if($attendance->is_overtime){
                            $employeeTimeOut = Carbon::parse($attendance->timeout_pm);
                            $companyTimeOut = Carbon::parse(getCompanySetting()->afternoon_time_out);
                            $overtimeSalary += round(($employeeTimeOut->floatDiffInHours($companyTimeOut)) * $perHourSalary, 0);
                        }
                    }
                }else{ // Else meaning absent
                    $totalHours -= getCompanyHours();
                }
            }
            $thisMonthFirstDay->addDay();
        }
        // Loop through this months dates end
        // Get hours every attendance work days end
        // Overtime or absent
        $OtAb = (round($totalHours*$perHourSalary, 0) + round($overtimeSalary, 0));
        // Overtime or absent end

        // Get total salary 
        $salary = round(($basicSalary+$holidaySalary+($OtAb)), 0);
        // Get total salary end

        $collection->push([
            $totalHours,
            excelFormat($basicSalary),
            $totalWorkingDays,
            excelFormat($perDaySalary),
            excelFormat($perHourSalary),
            $totalHolidayHours,
            excelFormat($holidaySalary),
            excelFormat($OtAb),
            '',
            excelFormat($salary),
        ]);

        return response()->json($collection);
    }

    public function getContributionRate($month = null, $year = null){
        $collection = collect();
        if(self::hasContributions(auth()->user()->id)[0]){
            $collection->push(['', '', '', '', '', '',]);

            $collection->push([
                'Contribution Name',
                'Employee Rate',
                'Employer Rate',
                'Total Rate',
                '',
                'Salary With Contribution/s',
            ]);
            $estimatedSalary = floatval(str_replace(',', '', $this->getEstimatedSalary($month, $year)->getData()[2][9]));
            foreach ($this->getUserContributionRate() as $item) {
                $estimatedSalary -= (getMyBasicSalary() * $item->employee_rate);
                
                $collection->push([
                    $item->name,
                    excelFormat((getMyBasicSalary() * $item->employee_rate)),
                    excelFormat((getMyBasicSalary() * $item->employer_rate)),
                    excelFormat((getMyBasicSalary() * ($item->employee_rate + $item->employer_rate))),
                    '',
                    excelFormat($estimatedSalary),
                ]);
            }

            $collection->push([
                temporaryPagibigRate()['name'],
                excelFormat(temporaryPagibigRate()['employee_rate']),
                excelFormat(temporaryPagibigRate()['employer_rate']),
                excelFormat(temporaryPagibigRate()['total_rate']),
                '',
                excelFormat($estimatedSalary - temporaryPagibigRate()['employee_rate']),
            ]);
        }


        return response()->json($collection);
    }

    private function getUserContributionRate($userId = null){
        if($userId == null){
            $userContribution = User::with('profile.employeeRole.contributions')->where('id', auth()->user()->id)->first();
        }else{
            $userContribution = User::with('profile.employeeRole.contributions')->where('id', $userId)->first();
        }
        
        return $userContribution->profile->employeeRole->contributions;
    }

    private function hasContributions($userId = null){
        return DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->where('users.id', $userId)
            ->pluck('profiles.has_contributions');
    }

    private function mergeSalaryDeductions($userId = null){
        if($userId == null){
            $salaryLoans = SalaryLoan::with(['SalaryDeduction'])
                ->where('user_id', auth()->user()->id)
                ->where('status', 'Deduction')
                ->thisMonthDeductions()->get();
        }else{
            $salaryLoans = SalaryLoan::with(['SalaryDeduction'])
                ->where('user_id', $userId)
                ->where('status', 'Deduction')
                ->thisMonthDeductions()->get();
        }
        // return $salaryLoans[0]->salaryDeduction;
        return $salaryLoans;
    }

    // All protected functions has no routes
    protected function getHolidaysDate(){
        $holidays = Holiday::get()->pluck('holiday_date', 'id')->toArray();
        
        return $holidays;
    }

}
