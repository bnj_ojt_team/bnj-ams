<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;

class UsersController extends Controller
{
    // Account Settings Function
    public function accountSetting(){
        $user = User::findOrFail(auth()->user()->id);
        return view('Manager.Users.account-setting', [
            'user' => $user
        ]);
    }

    // Edit user
    public function edit(Request $request){
        $user = User::findOrFail(auth()->user()->id);
        if($request->isMethod('post')){
            // dd($request->all());
            if($request->has('password')){
                $validator = $request->validate([
                    'current-password' => ['bail', 'required', function ($attribute, $value, $fail) use ($user) {
                        if (!\Hash::check($value, $user->password)) {
                            return $fail(__('The current password is incorrect.'));
                        }
                    }],
                    'password' => ['bail', 'required', 'min:8', function ($attribute, $value, $fail) use ($user) {
                        if (\Hash::check($value, $user->password)) {
                            return $fail(__('New password should not be the same as your current password.'));
                        }
                    }],
                    'confirm-password' => ['bail', 'required', 'same:password'],
                ],[
                    'password.required' => ucfirst('New password is required'),
                ]);
            }else{
                $validator = $request->validate([
                    'username' => ['bail', 'required', Rule::unique('users','username')->ignore($user->id)],
                    'email' => ['bail', 'required', Rule::unique('users','email')->ignore($user->id), 'email'],
                    'file' => ['bail', 'nullable', 'image'],
                ]);
            }
            $user->update($request->all());
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filepath = storage_path('app/public/avatars');
                $currentImage = $filepath.'/'.$user->image;
                if(File::isFile($currentImage)){
                    File::delete($currentImage);
                }
                $filename = uniqid().uniqid().uniqid().'.'.$file->getClientOriginalExtension();
                if(!File::isDirectory($filepath)){
                    File::makeDirectory($filepath);
                }
                if($file){
                    $file->move($filepath, $filename);
                }
                $user->image = $filename;
            }
            if($user->save()){
                return response()->json(['title' => 'User Account has updated. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'User Account could not be updated. ', 'icon' => 'error'], 404);
            }
        }
        return response()->json($user);
    }
}
