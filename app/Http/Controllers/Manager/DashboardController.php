<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quote;
use App\Models\Holiday;
use App\Models\Profile;
use App\Models\Attendance;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index(){

        $quote = '';

        $quotes = Quote::query()->where('is_active','=','1')->first();

        if(!empty($quotes)){
            $quote .= ' “ '.$quotes->quotes.' ” ';
        }else{
            Artisan::call('inspire');
            $quote .= Artisan::output();
        }

        $totalemployees = Profile::query()->whereHas('user',function($query){
            return $query->where('users.role','like','%employee%')
                ->orWhere('users.role','like','%manager%');
        })->count();

        $totalPresent = Attendance::whereDate('attendance_date', '=', Carbon::today()->toDateString())
            ->where(function ($query) {
                $query->whereNotNull('timein_am')
                    ->orWhereNotNull('timeout_am')
                    ->orWhereNotNull('timein_pm')
                    ->orWhereNotNull('timeout_pm');
            })->get();

        return view('Manager.Dashboard.index', [
            'quote' => $quote,
            'totalemployees' => $totalemployees,
            'totalPresent' => $totalPresent,
        ]);
    }
    public function getAllHolidays(){
        $holidays = Holiday::get();
        return response()->json(['data' => $holidays]);
    }
    
}
