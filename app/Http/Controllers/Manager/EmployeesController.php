<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\EmployeeRole;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;
use App\Models\Profile;
use Carbon\Carbon;

class EmployeesController extends Controller
{
    public function index(Request $request){
        $employeeRoles = EmployeeRole::get()->map(function($row){
            return [
                'key' => $row->id,
                'value' => $row->position.' '.$row->level
            ];
        })->pluck('value', 'key');

        if($request->wantsJson()){
            $employees = User::with('profile', 'profile.employeeRole')
                                ->where('role', 'LIKE', '%employee%')
                                ->orWhere('role', 'LIKE', '%manager%')->get();
            return response()->json(['data' => $employees]);
        }
        return view('Manager.Employees.index', [
            'employeeRoles' => $employeeRoles
        ]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){
            $employment_status = $request->profile;
            $employment_status['employment_status'] = 'Trainee';
            $request->merge(['profile' => $employment_status]);
            $validator = $request->validate([
                'username' => ['bail', 'required','max:255',Rule::unique('users','username')],
                'email' => ['bail', 'required','max:255',Rule::unique('users','email'),'email'],
                'password' => ['bail', 'required','max:255'],
                'confirm-password' => ['bail', 'required','same:password'],
                'profile.firstname' => ['bail', 'required','max:255'],
                'profile.middlename' => ['bail', 'required','max:255'],
                'profile.lastname' => ['bail', 'required','max:255'],
                'profile.birthdate' => ['bail', 'required','date'],
                'profile.contact_number' => ['bail', 'required','max:255','regex:/^(09|\+639)[\d-]{9,12}/',Rule::unique('contacts','phone_number')],
                'profile.date_employed' => ['bail', 'required','date'],
                'profile.employment_status' => ['bail', 'required',Rule::in([ucwords('Regular'),ucwords('retired'), ucwords('trainee')])],
                'profile.address' => ['bail', 'required','max:65535'],
                'contact.fullname' => ['bail', 'required','max:255'],
                'contact.relation' => ['bail', 'required','max:255'],
                'contact.phone_number' => ['bail', 'required','max:255','regex:/^(09|\+639)[\d-]{9,12}/',Rule::unique('contacts','phone_number')],
                'contact.address' => ['bail', 'required','max:65535'],
                'role' => ['required','max:255', Rule::in(['manager', 'employee'])]
            ],
            [
                'username.required' => ucwords('username must not be left empty!'),
                'username.unique' => ucwords('username already exists!'),
                'username.max' => ucwords('username characters exceeded to 255!'),
                'email.required' => ucwords('email must not be left empty!'),
                'email.unique' => ucwords('email alreay exists!'),
                'role.required' => ucwords('please select role!'),
                'role.in' => ucwords('role must be manager or employee only!'),
                'email.max' => ucwords('email characters exceeded to 255!'),
                'email.email' => ucwords('please enter valid email!'),
                'password.required' => ucwords('password must not be left empty!'),
                'password.max' => ucwords('password characters exceeded to 255!'),
                'confirm-password.same' => ucwords('password confirmation not matched!'),
                'confirm-password.required' => ucwords('password confirmation must not empty!'),
                'profile.firstname.required' => ucwords('first name must not be empty!'),
                'profile.firstname.max' => ucwords('first name exceed at 255 characters!'),
                'profile.middlename.required' => ucwords('middle name must not be empty!'),
                'profile.middlename.max' => ucwords('middle name exceed at 255 characters!'),
                'profile.lastname.required' => ucwords('last name must not be empty!'),
                'profile.lastname.max' => ucwords('last name exceed at 255 characters!'),
                'profile.birthdate.required' => ucwords('birthdate must not be empty!'),
                'profile.birthdate.date' => ucwords('invalid date on birthdate!'),
                'profile.date_employed.required' => ucwords('date employed must not be empty!'),
                'profile.date_employed.date' => ucwords('invalid date on date employed!'),
                'profile.employment_status.required' => ucwords('employment status must not be empty!'),
                'profile.employment_status.in' => ucwords('inavlid choice in employment status!'),
                'profile.address.required' => ucwords('profile address must not empty!'),
                'profile.address.max' => ucwords('profile address character limit reached!'),
                'contact.fullname' => ucwords('fullname must not be empty!'),
                'contact.max' => ucwords('fullname characters exceeded!'),
                'contact.fullname.required' => ucwords('fullname must not be empty!'),
                'contact.fullname.max' => ucwords('fullname characters exceeded!'),
                'contact.relation.required' => ucwords('relation must not be empty!'),
                'contact.relation.max' => ucwords('relation characters exceeded!'),
                'contact.phone_number.required' => ucwords('fullname must not be empty!'),
                'contact.phone_number.max' => ucwords('fullname characters exceeded!'),
                'contact.phone_number.regex' => ucwords('phone number must start with 09 or +639 and followed by 9 digits'),
                'contact.phone_number.unique' => ucwords('phone number already used'),
                'contact.address.required' => ucwords('contact address must not empty!'),
                'contact.address.max' => ucwords('contact address character limit reached!'),
            ]);
            $employee = User::create($request->all());
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filepath = storage_path('app/public/avatars');
                $filename = uniqid().uniqid().uniqid().'.'.$file->getClientOriginalExtension();
                if(!File::isDirectory($filepath)){
                    File::makeDirectory($filepath);
                }
                if($file){
                    $file->move($filepath, $filename);
                }
                $employee->image = $filename;
            }
            $employeeContact = $employee->contact()->create($request->input('contact'));
            $employeeProfile = $employee->profile()->create($request->input('profile'));

            if($employee->save() && $employeeContact->save() && $employeeProfile->save()){
                return response()->json(['title' => 'Employee Profile Has been Saved. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Employee Profile could not be Saved. ', 'icon' => 'error'], 404);
            }
        }
    }

    public function edit($id = null, Request $request){
        $employee = User::with('profile', 'profile.employeeRole', 'contact')->findOrFail($id);
        if($request->isMethod('post')){
            $validator = $request->validate([
                'username' => ['required','max:255',Rule::unique('users','username')->ignore($employee->id)],
                'email' => ['required','max:255',Rule::unique('users','email')->ignore($employee->id),'email'],
                // 'password' => ['max:255'],
                // 'confirm-password' => ['same:password'],
                'profile.firstname' => ['required','max:255'],
                'profile.middlename' => ['required','max:255'],
                'profile.lastname' => ['required','max:255'],
                'profile.birthdate' => ['required','date'],
                'profile.date_employed' => ['required','date'],
                'profile.employee_role_id' => ['required'],
                'profile.employment_status' => ['required',Rule::in([ucwords('Regular'),ucwords('retired')])],
                'profile.address' => ['required','max:65535'],
                'contact.fullname' => ['required','max:255'],
                'contact.relation' => ['required','max:255'],
                'contact.phone_number' => ['required','max:255'],
                'contact.address' => ['required','max:65535'],
            ], [
                'username.required' => ucwords('username must not be left empty!'),
                'username.unique' => ucwords('username already exists!'),
                'username.max' => ucwords('username characters exceeded to 255!'),
                'email.required' => ucwords('email must not be left empty!'),
                'email.unique' => ucwords('email alreay exists!'),
                'email.max' => ucwords('email characters exceeded to 255!'),
                'email.email' => ucwords('please enter valid email!'),
                // 'password.max' => ucwords('password characters exceeded to 255!'),
                // 'confirm-password.same' => ucwords('password confirmation not matched!'),
                'profile.firstname.required' => ucwords('first name must not be empty!'),
                'profile.employee_role_id.required' => ucwords('Please select atleast one Position!'),
                'profile.firstname.max' => ucwords('first name exceed at 255 characters!'),
                'profile.middlename.required' => ucwords('middle name must not be empty!'),
                'profile.middlename.max' => ucwords('middle name exceed at 255 characters!'),
                'profile.lastname.required' => ucwords('last name must not be empty!'),
                'profile.lastname.max' => ucwords('last name exceed at 255 characters!'),
                'profile.birthdate.required' => ucwords('birthdate must not be empty!'),
                'profile.birthdate.date' => ucwords('invalid date on birthdate!'),
                'profile.date_employed.required' => ucwords('date employed must not be empty!'),
                'profile.date_employed.date' => ucwords('invalid date on date employed!'),
                'profile.employment_status.required' => ucwords('employment status must not be empty!'),
                'profile.employment_status.in' => ucwords('inavlid choice in employment status!'),
                'profile.address.required' => ucwords('profile address must not empty!'),
                'profile.address.max' => ucwords('profile address character limit reached!'),
                'contact.fullname' => ucwords('fullname must not be empty!'),
                'contact.max' => ucwords('fullname characters exceeded!'),
                'contact.fullname.required' => ucwords('fullname must not be empty!'),
                'contact.fullname.max' => ucwords('fullname characters exceeded!'),
                'contact.relation.required' => ucwords('relation must not be empty!'),
                'contact.relation.max' => ucwords('relation characters exceeded!'),
                'contact.phone_number.required' => ucwords('fullname must not be empty!'),
                'contact.phone_number.max' => ucwords('fullname characters exceeded!'),
                'contact.address.required' => ucwords('contact address must not empty!'),
                'contact.address.max' => ucwords('contact address character limit reached!'),
            ]);
            $employee->update($request->all());
            $employee->profile()->update($request->input('profile'));
            $employee->contact()->update($request->input('contact'));
            if($employee->save() && $employee->push()){
                return response()->json(['title' => 'Employee Profile Has been Updated. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Employee Profile could not be Updated. ', 'icon' => 'error'], 404);
            }
        }
        return response()->json($employee);
    }
    public function getBirthdays(){
        $carbon = new Carbon('now');
        $data = Profile::query()
            ->select('profiles.*')
            ->selectRaw('profiles.birthdate as employee_birthdate')
            ->get();
        return response()->json($data);
    }
}
