<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SalaryLoan;
use Carbon\Carbon;

class SalaryLoansController extends Controller
{
    public function index(Request $request){
        // dd(getMyBasicSalary());
        if($request->wantsJson()){
            $loans = SalaryLoan::with('salaryDeduction')->myLoan()->get();
            return response()->json(['data' => $loans]);
        }
        return view('Manager.SalaryLoans.index');
    }

    public function add(Request $request){
        if($request->isMethod('post')){
            $pendingLoan = SalaryLoan::myLoan()->where('status', 'Pending')->first();
            if($pendingLoan != null){
                return response()->json(['title' => ucwords('You have a pending loan request. You can edit it if you want.'), 'icon' => 'error'], 404);
            }
            $validator = $request->validate([
                'amount' => ['bail', function($attribute, $value, $fail){
                    if($value > getMyBasicSalary()){
                        $fail(ucwords('your maximum loan amount is '. money(getMyBasicSalary()) .' PHP.'));
                    }
                }],
            ]);
            $amount = $request->input('amount');
            // Does not exist in request balance, remainder, status and date
            $latestLoan = SalaryLoan::myLoan()->latest('id')->first();

            $request->merge(['user_id' => auth()->user()->id]);
            $request->merge(['status' => 'Pending']);
            $request->merge(['date' => Carbon::now()]);
            $request->merge(['remainder' => 0]);
            if($latestLoan != null){
                // if has previous balance
                $prevBalance = $latestLoan->balance;
                $request->merge(['balance' => (($amount + $prevBalance))]);
            }else if($latestLoan == null){
                // If does not have previous loan
                $request->merge(['balance' => $amount]);
            }

            if(SalaryLoan::create($request->all())){
                return response()->json(['title' => ucwords('Loan has been requested.'), 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => ucwords('Could not make a request at the moment. Please try again later.'), 'icon' => 'error'], 404);
            }
        }
    }

    public function edit($id = null, Request $request){
        $loan = SalaryLoan::findOrFail($id);
        if($request->isMethod('post')){
            $validator = $request->validate([
                'amount' => ['bail', function($attribute, $value, $fail){
                    if($value > getMyBasicSalary()){
                        $fail(ucwords('your maximum loan amount is '. money(getMyBasicSalary()) .' PHP.'));
                    }
                }],
            ]);
            if($loan->status != 'Pending'){
                return response()->json(['title' => ucwords('Not found!!!. '), 'icon' => 'error'], 404);
            }
            $amount = $request->input('amount');
            // Does not exist in request: balance, remainder, status and date
            $latestLoan = SalaryLoan::myLoan()->latest('id')->skip(1)->take(1)->first();
            $request->merge(['user_id' => auth()->user()->id]);
            $request->merge(['status' => 'Pending']);
            $request->merge(['date' => Carbon::now()]);
            $request->merge(['remainder' => 0]);
            if($latestLoan != null){
                // if has previous balance
                $prevBalance = $latestLoan->balance;
                $request->merge(['balance' => (($amount + $prevBalance))]);
            }else if($latestLoan == null){
                // If does not have previous loan
                $request->merge(['balance' => $amount]);
            }

            if($loan->update($request->all())){
                return response()->json(['title' => ucwords('Loan has been requested.'), 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => ucwords('Could not make a request at the moment. Please try again later.'), 'icon' => 'error'], 404);
            }
        }
        return response()->json($loan);
    }

    public function delete($id = null){
        $loan = SalaryLoan::findOrFail($id);
        if($loan->delete()){
            return response()->json(['title' => ucwords('Loan has been deleted. '), 'icon' => 'success'], 200);
        }else{
            return response()->json(['title' => ucwords('Error deleting Loan. '), 'icon' => 'error'], 404);
        }
    }
}
