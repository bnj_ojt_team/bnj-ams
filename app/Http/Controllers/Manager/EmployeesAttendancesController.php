<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Attendance;
use App\Models\Profile;
use Illuminate\Http\Request;
use App\Models\Reason;

class EmployeesAttendancesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $profiles = Profile::query()->get()->map(function($row){
            return ['value' => $row->firstname, 'key' => $row->id];
        })->pluck('value','key');
        return view('Manager.EmployeesAttendances.index',compact('profiles'));
    }

    public function getAttendances(){
        $data = Attendance::query()
        ->with(['User.Profile', 'Excuse', 'Accomplishment.homeAttendances','Reason'])
        ->whereNotNull('timein_am')
        ->orWhereNotNull('timeout_am')
        ->orWhereNotNull('timein_pm')
        ->orWhereNotNull('timeout_pm')
        ->get();
        return response()->json(['data'=>$data]);
    }

    public function accomplishment($id = null){
        $attendance = Attendance::query()->with(['Accomplishment', 'User.Profile'])->findOrFail($id);
        if(empty($attendance->accomplishment)){
            abort(404,'accomplishment not found!');
        }

        return view('Manager.EmployeesAttendances.accomplishment',compact('attendance'));
    }

    public function reason($id = null){
        $reason = Reason::query()->with(['Attendance.User.Profile'])->findOrFail($id);

        if(empty($reason)){
            abort(404,'reason not found!');
        }

        return view('Manager.EmployeesAttendances.reason',compact('reason'));
    }

}
