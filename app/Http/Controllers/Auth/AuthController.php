<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Session;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPasswordMail;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\FlareClient\Http\Exceptions\NotFound;
use Validator;

class AuthController extends Controller
{
    public function index(){
        return view('Auth.index');
    }

    public function authenticate(Request $request){
        $field = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $validator = Validator::make($request->all(), [
            'login' => ['required', Rule::exists('users', $field)],
            'password' => ['required'],
        ],[
            'login.exists' => ucwords($field.' does not exists')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withInput()
                            ->withErrors($validator)
                            ->with('status', 'Please provide the needed details.');
        }else{
            if (Auth::attempt([$field => $request->input('login'), 'password' => $request->input('password')], $request->boolean('remember_me'))) {
                $request->session()->regenerate();
                if(auth()->user()->role == 'admin'){
                    return redirect(RouteServiceProvider::ADMIN);
                }else if(auth()->user()->role == 'manager'){
                    return redirect(RouteServiceProvider::MANAGER);
                }else{
                    return redirect(RouteServiceProvider::EMPLOYEE);
                }
            }
            return redirect()->back()->withInput()->with('status', 'Credentials Are Invalid.');
        }
    }

    public function forgotPassword(Request $request){
        if($request->isMethod('post')){
            $user = User::where('email','=',$request->input('email'));
            if($user->count() > 0){
                $user = User::findOrFail($user->firstOrFail()->id);
                $user->token = uniqid().uniqid().uniqid().uniqid().uniqid();
                if($user->save()){
                    Mail::to($user->email)->queue(new ForgotPasswordMail($user));
                    return redirect()->back()->withInput()->with('status', 'An Email Has been sent. Please Check your Email or Spam. You can Re-request another link if you did not recieve any mail.');
                }
                return redirect()->back()->withInput()->with('status', 'Failed Sending Email.');
            }
            return redirect()->back()->withInput()->with('status', 'Email not found.');
        }
        return view('Auth.forgotPassword');
    }

    public function passwordReset($token = null, Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'password' => ['required', 'min:8'],
                'confirm-password' => ['required', 'same:password'],
            ]);

            if($validator->fails()){
                return redirect()->back()->withInput()->withErrors($validator);
            }else{
                $user = User::where('token','=',$token);
                if($user->count() > 0){
                    $user = User::findOrFail($user->firstOrFail()->id);
                    $user->token = null;
                    $user->password = $request->input('password');
                    if($user->save()){
                        return redirect()->route('auth.user.index')->with('status', 'Password has been changed.');
                    }
                    return redirect()->route('auth.user.index')->with('status', 'Password changing failed.');
                }
            }
        }
        return view('Auth.passwordReset', [
            'token' => $token
        ]);
    }

    public function logout(Request $request){
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        Session::flush();
        Auth::guard('web')->logout();

        return redirect(RouteServiceProvider::LOGIN)
                ->with('status', 'Session Ended.');
    }
}
