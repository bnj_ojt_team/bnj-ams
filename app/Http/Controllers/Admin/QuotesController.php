<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quote;
use DB;

class QuotesController extends Controller
{
    public function index(){
        return view('Admin.Quotes.index');
    }

    public function getAllQuotes(){
        $quotes = Quote::get();
        return response()->json(['data' => $quotes]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){
            $validator = $request->validate([
                'quotes'=>['bail', 'required','min:6','max:255'],
                'is_active'=>['bail', 'nullable']
            ],[
                'quotes.required'=>ucwords('please provide quotes!'),
                'quotes.max'=>ucwords('quotes must below at 255 characters!'),
            ]);
            $quote = Quote::create($request->all());
            if($quote->is_active == 1){
                Quote::where('is_active', 1)->update(['is_active' => 0]);
            }
            $quote->is_active = $request->boolean('is_active');
            if($quote->save()){
                return response()->json(['title' => 'Quote has been saved. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Quote could not be saved. Please try again. ', 'icon' => 'error'], 404);
            }
        }
    }

    public function edit($id = null, Request $request){
        $quote = Quote::query()->findOrFail($id);
        if($request->isMethod('post')){
            $validator = $request->validate([
                'quotes'=>['bail', 'required','min:6','max:255'],
                'is_active'=>['bail', 'nullable']
            ],[
                'quotes.required'=>ucwords('please provide quotes!'),
                'quotes.max'=>ucwords('quotes must below at 255 characters!'),
            ]);

            $quote->update($request->all());
            if(!$quote->wasChanged()){
                return response()->json(['title' => 'Nothing to update. ', 'icon' => 'info'], 200);
            }
            if($quote->is_active == 1){
                Quote::where('is_active', 1)->update(['is_active' => 0]);
            }
            $quote->is_active = $request->boolean('is_active');


            if($quote->save()){
                return response()->json(['title' => 'Quote has been updated. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Quote could not be updated. Please try again. ', 'icon' => 'error'], 404);
            }
        }
        return response()->json($quote);
    }

    public function isActive($id = null, $process = null, Request $request){
        $quote = Quote::query()->findOrFail($id);
        if($process == "on"){
            DB::table('quotes')
                ->where('is_active', true)
                ->update(['is_active' => false]);
            $quote->is_active = true;
        }else{
            $quote->is_active = false;
        }
        if($quote->save()){
            if($process == "on"){
                return response()->json(['title' => 'Quotes has been Enabled. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Quote has been Disabled. ', 'icon' => 'success'], 200);
            }
        }else{
            return response()->json(['title' => 'Quote could not be changed. ', 'icon' => 'error'], 404);
        }
    }

    public function delete($id = null){
        $quote = Quote::findOrFail($id);
        if($quote->delete()){
            return response()->json(['title' => 'Quotes has been Deleted. ', 'icon' => 'success'], 200);
        }else{
            return response()->json(['title' => 'Quote could not be deleted. ', 'icon' => 'error'], 404);
        }
    }
}
