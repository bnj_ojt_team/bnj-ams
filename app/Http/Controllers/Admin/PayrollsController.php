<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\Excuse;
use App\Models\Holiday;
use App\Models\User;
use Illuminate\Database\Query\Builder;
use Moment\Moment;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Collection;

class PayrollsController extends Controller
{
    const NON_WORKING_DAYS = ['Saturday', 'Sat', 'Sunday', 'Sun'];
    const HOLIDAYS_TYPE = ['Regular Holiday',
                            'Non Working Holiday',
                            'Special Non Working Holiday'];

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('Admin.Payrolls.index');
    }

    public function getAttendances($month = null, $year = null){
        $dateFilter = (($month !=  null) ? $month:date('F')).' '.(($year != null || $year != 'null') ? $year:date('Y'));
        $moment = new Moment($dateFilter);
        $period = $moment->getPeriod('month');
        $data = Attendance::query()
            ->leftJoin('excuses', 'attendances.id', '=', 'excuses.attendance_id')
            ->with([ 'user.profile.employeeRole'])
            ->where(function ($query) use ($period){
                return $query->where('attendances.attendance_date', '>=', $period->getStartDate()->format('Y-m-d'))
                    ->where('attendances.attendance_date', '<=', $period->getEndDate()->format('Y-m-d'));
            })->whereNotNull('attendance_total_hours')
            ->select('attendances.*')
            ->selectRaw('SUM(attendances.attendance_total_hours) as total_hours')
            ->selectRaw('SUM(excuse_total_hours) as total_excuse_hours')
            ->groupBy('user_id')
            ->get();
        // dd($data);
        return response()->json(['data' => $data]);
    }

    public function getAttendanceLists($userId = null, $month = null, $year = null){

        $dateFilter = (($month == null) ? date('F'):$month).' '.(($year == null) ? date('Y'):$year);

        $collection = collect();
        $workFromHomeCollection = collect();
        $nonWorkingDaysCollection = collect();

        $sNonWorkingHolidaysCollection = collect();
        $nonWorkingHolidaysCollection = collect();
        $regularHolidaysCollection = collect();

        $thisMonthFirstDay = new Carbon('first day of '.$dateFilter);
        $thisMonthLastDay = new Carbon('last day of '.$dateFilter);
        $attendances = Attendance::with('excuse', 'user.profile.employeeRole', 'accomplishment', 'accomplishment.homeAttendances')->where('user_id', '=', $userId)->get();
        $currentUser = User::with('profile.employeeRole')->findOrFail($userId);

        // Loop through this months dates
        while($thisMonthFirstDay->toDateString() <= $thisMonthLastDay->toDateString()){

            $attendanceCondition = $attendances->contains(function ($item) use ($thisMonthFirstDay) {
                return ($item['attendance_date'] == $thisMonthFirstDay->toDateString() && ($item['timein_am'] != null || $item['timeout_am'] != null || $item['timein_pm'] != null || $item['timeout_pm'] != null));
            });
            $attendance = ($attendanceCondition) ? $attendances->where('attendance_date', '=', $thisMonthFirstDay->toDateString())->first() : null;

            // If attendance is non working day
            if(in_array($thisMonthFirstDay->format('l'), self::NON_WORKING_DAYS) ||
                in_array($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate())){
                if($attendance != null){ // If employee has attendance
                    if(in_array($thisMonthFirstDay->format('l'), self::NON_WORKING_DAYS)){
                        $attendance->toArray();
                        $attendance->minus_hours = (getCompanyHours() - $attendance->attendance_total_hours);
                        $attendance->is_non_working = true;
                        $collection->push($attendance);
                    }else if(in_array($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate())){
                        $holidayKey = array_search($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate());
                        $holidayType = Holiday::where('id', $holidayKey)->get('holiday_type')->first();
                        if($holidayType->holiday_type === 'Special Non Working Holiday'){
                            $attendance->toArray();
                            $attendance->minus_hours = $attendance->attendance_total_hours;
                            $attendance->is_non_working = true;
                            $sNonWorkingHolidaysCollection->push($attendance);
                        }else if($holidayType->holiday_type === 'Non Working Holiday'){
                            $attendance->toArray();
                            $attendance->minus_hours = $attendance->attendance_total_hours;
                            $attendance->is_non_working = true;
                            $nonWorkingHolidaysCollection->push($attendance);
                        }else if($holidayType->holiday_type === 'Regular Holiday'){
                            $attendance->toArray();
                            $attendance->minus_hours = $attendance->attendance_total_hours;
                            $attendance->is_non_working = true;
                            $regularHolidaysCollection->push($attendance);
                        }
                    }
                    
                }
            }else{ // If attendance is working days 
                if($attendance != null){ // If employee has attendance
                    $attendance->toArray();
                    $attendance->minus_hours = (getCompanyHours() - $attendance->attendance_total_hours);
                    $attendance->is_non_working = false;
                    $collection->push($attendance);
                }else{ // If employee has no attendance for this day, meaning absent
                    $collection->push([
                        "id" => null,
                        "user_id" => null,
                        "attendance_date" => $thisMonthFirstDay->toDateString(),
                        "timein_am" => null,
                        "timeout_am" => null,
                        "timein_pm" => null,
                        "timeout_pm" => null,
                        "attendance_total_hours" => null,
                        "is_holiday" => null,
                        "is_present" => null,
                        "is_late" => null,
                        "is_work_from_home" => null,
                        "minus_hours" => getCompanyHours(),
                        "is_non_working" => false,
                        "excuse" => [
                            "attendance_id" => null,
                            "timein_am" => null,
                            "timeout_am" => null,
                            "timein_pm" => null,
                            "timeout_pm" => null,
                            "excuse_total_hours" => null,
                        ],
                        "user" => [
                            "profile" => [
                                "firstname" => $currentUser->profile->firstname,
                                "middlename" => $currentUser->profile->middlename,
                                "lastname" => $currentUser->profile->lastname,
                                "employee_role" => [
                                    "position" => $currentUser->profile->employeeRole->position,
                                    "level" => $currentUser->profile->employeeRole->level,
                                ]
                            ]
                        ]
                    ]);
                }
            }

            $attendance = $attendances->where('attendance_date', '=', $thisMonthFirstDay->toDateString())->first();
            // Push work from home attendances into collection if there are work from home attendances
            if($attendance != null){
                if($attendance->is_work_from_home === 1){
                    foreach ($attendance->accomplishment->homeAttendances as $value) {
                        $workFromHomeCollection->push([
                            "id" => $attendance->id,
                            "user_id" => $attendance->user_id,
                            "attendance_date" => $thisMonthFirstDay->toDateString(),
                            "timein_am" => $value->time_in,
                            "timeout_am" => $value->time_out,
                            "timein_pm" => null,
                            "timeout_pm" => null,
                            "attendance_total_hours" => Carbon::parse($value->time_in)->floatDiffInHours(Carbon::parse($value->time_out)),
                            "is_holiday" => null,
                            "is_present" => null,
                            "is_late" => null,
                            "is_work_from_home" => $attendance->is_work_from_home,
                            "is_non_working" => false,
                            "excuse" => [
                                "attendance_id" => null,
                                "timein_am" => null,
                                "timeout_am" => null,
                                "timein_pm" => null,
                                "timeout_pm" => null,
                                "excuse_total_hours" => null,
                            ],
                            "user" => [
                                "profile" => [
                                    "firstname" => $currentUser->profile->firstname,
                                    "middlename" => $currentUser->profile->middlename,
                                    "lastname" => $currentUser->profile->lastname,
                                    "employee_role" => [
                                        "position" => $currentUser->profile->employeeRole->position,
                                        "level" => $currentUser->profile->employeeRole->level,
                                    ]
                                ]
                            ]
                        ]);
                    }
                }
            }
            // Push work from home attendances into collection if there are work from home attendances end

            $thisMonthFirstDay->addDay();
        }
        // Loop through this months dates end

        $collection->push(['id' => "empty"]);
        // Merge non working days attendances with attendances collection if there are datas
        if($sNonWorkingHolidaysCollection->count() > 0){
            $collection->push([
                "id" => '',
                "text" => 'Special Non Working Holidays'
            ]);
            $collection = $collection->merge($sNonWorkingHolidaysCollection);
        }

        if($nonWorkingHolidaysCollection->count() > 0){
            $collection->push([
                "id" => '',
                "text" => 'Non Working Holidays'
            ]);
            $collection = $collection->merge($nonWorkingHolidaysCollection);
        }

        if($regularHolidaysCollection->count() > 0){
            $collection->push([
                "id" => '',
                "text" => 'Regular Holidays'
            ]);
            $collection = $collection->merge($regularHolidaysCollection);
        }
        // Merge non working days attendances with attendances collection if there are datas end

        $collection->push(['id' => "empty"]);
        // Merge work from home days attendances with attendances collection if there are datas
        if($workFromHomeCollection->count() > 0){
            $collection->push([
                "id" => '',
                "text" => 'Work From Home Hours'
            ]);

            // Append new data for header
            $collection->push([
                "id" => 'wfh',
                "A" => "Name",
                "B" => "Position",
                "C" => "Date",
                "D" => "Time In",
                "E" => "Time Out",
            ]);
            // Append new data for header end

            $collection = $collection->merge($workFromHomeCollection);
        }
        // Merge work from home days attendances with attendances collection if there are datas end
        

        // Append three new data with empty id
        $collection->push(['id' => "empty"]);
        $collection->push(['id' => "empty"]);
        $collection->push(['id' => "empty"]);
        $collection->push(['id' => "contribution"]);
        $collection->push(['id' => "contribution"]);
        // Append new line for sss and pagibig
        $collection->push(['id' => "contribution"]);
        $collection->push(['id' => "contribution"]);
        $collection->push(['id' => "contribution"]);
        $collection->push(['id' => "contribution"]);
        $collection->push(['id' => "contribution"]);
        // Append three new data with empty id end
        return response()->json(['data' => $collection]);
    }


    public function getEstimatedSalary($userId = null, $month = null, $year = null){
        $collection = collect();
        $dateFilter = (($month == null) ? date('F'):$month).' '.(($year == null) ? date('Y'):$year);

        // Instantiate new starting and ending date this month
        $thisMonthFirstDay = new Carbon('first day of '.$dateFilter);
        $startDate = new Carbon('first day of '.$dateFilter);
        $thisMonthLastDay = new Carbon('last day of '.$dateFilter);
        $endDate = new Carbon('last day of '.$dateFilter);
        // Get my attendances
        $attendances = Attendance::with('excuse', 'user.profile.employeeRole', 'accomplishment', 'accomplishment.homeAttendances')->where('user_id', '=', $userId)->get();
        // Instantiate new starting and ending date this month end

        $collection->push(['', '', '', '', '', '', '', '', '', '', ]);

        $collection->push([
            'Total',
            'Basic Salary',
            'Days',
            '1 Day',
            '1 Hour',
            'Holiday Work',
            'Holiday Salary',
            'OT/AB',
            '',
            'Salary Without Contribution/s',
        ]);

        // Get hours every attendance work days 
        $totalHours = 0;
        $totalHolidayHours = 0;
        $holidaSalary = 0;
        $basicSalary = getMyBasicSalary($userId); // Basic Salary getMyBasicSalary

        // Get count of working days in this month
        $holidays = [];
        foreach ($this->getHolidaysDate() as $date) {
            array_push($holidays, Carbon::parse($date));
        }
        $totalWorkingDays = Carbon::parse($startDate->format('Y-m-d'))->diffInDaysFiltered(function (Carbon $date) use ($holidays) {
            return $date->isWeekday() && !in_array($date, $holidays);
        }, Carbon::parse($endDate->format('Y-m-d'))->addDay());
        // Get count of working days in this month end

        // dd($endDate);

        // Get per day total salary
        $perDaySalary = round(($basicSalary/$totalWorkingDays), 4);
        // Get per day total salary end

        // Get per hour total salary
        $perHourSalary = round(($perDaySalary/getCompanyHours()), 4);
        // Get per hour total salary end

        // Loop through this months dates
        while($thisMonthFirstDay->toDateString() <= $thisMonthLastDay->toDateString()){
            $attendance = $attendances->where('attendance_date', '=', $thisMonthFirstDay->toDateString())->first();
            // If attendance is non working day
            if(in_array($thisMonthFirstDay->format('l'), self::NON_WORKING_DAYS) || // If sat or sun
                in_array($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate())){ // If non working holidays
                if($attendance != null){ // If employee has attendance
                    $hours = 0;
                    if(in_array($thisMonthFirstDay->format('l'), self::NON_WORKING_DAYS)){

                        if($attendance->is_work_from_home === 1){ // If non working days is work from home
                            foreach ($attendance->accomplishment->homeAttendances as $value) {
                                $time_in = Carbon::parse($value->time_in);
                                $time_out = Carbon::parse($value->time_out);
                                $hours += ($time_in->floatDiffInHours($time_out));
                            }
                        }
                        if($attendance->attendance_total_hours != null){ // If not work from home
                            $hours += $attendance->attendance_total_hours;
                        }
                        // Holiday Salary
                        $holidaSalary += round(($perHourSalary*$hours), 4);
                        // Holiday Salary end
                    }else if(array_search($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate())){
                        $holidayKey = array_search($thisMonthFirstDay->format('Y-m-d'), $this->getHolidaysDate());
                        $holidayRate = Holiday::where('id', $holidayKey)->get('rate')->first();

                        if($attendance->is_work_from_home === 1){ // If non working days is work from home
                            foreach ($attendance->accomplishment->homeAttendances as $value) {
                                $time_in = Carbon::parse($value->time_in);
                                $time_out = Carbon::parse($value->time_out);
                                $hours += ($time_in->floatDiffInHours($time_out));
                            }
                        }
                        if($attendance->attendance_total_hours != null){ // If not work from home
                            $hours += $attendance->attendance_total_hours;
                        }

                        // Holiday Salary
                        $holidaSalary += round(($perHourSalary*$hours*($holidayRate->rate/100)), 4);
                        // Holiday Salary end
                    }

                    $totalHolidayHours += $hours;
                }
            }else{
                if($attendance != null){ // If employee has attendance meaning present
                    if($attendance->is_work_from_home === 1){ // If working days is work from home
                        foreach ($attendance->accomplishment->homeAttendances as $value) {
                            $time_in = Carbon::parse($value->time_in);
                            $time_out = Carbon::parse($value->time_out);
                            $totalHours += ($time_in->floatDiffInHours($time_out));
                        }
                    }else{
                        $totalHours -= (getCompanyHours() - $attendance->attendance_total_hours);
                    }
                }else{ // Else meaning absent
                    $totalHours -= getCompanyHours();
                }
            }
            $thisMonthFirstDay->addDay();
        }
        // Loop through this months dates end
        // Get hours every attendance work days end

        // Overtime or absent
        $OtAb = round($totalHours*$perHourSalary, 0);
        // Overtime or absent end

        // Get total salary 
        $salary = round(($basicSalary+$holidaSalary+($OtAb)), 0);
        // Get total salary end

        $collection->push([
            $totalHours,
            excelFormat($basicSalary),
            $totalWorkingDays,
            excelFormat($perDaySalary),
            excelFormat($perHourSalary),
            $totalHolidayHours,
            excelFormat($holidaSalary),
            excelFormat($OtAb),
            '',
            excelFormat($salary),
        ]);

        return response()->json($collection);
    }

    public function getContributionRate($userId = null, $month = null, $year = null){
        $collection = collect();
        if(self::hasContributions($userId)[0]){
            $collection->push(['', '', '', '', '', '',]);

            $collection->push([
                'Contribution Name',
                'Employee Rate',
                'Employer Rate',
                'Total Rate',
                '',
                'Salary With Contribution/s',
            ]);
            $estimatedSalary = floatval(str_replace(',', '', $this->getEstimatedSalary($userId, $month, $year)->getData()[2][9]));
            foreach ($this->getUserContributionRate($userId) as $item) {
                $estimatedSalary -= (getMyBasicSalary($userId) * $item->employee_rate);
                
                $collection->push([
                    $item->name,
                    excelFormat((getMyBasicSalary($userId) * $item->employee_rate)),
                    excelFormat((getMyBasicSalary($userId) * $item->employer_rate)),
                    excelFormat((getMyBasicSalary($userId) * ($item->employee_rate + $item->employer_rate))),
                    '',
                    excelFormat($estimatedSalary),
                ]);
            }

            $collection->push([
                temporaryPagibigRate()['name'],
                excelFormat(temporaryPagibigRate()['employee_rate']),
                excelFormat(temporaryPagibigRate()['employer_rate']),
                excelFormat(temporaryPagibigRate()['total_rate']),
                '',
                excelFormat($estimatedSalary - temporaryPagibigRate()['employee_rate']),
            ]);
        }
            


        return response()->json($collection);
    }

    private function getUserContributionRate($userId = null){
        if($userId == null){
            $userContribution = User::with('profile.employeeRole.contributions')->where('id', auth()->user()->id)->first();
        }else{
            $userContribution = User::with('profile.employeeRole.contributions')->where('id', $userId)->first();
        }
        
        return $userContribution->profile->employeeRole->contributions;
    }

    private function hasContributions($userId = null){
        return DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->where('users.id', $userId)
            ->pluck('profiles.has_contributions');
    }

    protected function getHolidaysDate(){
        $holidays = Holiday::get()->pluck('holiday_date', 'id')->toArray();
        
        return $holidays;
    }
}
