<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DatabaseBackupController extends Controller
{
    public function backup(){
        $files = Storage::disk('local')->allFiles('public/databases');
        $fileNames = array_map(function($file){
            return basename($file);
        }, $files);
        return view('Admin.DatabaseBackup.backup', [
            'files' => $fileNames
        ]);
    }

    public function downloadFile($filename = null){
        return response()->download(public_path('storage/databases/'.$filename));
    }

    public function createNewBackup(){
        $filepath = storage_path('app/public/databases/');
        $filename = date('M_d_Y_h_i_s_').'bnj_ams_db.sql';
        if(!File::isDirectory($filepath)){
            File::makeDirectory($filepath);
        }
        $command = "mysqldump --opt -h " . env('DB_HOST') . " -u ". env('DB_USERNAME') . " " . env('DB_DATABASE') . " > " . $filepath . $filename;

        if(exec($command) == ""){
            return redirect()->back()
                ->with('icon', 'success')
                ->with('title', 'New Database Backup Has been created.');
        }else{
            return redirect()->back()
                ->with('icon', 'error')
                ->with('title', 'New Database Backup creation failed.');
        }
    }

    public function createNewMainBackup(){
        $filepath = storage_path('app/public/databases/');
        $filename = 'bnj_ams_db.sql';
        if(!File::isDirectory($filepath)){
            File::makeDirectory($filepath);
        }
        $command = "mysqldump --opt -h " . env('DB_HOST') . " -u ". env('DB_USERNAME') . " " . env('DB_DATABASE') . " > " . $filepath . $filename;

        if(exec($command) == ""){
            return redirect()->back()
                ->with('icon', 'success')
                ->with('title', 'New Main Database Backup Has been created.');
        }else{
            return redirect()->back()
                ->with('icon', 'error')
                ->with('title', 'New Main Database Backup creation failed.');
        }
    }
}
