<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmployeeRole;
use Illuminate\Http\Request;
use App\Models\Contribution;
use Illuminate\Validation\Rule;

class ContributionsController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $contributions = Contribution::with('employeeRole')->get();
            return response()->json(['data' => $contributions]);
        }
        $employeeRoles = EmployeeRole::query()->get()->map(function ($query){
            return ['value' => $query->position.' '.$query->level, 'key' => $query->id];
        })->pluck('value','key');
        return view('Admin.Contributions.index', compact('employeeRoles'));
    }

    public function add(Request $request){
        if($request->isMethod('post')){
            // dd($request->all());
            $validator = $request->validate([
                'name'=>['bail', 'required'],
                'employer_rate'=>['bail', 'required'],
                'employee_rate'=>['bail', 'required'],
                'employee_role_id' =>['required', 'numeric', Rule::exists('employee_roles','id')],
            ]);
            $errate = $request->input('employer_rate');
            $eerate = $request->input('employee_rate');
            $request->merge(['total_percentage' => $errate + $eerate]);
            
            $contributions = Contribution::create($request->all());

            if($contributions->save()){
                return response()->json(['title' => 'Benefit has been saved. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Benefit could not be saved. Please try again. ', 'icon' => 'error'], 404);
            }
        }
    }

    public function edit($id =null, Request $request){
        $contributions = Contribution::FindOrFail($id);
        if($request->isMethod('post')){
            // dd($request->all());
            $validator = $request->validate([
                'name'=>['bail', 'required'],
                'employer_rate'=>['bail', 'required'],
                'employee_rate'=>['bail', 'required'],
                'employee_role_id' =>['required', 'numeric', Rule::exists('employee_roles','id')],
            ]);
            $errate = $request->input('employer_rate');
            $eerate = $request->input('employee_rate');
            $request->merge(['total_percentage' => $errate + $eerate]);
            
            $contributions->update($request->all());

            if($contributions->save()){
                return response()->json(['title' => 'Benefit has been updated. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Benefit could not be updated. Please try again. ', 'icon' => 'error'], 404);
            }
        }

        return response()->json($contributions);
    }

    public function delete($id = null){
        $contributions = Contribution::findOrFail($id);
        if($contributions->delete()){
            return response()->json(['title' => 'Benefit has been deleted. ', 'icon' => 'success'], 200);
        }else{
            return response()->json(['title' => 'Benefit could not be deleted. ', 'icon' => 'error'], 404);
        }
    }
}
