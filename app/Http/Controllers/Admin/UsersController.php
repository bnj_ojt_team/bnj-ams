<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Validator;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    // Account Settings Function
    public function accountSetting(){
        $user = User::findOrFail(auth()->user()->id);
        return view('Admin.Users.account-setting', [
            'user' => $user
        ]);
    }

    public function index(Request $request){
        if($request->wantsJson()){
            $employees = User::get();
            return response()->json(['data' => $employees]);
        }
        return view('Admin.Users.index');
    }

    // Edit user
    public function edit(Request $request){
        $user = User::findOrFail(auth()->user()->id);
        if($request->isMethod('post')){
            if($request->has('password')){
                $validator = $request->validate([
                    'current-password' => ['bail', 'required', function ($attribute, $value, $fail) use ($user) {
                        if (!\Hash::check($value, $user->password)) {
                            return $fail(__('The current password is incorrect.'));
                        }
                    }],
                    'password' => ['bail', 'required', 'min:8', function ($attribute, $value, $fail) use ($user) {
                        if (\Hash::check($value, $user->password)) {
                            return $fail(__('New password should not be the same as your current password.'));
                        }
                    }],
                    'confirm-password' => ['bail', 'required', 'same:password'],
                ],[
                    'password.required' => ucfirst('New password is required'),
                ]);
            }else{
                $validator = $request->validate([
                    'username' => ['bail', 'required', Rule::unique('users','username')->ignore($user->id)],
                    'email' => ['bail', 'required', Rule::unique('users','email')->ignore($user->id), 'email'],
                    'file' => ['bail', 'nullable', 'image'],
                ]);
            }
            $user->update($request->all());
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filepath = storage_path('app/public/avatars');
                $currentImage = $filepath.'/'.$user->image;
                if(File::isFile($currentImage)){
                    File::delete($currentImage);
                }
                $filename = uniqid().uniqid().uniqid().'.'.$file->getClientOriginalExtension();
                if(!File::isDirectory($filepath)){
                    File::makeDirectory($filepath);
                }
                if($file){
                    $file->move($filepath, $filename);
                }
                $user->image = $filename;
            }
            if($user->save()){
                return response()->json(['title' => 'User Account has updated. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'User Account could not be updated. ', 'icon' => 'error'], 404);
            }
        }
        return response()->json($user);
    }

    // Add new user
    public function add(Request $request){
        if($request->isMethod('post')){
            $validator = $request->validate([
                'username' => ['bail', 'required', Rule::unique('users','username')],
                'email' => ['bail', 'required', Rule::unique('users','email'), 'email'],
                'password' => ['bail', 'required', 'min:8'],
                'file' => ['bail', 'nullable', 'image'],
                'role' => ['bail', Rule::in($this->role($request->input('role')))],
                'confirm-password' => ['bail', 'required', 'same:password'],
            ]);

            $users = User::make($request->all());
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filepath = storage_path('app/public/avatars');
                $filename = uniqid().uniqid().uniqid().'.'.$file->getClientOriginalExtension();
                if(!File::isDirectory($filepath)){
                    File::makeDirectory($filepath);
                }
                if($file){
                    $file->move($filepath, $filename);
                }
                $users->image = $filename;
            }
            if($users->save()){
                return response()->json(['title' => 'User Account Has been added. ', 'icon' => 'success',
                    'redirect' => route('admin.users.index')], 200);
            }else{
                return response()->json(['title' => 'User could not be added. ', 'icon' => 'error'], 404);
            }
        }
        return view('Admin.Users.add');
    }

    // Set active Or disable
    public function isActive($id = null, $process = null, Request $request){
        $user = User::query()->findOrFail($id);
        if($user->role == 'admin'){
            return response()->json(['title' => 'Admin account can not be disabled. ', 'icon' => 'error'], 200);
        }
        if($process == "on"){
            $user->active = true;
        }else{
            $user->active = false;
        }
        if($user->save()){
            if($process == "on"){
                return response()->json(['title' => 'Account has been Activated. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Account has been Disabled. ', 'icon' => 'success'], 200);
            }
        }else{
            return response()->json(['title' => 'Account could not be modified. ', 'icon' => 'error'], 404);
        }
    }

    // User Role
    public function role($role){
        if(strtolower($role) == 'admin'){
            return [
                'admin' => 'admin',
                'Admin' => 'Admin'
            ];
        }else if(strtolower($role) == 'manager'){
            return [
                'manager' => 'manager',
                'Manager' => 'Manager'
            ];
        }
    }
}
