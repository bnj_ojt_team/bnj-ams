<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SalaryLoan;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SalaryLoansController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $profiles = Profile::query()->get()->map(function($row){
            return ['value' => $row->firstname, 'key' => $row->id];
        })->pluck('value','key');
        return view('Admin.SalaryLoans.index',compact('profiles'));
    }

    public function getSalaryLoans(){
        $data = SalaryLoan::query()->with(['user.profile'])->get();
        return response()->json(['data' => $data]);
    }

    public function edit($id = null, Request $request){
        $salaryLoan = SalaryLoan::query()->findOrFail($id);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'amount' => ['sometimes', 'numeric'],
                'remainder' => ['sometimes', 'numeric'],
                'balance' => ['sometimes', 'numeric'],
                'reason' => ['sometimes', 'max:65535']
            ],[
                'amount.sometimes' => ucwords('the amount must required!'),
                'amount.numeric' => ucwords('the amount must number!'),
                'remainder.sometimes' => ucwords('the remainder must required!'),
                'remainder.numeric' => ucwords('the remainder must number!'),
                'balance.sometimes' => ucwords('the balance must required!'),
                'balance.numeric' => ucwords('the balance must number!'),
                'reason.sometimes' => ucwords('the reason must not empty!'),
                'balance.max' => ucwords('the reason exceed at 65535 characters!'),
            ]);

            $salaryLoan->update($request->all());
            $salaryLoan->remainder = 0;

            if($salaryLoan->save()){
                $result = ['message' => ucwords('salary loan successfully updated!'), 'result'=>'Success'];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('salary loan has not been updated!'), 'result'=>'Error'];
                return response()->json($result,404);
            }

        }
        return response()->json($salaryLoan);
    }

    public function status($id = null,int $index = null, Request $request){
        $salaryLoan = SalaryLoan::query()->findOrFail($id);
        $status = [ucwords('declined'), ucwords('completed')];
        if($request->isMethod('post')){
            $salaryLoan->status = $status[$index];
            if($salaryLoan->save()){
                $result = ['message' => ucwords('salary loan status changed!'), 'result'=>'Success'];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('salary loan status has not been changed!'), 'result'=>'Error'];
                return response()->json($result,404);
            }
        }
    }

}
