<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EmployeeRole;

class EmployeeRolesController extends Controller
{
    public function index(Request $request){
        if($request->wantsJson()){
            $employeeRole = EmployeeRole::get();
            return response()->json(['data' => $employeeRole]);
        }
        return view('Admin.EmployeeRoles.index');
    }

    // Add function
    public function add(Request $request){
        if($request->isMethod('post')){
            if(EmployeeRole::create($request->all())){
                return response()->json(['title' => 'Employee Role Has been saved. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Employee Role Could not be saved. ', 'icon' => 'error'],404);
            }
        }
    }

    // Edit function
    public function edit($id = null, Request $request){
        $employeeRole = EmployeeRole::findOrFail($id);
        if($request->isMethod('post')){
            $employeeRole->update($request->all());
            if(!$employeeRole->wasChanged()){
                return response()->json(['title' => 'Nothing has changed. ', 'icon' => 'info'], 200);
            }
            if($employeeRole->save()){
                return response()->json(['title' => 'Employee Role Has been updated. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Employee Role Could not be updated. ', 'icon' => 'error'], 404);
            }
        }
        return response()->json($employeeRole);
    }

    // Delete function
    public function delete($id = null, $password = null){

        $employeeRole = EmployeeRole::findOrFail($id);

        if(!password_verify($password,auth()->user()->password)){
            $result  = ['title' =>ucwords( 'password confirmation not matched!'), 'icon' => 'info'];
            return response()->json($result,404);
        }

        if($employeeRole->delete()){
            return response()->json(['title' => 'Employee Role has been Deleted. ', 'icon' => 'success'], 200);
        }else{
            return response()->json(['title' => 'Employee Role could not be deleted. ', 'icon' => 'error'], 404);
        }
    }
}
