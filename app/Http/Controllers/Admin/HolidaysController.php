<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Holiday;
use Illuminate\Validation\Rule;

class HolidaysController extends Controller
{
    public function index(){
        return view('Admin.Holidays.index');
    }

    public function getAllHolidays(){

        // $date = new \DateTime();
        // $holidays = Holiday::query()
        //     ->select('*')
        //     ->selectRaw('(CASE WHEN holidays.is_per_year THEN DATE_FORMAT(holidays.holiday_date, "'.$date->format('Y').'-%m-%d") ELSE holidays.holiday_date END) as holiday_dates')
        //     ->having('holiday_dates', '=', $date->format('Y-m-d'))
        //     ->get();

        $holidays = Holiday::query()->get();
        return response()->json(['data' => $holidays]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){
            $holiday_types = [
                'Regular Holiday' => 'Regular Holiday',
                'Non Working Holiday' => 'Non Working Holiday',
                'Special Non Working Holiday' => 'Special Non Working Holiday',
            ];
            $validator = $request->validate([
                'holiday_name'=>['bail', 'required',Rule::unique('holidays', 'holiday_name'),'max:255'],
                'holiday_type'=>['bail', 'required', Rule::in($holiday_types)],
                'holiday_date'=>['bail', 'required', 'date', Rule::unique('holidays', 'holiday_date')],
                'is_per_year'=>['bail', 'nullable']
            ]);
            $holiday = Holiday::create($request->all());
            $holiday->is_per_year = $request->boolean('is_per_year');
            if($holiday->save()){
                return response()->json(['title' => 'Holiday has been saved. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Holiday could not be saved. Please try again. ', 'icon' => 'error'], 404);
            }
        }
    }

    public function edit($id = null, Request $request){
        $holiday = Holiday::query()->findOrFail($id);
        if($request->isMethod('post')){
            $holiday_types = [
                'Regular Holiday' => 'Regular Holiday',
                'Non Working Holiday' => 'Non Working Holiday',
                'Special Non Working Holiday' => 'Special Non Working Holiday',
            ];
            $validator = $request->validate([
                'holiday_name'=>['bail', 'required',Rule::unique('holidays', 'holiday_name')->ignore($holiday->id),'max:255'],
                'holiday_type'=>['bail', 'required', Rule::in($holiday_types)],
                'holiday_date'=>['bail', 'required', 'date', Rule::unique('holidays', 'holiday_date')->ignore($holiday->id)],
                'is_per_year'=>['bail', 'nullable']
            ]);
            $holiday->update($request->all());
            $holiday->is_per_year = $request->boolean('is_per_year');
            if(!$holiday->wasChanged()){
                return response()->json(['title' => 'Nothing has changed. ', 'icon' => 'info'], 200);
            }
            if($holiday->save()){
                return response()->json(['title' => 'Holiday has been updated. ', 'icon' => 'success'], 200);
            }else{
                return response()->json(['title' => 'Holiday could not be updated. Please try again. ', 'icon' => 'error'], 404);
            }
        }
        return response()->json($holiday);
    }

    public function delete($id = null){
        $holiday = Holiday::findOrFail($id);
        if($holiday->delete()){
            return response()->json(['title' => 'Holiday has been Deleted. ', 'icon' => 'success'], 200);
        }else{
            return response()->json(['title' => 'Holiday could not be deleted. ', 'icon' => 'error'], 404);
        }
    }
}
