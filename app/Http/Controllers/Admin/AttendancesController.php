<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attendance;
use App\Models\Profile;
use App\Models\Accomplishment;
use App\Models\Reason;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class AttendancesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    // View for Attendances index
    public function index(){
        $profiles = Profile::query()->get()->map(function($row){
            return ['value' => $row->firstname, 'key' => $row->id];
        })->pluck('value','key');
        return view('Admin.Attendances.index',compact('profiles'));
    }

    // View for OT Attendances
    public function todayOvertimeAttendances(){
        // $date1 = Carbon::parse('2022-01-01 17:00:00');
        // $date2 = Carbon::parse('2022-01-01 17:01:00');
        // dd($date1->floatDiffInHours($date2));

        $profiles = Profile::query()->get()->map(function($row){
            return ['value' => $row->firstname, 'key' => $row->id];
        })->pluck('value','key');
        return view('Admin.Attendances.today_overtime_attendances',compact('profiles'));
    }

    public function homeAttendances(Request $request){
        if($request->wantsJson()){
            $accomplishmentsToday = Accomplishment::with(['Attendance.User.Profile', 'Attendance.Reason'])
                ->whereHas('attendance', function ($query){
                    $query->whereDate('attendances.attendance_date', Carbon::today()->toDateString());
                })->get();
            return response()->json(['data' => $accomplishmentsToday]);
        }
        return view('Admin.Attendances.home_attendances');
    }

    public function updateAttendance($id = null){
        $query = DB::table('accomplishments')
            ->where('id', $id)
            ->update(['status' => DB::raw(
                'CASE WHEN status = "Pending" THEN "Approved" 
                WHEN status = "Approved" THEN "Disapproved" 
                ELSE "Approved" END'
            )]);

        return response()->json(['title' => ($query) ? 'Attendance has been updated. ':'Attendance could not be updated. ', 'icon' => ($query) ? 'success':'error'], 200);
    }


    public function getTodayOvertimeAttendances(){
        $data = Attendance::query()
        ->with(['User.Profile', 'Excuse', 'Accomplishment.homeAttendances','Reason'])
        ->whereDate('attendance_date', Carbon::today()->toDateString())
        ->where(function ($query){
            $query->whereNotNull('timein_am')
                ->orWhereNotNull('timeout_am')
                ->orWhereNotNull('timein_pm')
                ->orWhereNotNull('timeout_pm');
        })->whereTime('timeout_pm', '>', getCompanySetting()->afternoon_time_out)->get();
        return response()->json(['data'=>$data]);
    }

    public function updateOvertime($id = null){
        $query = DB::table('attendances')
            ->where('id', $id)
            ->update(['is_overtime' => DB::raw('CASE WHEN is_overtime = 1 THEN 0 ELSE 1 END')]);

        return response()->json(['title' => ($query) ? 'Attendance has been updated. ':'Attendance could not be updated. ', 'icon' => ($query) ? 'success':'error'], 200);
    }

    public function getAttendances(){
        $data = Attendance::query()
        ->with(['User.Profile', 'Excuse', 'Accomplishment.homeAttendances','Reason'])
        ->whereDate('attendance_date', Carbon::today()->toDateString())
        ->whereNotNull('timein_am')
        ->orWhereNotNull('timeout_am')
        ->orWhereNotNull('timein_pm')
        ->orWhereNotNull('timeout_pm')
        ->get();
        return response()->json(['data'=>$data]);
    }

    public function accomplishment($id = null){
        $attendance = Attendance::query()->with(['Accomplishment.homeAttendances', 'User.Profile'])->findOrFail($id);
        if(empty($attendance->accomplishment)){
            abort(404,'accomplishment not found!');
        }
        return view('Admin.Attendances.accomplishment',compact('attendance'));
    }

    public function reason($id = null){
        $reason = Reason::query()->with(['Attendance.User.Profile'])->findOrFail($id);

        if(empty($reason)){
            abort(404,'reason not found!');
        }

        return view('Admin.Attendances.reason',compact('reason'));
    }

}
