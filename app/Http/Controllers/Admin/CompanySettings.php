<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CompanySetting;
use Illuminate\Http\Request;

class CompanySettings extends Controller
{

    public function index(){
        $companySetting = CompanySetting::query()->first();
        return view('Admin.CompanySettings.index', compact('companySetting'));
    }

    public function getCompanySettings(){
        $data = CompanySetting::query()->get();
        return response()->json(['data'=>$data]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){

            $validator = $request->validate([
                'morning_time_in' => ['required','date_format:H:i'],
                'morning_time_out' => ['required','date_format:H:i','after:morning_time_in'],
                'afternoon_time_in' => ['required','date_format:H:i','after:morning_time_out'],
                'afternoon_time_out' => ['required','date_format:H:i','after:afternoon_time_in'],
                'active' =>['boolean','nullable']
            ],[
                'morning_time_in.required' => ucwords('morning time in required!'),
                'morning_time_in.date_format' => ucwords('invalid morning time in format!'),
                'morning_time_out.required' => ucwords('morning time out required!'),
                'morning_time_out.date_format' => ucwords('invalid morning time out format!'),
                'morning_time_out.after' => ucwords('morning time out must be after morning time in!'),
                'afternoon_time_in.required' => ucwords('afternoon time in required!'),
                'afternoon_time_in.date_format' => ucwords('invalid afternoon time in format!'),
                'afternoon_time_in.after' => ucwords('afternoon time in must be after morning time out!'),
                'afternoon_time_out.required' => ucwords('afternoon time out required!'),
                'afternoon_time_out.date_format' => ucwords('invalid afternoon time out format!'),
                'afternoon_time_out.after' => ucwords('afternoon time out must be after afternoon time in!'),
                'active.boolean' => ucwords('invalid active value!')
            ]);

            $companySetting = CompanySetting::query()->make($request->all());
            $active = CompanySetting::query();
            if( $request->boolean('active')){
                $companySetting->active = $request->boolean('active');
                $active->where('active','=',1)
                    ->update(['active' => 0]);
            }

            if($companySetting->save() ||$active ){
                $result = ['message'=>ucwords('the company setting has been saved!'), 'result'=>strtolower('success')];
                return response()->json($result,200);
            }else{
                $result = ['message'=>ucwords('the company setting has not been saved!'), 'result'=>strtolower('error')];
                return response()->json($result,404);
            }
        }
    }

    public function edit($id = null, Request $request){
        $companySetting = CompanySetting::query()->findOrFail($id);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'morning_time_in' => ['required','date_format:H:i'],
                'morning_time_out' => ['required','date_format:H:i','after:morning_time_in'],
                'afternoon_time_in' => ['required','date_format:H:i','after:morning_time_out'],
                'afternoon_time_out' => ['required','date_format:H:i','after:afternoon_time_in']
            ],[
                'morning_time_in.required' => ucwords('morning time in required!'),
                'morning_time_in.date_format' => ucwords('invalid morning time in format!'),
                'morning_time_out.required' => ucwords('morning time out required!'),
                'morning_time_out.date_format' => ucwords('invalid morning time out format!'),
                'morning_time_out.after' => ucwords('morning time out must be after morning time in!'),
                'afternoon_time_in.required' => ucwords('afternoon time in required!'),
                'afternoon_time_in.date_format' => ucwords('invalid afternoon time in format!'),
                'afternoon_time_in.after' => ucwords('afternoon time in must be after morning time out!'),
                'afternoon_time_out.required' => ucwords('afternoon time out required!'),
                'afternoon_time_out.date_format' => ucwords('invalid afternoon time out format!'),
                'afternoon_time_out.after' => ucwords('afternoon time out must be after afternoon time in!'),
            ]);

            $companySetting->update($request->all());

            $active = CompanySetting::query();
            if( $request->boolean('active')){
                $companySetting->active = $request->boolean('active');
                $active->where('active','=',1)
                    ->update(['active' => 0]);
            }

            if($companySetting->save() || $active){
                $result = ['message'=>ucwords('the company setting has been saved!'), 'result'=>strtolower('success')];
                return response()->json($result,200);
            }else{
                $result = ['message'=>ucwords('the company setting has not been saved!'), 'result'=>strtolower('error')];
                return response()->json($result,404);
            }
        }
        return response()->json($companySetting);
    }

    public function delete($id = null){
        $companySetting = CompanySetting::query()->findOrFail($id);
        if($companySetting->delete()){
            $result = ['message'=>ucwords('the company setting has been deleted!'), 'result'=>strtolower('success')];
            return response()->json($result,200);
        }else{
            $result = ['message'=>ucwords('the company setting has not been deleted!'), 'result'=>strtolower('error')];
            return response()->json($result,404);
        }
    }

    public function active($id = null){
        $companySetting = CompanySetting::query()->findOrFail($id);
        $bool = $companySetting->active;
        $companySetting->active = !$bool ;

        $active = CompanySetting::query()
            ->where('active','=',1)
            ->update(['active' => 0]);

        if($companySetting->save() || $active ){
            $result = ['message'=>ucwords('the company setting has been saved!'), 'result'=>strtolower('success')];
            return response()->json($result,200);
        }else{
            $result = ['message'=>ucwords('the company setting has not been saved!'), 'result'=>strtolower('error')];
            return response()->json($result,404);
        }
    }

}
