<?php
use Carbon\Carbon;
use App\Models\Attendance;
use App\Models\Holiday;
use App\Models\CompanySetting;
use App\Models\HomeAttendance;
use App\Models\Accomplishment;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Models\SalaryLoan;
// Helpers Here
// BNJ time in 8:30 - 9:00 not late
// Time out morning 12:00 less than 12:00 late morning
// Time in afternoon 12:30 - 1:00 not late
// Time out afternoon 5:30 lett than 5:30 late afternoon
// All time should be formatted in 24 hours format
// $dt = Carbon::now();
// $ddt = Carbon::parse('11:30');

// Get latest loan
function getMyLatestLaon(){
    $myLoan = SalaryLoan::with('salaryDeduction')->myLoan()->orderBy('id', 'desc')->first();
    return $myLoan;
}

// Get second to the last loan
function getMySecondLatestLoan(){
    $myLoan = SalaryLoan::with('salaryDeduction')->myLoan()->orderBy('id', 'desc')->skip(1)->first();
    return $myLoan;
}

function getCurrentDate(){
    return Carbon::now();
}

function excelFormat($amount){
    return number_format($amount, 2);
}

function money($amount){
    return 'PHP' . number_format($amount, 2);
}

function temporaryPagibigRate(){
    return [
        'name' => 'Pag Ibig',
        'employee_rate' => 100, 
        'employer_rate' => 100, 
        'total_rate' => 200
    ];
}

// Get company default hours count
function getCompanyHours(){
    $companyTime = 0;
    $companyTime += (Carbon::parse(getCompanySetting()->morning_time_in)->floatDiffInHours(Carbon::parse(getCompanySetting()->morning_time_out)));
    ;
    $companyTime += (Carbon::parse(getCompanySetting()->afternoon_time_in)->floatDiffInHours(Carbon::parse(getCompanySetting()->afternoon_time_out)));
    getCompanySetting()->afternoon_time_out;
    return $companyTime;
}

// Get company settings
function getCompanySetting(){
    $companySetting = CompanySetting::where('active', 1)->first();
    return $companySetting;
}

// Get current logged users basic salary
function getMyBasicSalary($user = null){
    if($user == null){
        $basicSalary = User::with('profile', 'profile.employeeRole')->findOrFail(auth()->user()->id);
    }else{
        $basicSalary = User::with('profile', 'profile.employeeRole')->findOrFail($user);
    }
    return $basicSalary->profile->employeeRole->basic_salary;
}

// Parse Attendances
function ParseAttendance($timeinam = null, $timeoutam = null, $timeinpm = null, $timeoutpm = null, $id = null){
    $companySetting = CompanySetting::first();
    // Change this into company settings
	$compMorningTimeIn = Carbon::parse($companySetting->morning_time_in);
    $compAfternoonTimeIn = Carbon::parse($companySetting->afternoon_time_in);
    $compMorningTimeOut = Carbon::parse($companySetting->morning_time_out);
    $compAfternoonTimeOut = Carbon::parse($companySetting->afternoon_time_out);
	$morningTimeInLates = [];
	$afternoonTimeInLates = [];

    // Loop and add 15 minutes if difference is greater than 15 am and pm time in
    while($compMorningTimeIn->diffInMinutes($compMorningTimeOut) > 15){
        if($compMorningTimeIn->diffInMinutes($compMorningTimeOut) != 15){
            array_push($morningTimeInLates, $compMorningTimeIn->toTimeString());
        }
        $compMorningTimeIn->addMinutes(15);
    }
    while($compAfternoonTimeIn->diffInMinutes($compAfternoonTimeOut) > 15){
        if($compAfternoonTimeIn->diffInMinutes($compAfternoonTimeOut) != 15){
            array_push($afternoonTimeInLates, $compAfternoonTimeIn->toTimeString());
        }
        $compAfternoonTimeIn->addMinutes(15);
    }


    $morningTimeIn = Carbon::parse($companySetting->morning_time_in); // Will add 15 minutes every late
    $afternoonTimeIn = Carbon::parse($companySetting->afternoon_time_in); // Will add 15 minutes every late
	$time = null;

    // Total work hours in morning
    if($timeinam != null && $timeoutam != null){
		$employeeTimeInAm = Carbon::parse($timeinam);
    	foreach ($morningTimeInLates as $value){
    		$lateTimeInAm = Carbon::parse($value);
    		if($employeeTimeInAm->toTimeString() > $lateTimeInAm->toTimeString()){
    			$morningTimeIn->addMinutes(15);
    		}
    	}
        $timeinam = Carbon::parse($morningTimeIn);
        $timeoutam = Carbon::parse($timeoutam); // Current time out am inputted
        $morningTimeOut = Carbon::parse($companySetting->morning_time_out); // Initialize new company morning time out
        if($timeoutam > $morningTimeOut){
            $time = round($timeinam->floatDiffInHours($morningTimeOut, true), 2);
        }else{

	        $morningTimeOutUnderTime = [];
            // Loop and add 15 minutes if difference is greater than 15 am and pm time out
            while($morningTimeOut->diffInMinutes($timeinam) > 15){
                if($morningTimeOut->diffInMinutes($timeinam) != 15){
                    array_push($morningTimeOutUnderTime, $morningTimeOut->toTimeString());
                }
                $morningTimeOut->subMinutes(15);
            }
            $companyTimeOut = Carbon::parse($companySetting->morning_time_out);
            $employeeTimeOutAm = Carbon::parse($timeoutam);
            foreach ($morningTimeOutUnderTime as $value){
                $underTimeInAm = Carbon::parse($value);
                if($employeeTimeOutAm->toTimeString() < $underTimeInAm->toTimeString()){
                    $companyTimeOut->subMinutes(15);
                }
            }

            $time = round($timeinam->floatDiffInHours(Carbon::parse($companyTimeOut), true), 2);
        }
    }
    // Total work hours in afternoon
    if($timeinpm != null && $timeoutpm != null){
    	$employeeTimeInPm = Carbon::parse($timeinpm);
    	foreach ($afternoonTimeInLates as $value){
    		$lateTimeInPm = Carbon::parse($value);
    		if($employeeTimeInPm->toTimeString() > $lateTimeInPm->toTimeString()){
    			$afternoonTimeIn->addMinutes(15);
    		}
    	}
        $timeinpm = Carbon::parse($afternoonTimeIn);
        $timeoutpm = Carbon::parse($timeoutpm); // Current time out pm inputted
        $afternoonTimeOut = Carbon::parse($companySetting->afternoon_time_out); // Initialize new company afternoon time out
        if($timeoutpm > $afternoonTimeOut){
            $time += round($timeinpm->floatDiffInHours($afternoonTimeOut, true), 2);
        }else{
            $afternoonTimeOutUnderTime = [];
            // Loop and add 15 minutes if difference is greater than 15 am and pm time out
            while($afternoonTimeOut->diffInMinutes($timeinam) > 15){
                if($afternoonTimeOut->diffInMinutes($timeinam) != 15){
                    array_push($afternoonTimeOutUnderTime, $afternoonTimeOut->toTimeString());
                }
                $afternoonTimeOut->subMinutes(15);
            }
            $companyTimeOut = Carbon::parse($companySetting->afternoon_time_out);
            $employeeTimeOutPm = Carbon::parse($timeoutpm);
            foreach ($afternoonTimeOutUnderTime as $value){
                $underTimeInPm = Carbon::parse($value);
                if($employeeTimeOutPm->toTimeString() < $underTimeInPm->toTimeString()){
                    $companyTimeOut->subMinutes(15);
                }
            }
            $time += round($timeinpm->floatDiffInHours($companyTimeOut, true), 2);
        }
    }

    $attendance = Attendance::with('excuse')->findOrFail($id);
    Attendance::updateOrCreate([
        'id' => $id,
        'user_id' => auth()->user()->id
    ],[
        'attendance_total_hours' => ($time == null) ? null:$time - $attendance->excuse->excuse_total_hours
    ]);

    return $time;
}

// Parse Excuses
function ParseExcuse($timeinam = null, $timeoutam = null, $timeinpm = null, $timeoutpm = null, $id = null){
	// Time should be first
	$time = null;
    if($timeinam != null && $timeoutam != null){
        $partialtimeinam = Carbon::parse($timeinam);
        $partialtimeoutam = Carbon::parse($timeoutam);
        $timeinam = null;
        $timeoutam = Carbon::parse($timeoutam);

        // Time in is greater than time out always
        while($partialtimeinam->gt($partialtimeoutam)){
            $partialtimeoutam->addMinutes(15);
            $timeinam = Carbon::parse($partialtimeoutam);
        }
        $time = round($timeoutam->floatDiffInHours($timeinam, true), 2);
    }
    if($timeinpm != null && $timeoutpm != null){
        $partialtimeinpm = Carbon::parse($timeinpm);
        $partialtimeoutpm = Carbon::parse($timeoutpm);
        $timeinpm = null;
        $timeoutpm = Carbon::parse($timeoutpm);

        // Time in is greater than time out always
        while($partialtimeinpm->gt($partialtimeoutpm)){
            $partialtimeoutpm->addMinutes(15);
            $timeinpm = Carbon::parse($partialtimeoutpm);
        }
        $time += round($timeoutpm->floatDiffInHours($timeinpm, true), 2);
    }
    if($id != null){
	    $attendance = Attendance::findOrFail($id);
	    $attendance->excuse()->updateOrCreate([
	        'attendance_id' => $id
	    ],[
	    	'excuse_total_hours' => $time
	    ]);
	}
    return $time;
}

// Seed Without Model
function seedWithoutModelHelper($table, $filename){
    $path = storage_path('databases/'.$filename);
    $arr_data = [];
    $file = fopen($path,"r");
    $header = fgetcsv($file);
    $count = count($header) - 1;
    $status = false;
    while (($data = fgetcsv($file)) !== FALSE)
    {
        for($i = 0; $i <= $count; $i++){
            if($data[$i] == "NULL"){
                $arr_data += [$header[$i] => null];
            }else{
                $arr_data += [$header[$i] => $data[$i]];
            }
        }
        DB::table($table)->insert($arr_data);
        $arr_data = [];
    }
}

// Compute Home Attendance
function computeHomeAttendances($attendance_date){
    $attendance = Attendance::with('accomplishment', 'accomplishment.homeAttendances')
                    ->where('attendance_date', Carbon::parse($attendance_date)->toDateString())
                    ->first();
    if($attendance->accomplishment()->exists()){
        if($attendance->accomplishment->homeAttendances()->exists()){
            foreach ($attendance->accomplishment->homeAttendances as $values) {
                $time_in = Carbon::parse($values->time_in);
                $time_out = Carbon::parse($values->time_out);
                $id = $values->id;
                $total_work_hours = round($time_in->floatDiffInHours($time_out, true), 2);
                HomeAttendance::updateOrCreate([
                    'id' => $id
                ],[
                    'total_work_hours' => $total_work_hours
                ]);
            }
        }

    }
}

// Calculate Total Work Hours in Home Attendances
function calculateWorkFromHomeHours($homeAttendances){
    $time = null;
    if($homeAttendances != null){
        foreach ($homeAttendances as $attendance) {
            $time_in = Carbon::parse($attendance->time_in);
            $time_out = Carbon::parse($attendance->time_out);
            $time += round($time_in->floatDiffInHours($time_out, true), 2);
        }
    }
    return $time;
}


