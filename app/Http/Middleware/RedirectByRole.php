<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Auth\Middleware\Role as Middleware;
use Illuminate\Support\Facades\Auth;

class RedirectByRole{

  public function handle($request, Closure $next, String $role) {
    $user = Auth::user();
    if($user->role == $role && $user->active == true){
        return $next($request);
    }else if($user->active == false){
        Auth::logout();
        return redirect(RouteServiceProvider::LOGIN)->with('status', 'Your Account is Disabled!');
    }
    abort(404);
  }
}
