<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'employee_role_id',
        'firstname',
        'lastname',
        'middlename',
        'employee_id_number',
        'birthdate',
        'date_employed',
        'employment_status',
        'contact_number',
        'address',
        'has_contributions',
    ];

    // Relations
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function employeeRole(){
        return $this->belongsTo(EmployeeRole::class);
    }

//
    protected function getEmployeeBirthdateAttribute(){
        $holidayDate = new Carbon($this->attributes['birthdate']);
        return date('Y-').$holidayDate->format('m-d');
    }

}
