<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    use HasFactory;

    protected $fillable = [
        'attendance_id',
        'content',
        'report_type',
    ];

    // Relations
    public function attendance(){
        return $this->belongsTo(Attendance::class);
    }
}
