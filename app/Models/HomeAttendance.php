<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class HomeAttendance extends Model
{
    use HasFactory;

    protected $fillable = [
        'accomplishment_id',
        'time_in',
        'time_out',
        'total_work_hours',
    ];

    // Relations
    public function accomplishment(){
        return $this->belongsTo(Accomplishment::class);
    }

    // Closure for data manipulation
    // protected static function booted()
    // {
    //     static::saving(function ($home_attendances) {
    //         foreach($home_attendances as $home_attendance){
    //             array_push($home_attendance, ['total_work_hours' => 1]);
    //             var_dump($home_attendance);
    //         }
    //     });
    // }
}
