<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Holiday extends Model
{
    use HasFactory;

    protected $fillable = [
        'holiday_name',
        'holiday_date',
        'holiday_type',
        'color',
        'rate',
        'is_per_year',
    ];

    protected $casts = [
        'holiday_date' => 'date:Y-m-d'
    ];

    protected function getHolidayDateAttribute(){
        if($this->attributes['is_per_year']){
            $holidayDate = new Carbon($this->attributes['holiday_date']);
            return date('Y-').$holidayDate->format('m-d');
        }
        return $this->attributes['holiday_date'];
    }
}
