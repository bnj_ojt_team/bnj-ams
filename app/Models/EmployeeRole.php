<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeRole extends Model
{
    use HasFactory;

    protected $fillable = [
        'position',
        'level',
        'basic_salary',
    ];

    // Relations
    public function profiles(){
        return $this->hasMany(Profile::class);
    }

    public function contributions(){
        return $this->hasMany(Contribution::class);
    }
}
