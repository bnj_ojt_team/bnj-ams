<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'attendance_date',
        'timein_am',
        'timeout_am',
        'timein_pm',
        'timeout_pm',
        'attendance_total_hours',
        'is_holiday',
        'is_present',
        'is_late',
        'is_work_from_home',
        'is_overtime',
    ];

    // Scopes
    public function scopeMyAttendance($query){
        if(auth()->user()->role != 'admin'){
            $query->where('user_id', '=', auth()->user()->id);
        }
    }

    // Relations
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function excuse(){
        return $this->hasOne(Excuse::class);
    }
    public function reason(){
        return $this->hasMany(Reason::class);
    }
    public function accomplishment(){
        return $this->hasOne(Accomplishment::class);
    }

}
