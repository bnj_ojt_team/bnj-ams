<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contribution extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'employer_rate',
        'employee_rate',
        'total_percentage',
        'employee_role_id',
    ];

    public function setEmployerRateAttribute($value){
        return $this->attributes['employer_rate'] = $value / 100;
    }
    public function setEmployeeRateAttribute($value){
        return $this->attributes['employee_rate'] = $value / 100;
    }
    public function setTotalPercentageAttribute($value){
        return $this->attributes['total_percentage'] = $value / 100;
    }
    public function setBenefitAttribute($value){
        return $this->attributes['name'] = strtolower($value);
    }

    // Relations
    public function employeeRole(){
        return $this->belongsTo(EmployeeRole::class);
    }
}
