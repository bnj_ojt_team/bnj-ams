<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accomplishment extends Model
{
    use HasFactory;

    protected $fillable = [
        'attendance_id',
        'content',
        'status',
    ];

    // Relations
    public function attendance(){
        return $this->belongsTo(Attendance::class);
    }

    public function homeAttendances(){
        return $this->hasMany(HomeAttendance::class);
    }
}
