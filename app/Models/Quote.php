<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\AsStringable;

class Quote extends Model
{
    use HasFactory;

    protected $fillable = [
        'is_active',
        'quotes',
    ];
}
