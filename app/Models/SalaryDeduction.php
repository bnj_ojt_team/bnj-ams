<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SalaryDeduction extends Model
{
    use HasFactory;

    protected $fillable = [
        'deduction',
        'date'
    ];

    // Model Scopes
    public function scopeThisMonthDeductions($query){
        $from = new Carbon('first day of this month');
        $to = new Carbon('last day of this month');
        $query->whereBetween('date', [$from, $to]);
    }

    // Relations
    public function salaryLoan(){
        return $this->belongsTo(SalaryLoan::class);
    }

    // Mutator
    public function setDateAttribute($value){
        return $this->attributes['date'] = Carbon::now();
    }
}
