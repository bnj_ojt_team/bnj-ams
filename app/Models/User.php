<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'token',
        'role',
        'image',
        'active'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Relations
    public function profile(){
        return $this->hasOne(Profile::class);
    }
    public function contact(){
        return $this->hasOne(Contact::class);
    }
    public function attendances(){
        return $this->hasMany(Attendance::class);
    }
    public function salaryLoans(){
        return $this->hasMany(SalaryLoan::class);
    }

    // Mutators
    public function setPasswordAttribute($value){
        return $this->attributes['password'] = Hash::make($value);
    }

    public function setRoleAttribute($value){
        return $this->attributes['role'] = strtolower($value);
    }
}
