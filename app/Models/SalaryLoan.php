<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SalaryLoan extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'amount',
        'remainder',
        'balance',
        'reason',
        'status',
        'date',
    ];

    // Scopes
    public function scopeMyLoan($query){
        $query->where('user_id', '=', auth()->user()->id);
    }
    public function scopeThisMonthDeductions($query){
        $from = new Carbon('first day of this month');
        $to = new Carbon('last day of this month');
        $query->whereBetween('date', [$from, $to]);
    }

    // Relations
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function salaryDeduction(){
        return $this->hasOne(SalaryDeduction::class);
    }

    // Mutator
    public function setDateAttribute($value){
        return $this->attributes['date'] = Carbon::now();
    }

    protected function setStatusAttribute($value){
        return $this->attributes['status'] = ucwords($value);
    }

}
