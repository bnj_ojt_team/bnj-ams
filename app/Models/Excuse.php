<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Excuse extends Model
{
    use HasFactory;

    protected $fillable = [
        'attendance_id',
        'timein_am',
        'timeout_am',
        'timein_pm',
        'timeout_pm',
        'excuse_total_hours',
    ];

    // Relations
    public function attendance(){
        return $this->belongsTo(Attendance::class);
    }
}
