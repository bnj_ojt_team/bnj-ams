<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\InvokableRule;
use Carbon\Carbon;

class CheckFormatPostMeridiem implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if ($value != null) {
            if(!preg_match('/^(1[2-9]|2[0-3]):([0-5][0-9])$/', $value)){
                $fail('The given Time Out in PM is invalid');
            }
        }else if (request()->has('timeout_pm')){
            $fail('Please provide input in Time In PM first before providing Time Out PM.');
        }else if (request()->has('excuse.timein_pm')){
            $fail('Please provide input in Excuse Time Out PM first before providing Excuse Time In PM.');
        }else if(request()->input('timein_pm') == '' && request()->has('excuse.timeout_pm')) {
            $fail('Please prvoide Time In PM first before providing Excuse Time Out PM.');
        }
    }
}
