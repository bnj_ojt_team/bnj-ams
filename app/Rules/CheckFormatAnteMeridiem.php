<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\InvokableRule;
use Carbon\Carbon;

class CheckFormatAnteMeridiem implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if ($value != null) {
            if(!preg_match('/^([0-9]|0[0-9]|1[0-1]):([0-5][0-9])$/', $value)){
                $fail('The given Time Out in AM is invalid');
            }
        }else if (request()->has('timeout_am')){
            // If user has inputted time out am without inputting time in am first
            $fail('Please provide input in Time In AM first before providing Time Out AM.');
        }else if (request()->has('excuse.timein_am')){
            // If user has inputted time in excuse am without inputting time out excuse am first
            $fail('Please provide input in Excuse Time Out AM first before providing Excuse Time In AM.');
        }else if(request()->input('timein_am') == '' && request()->has('excuse.timeout_am')) {
            // If user inputted time out excuse without time in in am
            $fail('Please prvoide Time In AM first before providing Excuse Time Out AM.');
        }
    }
}
