<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class dbimport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:import-main-backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Database Import';

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $filepath = storage_path('app/public/databases/');
        $filename = 'bnj_ams_db.sql';
        if(!File::isDirectory($filepath)){
            File::makeDirectory($filepath);
        }
        $command = "mysql -h " . env('DB_HOST') . " -u ". env('DB_USERNAME') ." -p " . env('DB_PASSWORD') . " " . env('DB_DATABASE') . " < " . $filepath . $filename;
        if(exec($command) == ""){
            $this->info('Database import successfull!');
        }else{
            $this->error('Something went wrong!');
        }
    }
}
