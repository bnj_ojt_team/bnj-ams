<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class dbexport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Database Export';

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $filepath = storage_path('app/public/databases/');
        $filename = 'bnj_ams_db.sql';
        if(!File::isDirectory($filepath)){
            File::makeDirectory($filepath);
        }
        $command = "mysqldump --opt -h " . env('DB_HOST') . " -u ". env('DB_USERNAME') . " " . env('DB_DATABASE') . " > " . $filepath . $filename;

        if(exec($command) == ""){
            $this->info('Database import successfull!');
        }else{
            $this->error('Something went wrong!');
        }
    }
}
