<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;
use App\Models\EmployeeRole;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class);
            $table->foreignIdFor(EmployeeRole::class);
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');
            $table->string('employee_id_number');
            $table->date('birthdate');
            $table->date('date_employed');
            $table->string('employment_status');
            $table->string('contact_number');
            $table->text('address');
            $table->tinyInteger('has_contributions');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
};
