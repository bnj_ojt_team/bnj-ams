<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class);
            $table->date('attendance_date');
            $table->time('timein_am')->nullable();
            $table->time('timeout_am')->nullable();
            $table->time('timein_pm')->nullable();
            $table->time('timeout_pm')->nullable();
            $table->double('attendance_total_hours')->nullable();
            $table->boolean('is_holiday')->nullable();
            $table->boolean('is_present')->nullable();
            $table->boolean('is_late')->nullable();
            $table->boolean('is_work_from_home')->default(0);
            $table->boolean('is_overtime')->default(false)->comment('Check if attendance is approved as overtime');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
};
