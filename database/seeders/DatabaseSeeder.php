<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // Helper is in App\Http folder
        seedWithoutModelHelper('accomplishments', 'accomplishments.csv');
        seedWithoutModelHelper('attendances', 'attendances.csv');
        seedWithoutModelHelper('company_settings', 'company_settings.csv');
        seedWithoutModelHelper('contacts', 'contacts.csv');
        seedWithoutModelHelper('employee_roles', 'employee_roles.csv');
        seedWithoutModelHelper('excuses', 'excuses.csv');
        seedWithoutModelHelper('failed_jobs', 'failed_jobs.csv');
        seedWithoutModelHelper('holidays', 'holidays.csv');
        seedWithoutModelHelper('password_resets', 'password_resets.csv');
        seedWithoutModelHelper('profiles', 'profiles.csv');
        seedWithoutModelHelper('quotes', 'quotes.csv');
        seedWithoutModelHelper('reasons', 'reasons.csv');
        seedWithoutModelHelper('users', 'users.csv');
        seedWithoutModelHelper('home_attendances', 'home_attendances.csv');
        seedWithoutModelHelper('contributions', 'contributions.csv');
        seedWithoutModelHelper('salary_loans', 'salary_loans.csv');
        seedWithoutModelHelper('salary_deductions', 'salary_deductions.csv');
    }
}
